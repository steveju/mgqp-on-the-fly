# 1666737
p_version = "v3.00"
$patch_ver = "#{p_version}.00"
if SCRIPT_UPDATE_ID != "qoiekacb"
  raise "PatchError  -  #{NWPatch.ver_str}\nこれは#{p_version}用のパッチです。\n#{p_version}以外のバージョンには導入しないでください。"
end

=begin
#===============================================================================
 Title: External Script Loader
 Author: Tsukihime
 Date: Dec 2, 2013
 URL: http://himeworks.wordpress.com/2013/12/02/external-script-loader/
--------------------------------------------------------------------------------
 ** Change log
 Dec 2, 2013
   - Initial release
--------------------------------------------------------------------------------   
 ** Terms of Use
 * Free to use in commercial/non-commercial projects
 * No real support. The script is provided as-is
 * Will do bug fixes, but no compatibility patches
 * Features may be requested but no guarantees, especially if it is non-trivial
 * Credits to Tsukihime in your project
 * Preserve this header
--------------------------------------------------------------------------------
 ** Description
 
 This script allows you to load external scripts into the game. It supports
 loading from encrypted archives as well.

--------------------------------------------------------------------------------
 ** Installation
 
 Place this script below Materials and above Main

--------------------------------------------------------------------------------
 ** Usage 
 
 To load and evaluate script, use the following function call:
 
   load_script(script_path)
   
--------------------------------------------------------------------------------
 ** Example
 
 I have a folder called "Scripts" and a script called "test.rb" in that folder.
 If I want to load the script, I would just write
 
   load_script("Scripts/test.rb")
   
 This will evaluate the test script.
 
#===============================================================================
=end
$imported = {} if $imported.nil?
$imported["TH_ExternalScriptLoader"] = true
#===============================================================================
# * Rest of Script
#===============================================================================
#-------------------------------------------------------------------------------
# Convenience function. Equivalent to
#   script = load_data(path)
#   eval(script)
# It supports loading from encrypted archives
#-------------------------------------------------------------------------------
def load_script(path)
  eval(load_data(path))
end

#-------------------------------------------------------------------------------
# Load files from non-RM files
#-------------------------------------------------------------------------------
class << Marshal
  alias_method(:th_core_load, :load)
  def load(port, proc = nil)
    th_core_load(port, proc)
  rescue TypeError
    if port.kind_of?(File)
      port.rewind 
      port.read
    else
      port
    end
  end
end unless Marshal.respond_to?(:th_core_load)

load_script("Translated Files/Scripts/3 - Vocab.rb") if File.exists?("Translated Files\\Scripts\\3 - Vocab.rb")
load_script("Translated Files/Scripts/基盤システム/134 - エンチャント名.rb") if File.exists?("Translated Files\\Scripts\\基盤システム\\134 - エンチャント名.rb")
load_script("Translated Files/Scripts/画面/153 - 転職編集.rb") if File.exists?("Translated Files\\Scripts\\画面\\153 - 転職編集.rb")
load_script("Translated Files/Scripts/192 - DownWords(Actor).rb") if File.exists?("Translated Files\\Scripts\\192 - DownWords(Actor).rb")
load_script("Translated Files/Scripts/193 - DownWords(Enemy).rb") if File.exists?("Translated Files\\Scripts\\193 - DownWords(Enemy).rb")
load_script("Translated Files/Scripts/196 - Follower.rb") if File.exists?("Translated Files\\Scripts\\196 - Follower.rb")
load_script("Translated Files/Scripts/198 - JobChange.rb") if File.exists?("Translated Files\\Scripts\\198 - JobChange.rb")
load_script("Translated Files/Scripts/199 - Library(Actor).rb") if File.exists?("Translated Files\\Scripts\\199 - Library(Actor).rb")
load_script("Translated Files/Scripts/202 - Library(Enemy).rb") if File.exists?("Translated Files\\Scripts\\202 - Library(Enemy).rb")
load_script("Translated Files/Scripts/203 - Library(H).rb") if File.exists?("Translated Files\\Scripts\\203 - Library(H).rb")
load_script("Translated Files/Scripts/205 - Library(Medal).rb") if File.exists?("Translated Files\\Scripts\\205 - Library(Medal).rb")
load_script("Translated Files/Scripts/208 - Present.rb") if File.exists?("Translated Files\\Scripts\\208 - Present.rb")
load_script("Translated Files/Scripts/210 - SkillWords(Actor).rb") if File.exists?("Translated Files\\Scripts\\210 - SkillWords(Actor).rb")

load_script("Patch/bugfixes.rb") if File.exists?("Patch\\bugfixes.rb")
load_script("Patch/image_replacements.rb") if File.exists?("Patch\\image_replacements.rb")
load_script("Patch/fullscreen_mod.rb") if File.exists?("Patch\\fullscreen_mod.rb")
load_script("Patch/static_translations.rb") if File.exists?("Patch\\static_translations.rb")
load_script("Patch/text_processing.rb") if File.exists?("Patch\\text_processing.rb")
load_script("Patch/translator_core_maps.rb") if File.exists?("Patch\\translator_core_maps.rb")
load_script("Patch/translator_core_cevents.rb") if File.exists?("Patch\\translator_core_cevents.rb")
load_script("Patch/other_mods.rb") if File.exists?("Patch\\other_mods.rb")