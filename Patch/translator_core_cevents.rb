class << DataManager
  def load_skills_from_file(file_location)
    if File.exists?(file_location)
      lines_from_skills = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_skills.count > 0
        line = lines_from_skills.shift.strip
        if line.start_with?("Skill ")
          skill_id = 0
          skill_name = ""
          skill_message1 = ""
          skill_message2 = ""
          skill_desc = ""
          skill_note = ""

          skill_id = line[5..-1].to_i
          line = lines_from_skills.shift.strip

          skill_message1 = line[13..-2].gsub("\\\\U","\\U").gsub("\\\\E","\\E").gsub("\\'","\'")
          $data_skills[skill_id].message1 = skill_message1 unless skill_message1.empty?
          line = lines_from_skills.shift.strip

          skill_message2 = line[13..-2].gsub("\\\\U","\\U").gsub("\\\\E","\\E").gsub("\\'","\'")
          $data_skills[skill_id].message2 = skill_message2 unless skill_message2.empty?
          line = lines_from_skills.shift.strip

          skill_name = line[8..-2].gsub("\\'","\'")
          $data_skills[skill_id].name = skill_name unless skill_name.empty?
          line = lines_from_skills.shift.strip

          skill_desc = line[15..-2].gsub("\\n","\n").gsub("\\r","\r").gsub("\\i","\i").gsub("\\'","\'")
          $data_skills[skill_id].description = skill_desc unless skill_desc.empty?
          line = lines_from_skills.shift.strip

          skill_note = line[8..-2].gsub("\\n","\n").gsub("\\r","\r")
          unless skill_note.empty?
            $data_skills[skill_id].note = skill_note
            $data_skills[skill_id].note_analyze
          end
        end
      end
      $data_skills.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_states_from_file(file_location)
    if File.exists?(file_location)
      lines_from_states = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_states.count > 0
        line = lines_from_states.shift.strip
        if line.start_with?("State ")
          state_id = 0
          state_message1 = ""
          state_message2 = ""
          state_message3 = ""
          state_message4 = ""
          state_name = ""
          state_desc = ""
          state_note = ""

          state_id = line[5..-1].to_i
          line = lines_from_states.shift.strip

          state_message1 = line[13..-2].gsub("\\'","\'")
          $data_states[state_id].message1 = state_message1 unless state_message1.empty?
          line = lines_from_states.shift.strip

          state_message2 = line[13..-2].gsub("\\'","\'")
          $data_states[state_id].message2 = state_message2 unless state_message2.empty? 
          line = lines_from_states.shift.strip

          state_message3 = line[13..-2].gsub("\\'","\'")
          $data_states[state_id].message3 = state_message3 unless state_message3.empty?
          line = lines_from_states.shift.strip

          state_message4 = line[13..-2].gsub("\\'","\'")
          $data_states[state_id].message4 = state_message4 unless state_message4.empty?
          line = lines_from_states.shift.strip

          state_name = line[8..-2].gsub("\\'","\'")
          $data_states[state_id].name = state_name unless state_name.empty?
          line = lines_from_states.shift.strip

          state_desc = line[15..-2].gsub("\\'","\'") unless line.end_with?("nil")
          $data_states[state_id].description = state_desc unless state_desc.empty?
          line = lines_from_states.shift.strip

          state_note = line[8..-2].gsub("\\n","\n").gsub("\\r","\r")
          unless state_note.empty?
            $data_states[state_id].note = state_note
            $data_states[state_id].note_analyze
          end
        end
      end
      $data_states.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_armors_from_file(file_location)
    if File.exists?(file_location)
      lines_from_armors = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_armors.count > 0
        line = lines_from_armors.shift.strip
        if line.start_with?("Armor ")
          armor_id = 0
          armor_name = ""
          armor_desc = ""
          armor_note = ""

          armor_id = line[5..-1].to_i
          line = lines_from_armors.shift.strip

          armor_name = line[8..-2].gsub("\\'","\'")
          $data_armors[armor_id].name = armor_name unless armor_name.empty?
          line = lines_from_armors.shift.strip

          armor_desc = line[15..-2].gsub("\\'","\'").gsub("\\n","\n").gsub("\\r","\r").gsub("\\i","\i").gsub("\\*","\*").gsub("\\\"","\"")
          $data_armors[armor_id].description = armor_desc unless armor_desc.empty?
          line = lines_from_armors.shift.strip

          armor_note = line[8..-2].gsub("\\r\\n","\r\n").gsub("\\\\n","\\n").gsub("\\\\e","\\e")
          unless armor_note.empty?
            $data_armors[armor_id].note = armor_note
            $data_armors[armor_id].note_analyze
          end
        end
      end
      $data_armors.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_weapons_from_file(file_location)
    if File.exists?(file_location)
      lines_from_weapons = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_weapons.count > 0
        line = lines_from_weapons.shift.strip
        if line.start_with?("Weapon ")
          weapon_id = 0
          weapon_name = ""
          weapon_desc = ""
          weapon_note = ""

          weapon_id = line[6..-1].to_i
          line = lines_from_weapons.shift.strip

          weapon_name = line[8..-2].gsub("\\'","\'")
          $data_weapons[weapon_id].name = weapon_name unless weapon_name.empty?
          line = lines_from_weapons.shift.strip

          weapon_desc = line[15..-2].gsub("\\'","\'").gsub("\\n","\n").gsub("\\r","\r").gsub("\\i","\i").gsub("\\\"","\"")
          $data_weapons[weapon_id].description = weapon_desc unless weapon_desc.empty?
          line = lines_from_weapons.shift.strip

          weapon_note = line[8..-2].gsub("\\r\\n","\r\n").gsub("\\\\n","\\n").gsub("\\\\e","\\e")
          unless weapon_note.empty?
            $data_weapons[weapon_id].note = weapon_note
            $data_weapons[weapon_id].note_analyze
          end
        end
      end
      $data_weapons.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_classes_from_file(file_location)
    if File.exists?(file_location)
      lines_from_classes = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_classes.count > 0
        line = lines_from_classes.shift.strip
        if line.start_with?("Class ")
          class_id = 0
          class_name = ""
          class_desc = ""
          class_note = ""

          class_id = line[5..-1].to_i
          line = lines_from_classes.shift.strip

          class_name = line[8..-2].gsub("\\'","\'")
          $data_classes[class_id].name = class_name unless class_name.empty?
          line = lines_from_classes.shift.strip

          class_desc = line[15..-2].gsub("\\'","\'").gsub("\\n","\n").gsub("\\r","\r")
          $data_classes[class_id].description = class_desc unless class_desc.empty?
          line = lines_from_classes.shift.strip

          class_note = line[8..-2].gsub("\\r\\n","\r\n").gsub("\\\\n","\\n").gsub("\\\\e","\\e")
          unless class_note.empty?
            $data_classes[class_id].note = class_note
            $data_classes[class_id].note_analyze
          end
        end
      end
      $data_classes.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_actors_from_file(file_location)
    if File.exists?(file_location)
      lines_from_actors = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_actors.count > 0
        line = lines_from_actors.shift.strip
        if line.start_with?("Actor ")
          actor_id = 0
          actor_nickname = ""
          actor_charname = ""
          actor_face = ""
          actor_name = ""
          actor_desc = ""
          actor_note = ""

          actor_id = line[5..-1].to_i
          line = lines_from_actors.shift.strip

          actor_nickname = line[12..-2]
          $data_actors[actor_id].nickname = actor_nickname unless actor_nickname.empty?
          line = lines_from_actors.shift.strip

          actor_charname = line[18..-2]
          $data_actors[actor_id].character_name = actor_charname unless actor_charname.empty?
          line = lines_from_actors.shift.strip

          actor_face = line[13..-2]
          $data_actors[actor_id].face_name = actor_face unless actor_face.empty?
          line = lines_from_actors.shift.strip

          actor_name = line[8..-2]
          $data_actors[actor_id].name = actor_name unless actor_name.empty?
          line = lines_from_actors.shift.strip

          actor_desc = line[15..-2].gsub("\\'","\'").gsub("\\n","\n").gsub("\\r","\r")
          $data_actors[actor_id].description = actor_desc unless actor_desc.empty?
          line = lines_from_actors.shift.strip

          actor_note = line[8..-2].gsub("\\n","\n").gsub("\\r","\r")
          unless actor_note.empty?
            $data_actors[actor_id].note = actor_note
            $data_actors[actor_id].note_analyze
          end
        end
      end
      $data_actors.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_items_from_file(file_location)
    if File.exists?(file_location)
      lines_from_items = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_items.count > 0
        line = lines_from_items.shift.strip
        if line.start_with?("Item ")
          item_id = 0
          item_name = ""
          item_desc = ""
          item_note = ""

          item_id = line[4..-1].to_i
          line = lines_from_items.shift.strip

          item_name = line[8..-2].gsub("\\'","\'")
          $data_items[item_id].name = item_name unless item_name.empty?
          line = lines_from_items.shift.strip

          item_desc = line[15..-2].gsub("\\'","\'").gsub("\\n","\n").gsub("\\r","\r").gsub("\\i","\i").gsub("\\*","\*").gsub("\\\"","\"")
          $data_items[item_id].description = item_desc unless item_desc.empty?
          line = lines_from_items.shift.strip

          item_note = line[8..-2].gsub("\\n","\n").gsub("\\r","\r")
          unless item_note.empty?
            $data_items[item_id].note = item_note
            $data_items[item_id].note_analyze
          end
        end
      end
      $data_items.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_enemies_from_file(file_location)
    if File.exists?(file_location)
      lines_from_enemies = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_enemies.count > 0
        line = lines_from_enemies.shift.strip
        if line.start_with?("Enemy ")
          enemy_id = 0
          enemy_image = ""
          enemy_name = ""
          enemy_desc = ""
          enemy_note = ""

          enemy_id = line[5..-1].to_i
          line = lines_from_enemies.shift.strip

          enemy_image = line[16..-2]
          $data_enemies[enemy_id].battler_name = enemy_image unless enemy_image.empty?
          line = lines_from_enemies.shift.strip

          enemy_name = line[8..-2]
          $data_enemies[enemy_id].name = enemy_name unless enemy_name.empty?
          line = lines_from_enemies.shift.strip

          enemy_desc = line[15..-2] unless line.end_with?("nil")
          $data_enemies[enemy_id].description = enemy_desc unless enemy_desc.empty?
          line = lines_from_enemies.shift.strip

          enemy_note = line[8..-2].gsub("\\n","\n").gsub("\\r","\r")
          unless enemy_note.empty?
            $data_enemies[enemy_id].note = enemy_note
            $data_enemies[enemy_id].note_analyze
          end
        end
      end
      $data_enemies.compact.each{|x|x.nw_note_analyze}
    end
  end

  def load_terms_from_file(file_location)
    if File.exists?(file_location)
      lines_from_terms = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_terms.count > 0
        line = lines_from_terms.shift.strip
        case line
        when "Basic"
          $data_system.terms.basic[0] = lines_from_terms.shift.strip[9..-2]
          $data_system.terms.basic[1] = lines_from_terms.shift.strip[17..-2]
          $data_system.terms.basic[2] = lines_from_terms.shift.strip[6..-2]
          $data_system.terms.basic[3] = lines_from_terms.shift.strip[14..-2]
          $data_system.terms.basic[4] = lines_from_terms.shift.strip[6..-2]
          $data_system.terms.basic[5] = lines_from_terms.shift.strip[14..-2]
          $data_system.terms.basic[6] = lines_from_terms.shift.strip[6..-2]
          $data_system.terms.basic[7] = lines_from_terms.shift.strip[14..-2]
        when "Params"
          $data_system.terms.params[0] = lines_from_terms.shift.strip[14..-2]
          $data_system.terms.params[1] = lines_from_terms.shift.strip[14..-2]
          $data_system.terms.params[2] = lines_from_terms.shift.strip[16..-2]
          $data_system.terms.params[3] = lines_from_terms.shift.strip[11..-2]
          $data_system.terms.params[4] = lines_from_terms.shift.strip[15..-2]
          $data_system.terms.params[5] = lines_from_terms.shift.strip[17..-2]
          $data_system.terms.params[6] = lines_from_terms.shift.strip[11..-2]
          $data_system.terms.params[7] = lines_from_terms.shift.strip[8..-2]
        when "ETypes"
          $data_system.terms.etypes[0] = lines_from_terms.shift.strip[10..-2]
          $data_system.terms.etypes[1] = lines_from_terms.shift.strip[10..-2]
          $data_system.terms.etypes[2] = lines_from_terms.shift.strip[8..-2]
          $data_system.terms.etypes[3] = lines_from_terms.shift.strip[8..-2]
          $data_system.terms.etypes[4] = lines_from_terms.shift.strip[13..-2]
        when "Commands"
          $data_system.terms.commands[0] = lines_from_terms.shift.strip[9..-2] #fight
          $data_system.terms.commands[1] = lines_from_terms.shift.strip[10..-2] #run
          $data_system.terms.commands[2] = lines_from_terms.shift.strip[10..-2] #attack
          $data_system.terms.commands[3] = lines_from_terms.shift.strip[10..-2] #defend
          $data_system.terms.commands[4] = lines_from_terms.shift.strip[8..-2] #item
          $data_system.terms.commands[5] = lines_from_terms.shift.strip[9..-2] #skill
          $data_system.terms.commands[6] = lines_from_terms.shift.strip[13..-2] #equipment
          $data_system.terms.commands[7] = lines_from_terms.shift.strip[10..-2] #status
          $data_system.terms.commands[8] = lines_from_terms.shift.strip[11..-2] #formation
          $data_system.terms.commands[9] = lines_from_terms.shift.strip[8..-2] #save
          $data_system.terms.commands[10] = lines_from_terms.shift.strip[15..-2] #endofgame
          lines_from_terms.shift.strip                                           #unused
          $data_system.terms.commands[12] = lines_from_terms.shift.strip[11..-2] #weapons
          $data_system.terms.commands[13] = lines_from_terms.shift.strip[9..-2] #armor
          $data_system.terms.commands[14] = lines_from_terms.shift.strip[13..-2] #valuables
          $data_system.terms.commands[15] = lines_from_terms.shift.strip[20..-2] #changeequipment
          $data_system.terms.commands[16] = lines_from_terms.shift.strip[18..-2] #bestequipment
          $data_system.terms.commands[17] = lines_from_terms.shift.strip[14..-2] #removeall
          $data_system.terms.commands[18] = lines_from_terms.shift.strip[12..-2] #newgame
          $data_system.terms.commands[19] = lines_from_terms.shift.strip[12..-2] #continue
          $data_system.terms.commands[20] = lines_from_terms.shift.strip[12..-2] #shutdown
          $data_system.terms.commands[21] = lines_from_terms.shift.strip[15..-2] #gototitle
          $data_system.terms.commands[22] = lines_from_terms.shift.strip[8..-2] #quitcancel
        end
      end
    end
  end

  def load_skill_types_from_file(file_location)
    if File.exists?(file_location)
      temp_array = []
      lines_from_file = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_file.count > 0
        line = lines_from_file.shift.strip
        temp_array.push(line[1..-2])
      end
      $data_system.skill_types = temp_array if $data_system.skill_types.count == temp_array.count
    end
  end

  def load_weapon_types_from_file(file_location)
    if File.exists?(file_location)
      temp_array = []
      lines_from_file = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_file.count > 0
        line = lines_from_file.shift.strip
        temp_array.push(line[1..-2])
      end
      $data_system.weapon_types = temp_array if $data_system.weapon_types.count == temp_array.count
    end
  end

  def load_armor_types_from_file(file_location)
    if File.exists?(file_location)
      temp_array = []
      lines_from_file = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_file.count > 0
        line = lines_from_file.shift.strip
        temp_array.push(line[1..-2])
      end
      $data_system.armor_types = temp_array if $data_system.armor_types.count == temp_array.count
    end
  end

  def load_elements_from_file(file_location)
    if File.exists?(file_location)
      temp_array = []
      lines_from_file = File.readlines(file_location).reject { |x| x.empty? }
      while lines_from_file.count > 0
        line = lines_from_file.shift.strip
        temp_array.push(line[1..-2])
      end
      $data_system.elements = temp_array if $data_system.elements.count == temp_array.count
    end
  end

  def convert_placenames
    NWConst::Warp::Places.each do |place|
      place[:name] = convert_jpn_placename(place[:name])
    end
  end

  def load_troops_from_file(troops_file_location)
    troop_array = DataManager.read_event_data_from_file(troops_file_location)
    $data_troops.each do |troop|
      unless troop.nil?
        troop.pages.each_with_index do |page, index|
          page.list = troop_array[troop.id][index] unless page.nil? or troop_array[troop.id][index].empty?
        end
      end
    end
  end

  def get_events_from_file(event_file_location, event_id=0)
    # Name of event, if needed
    event_name = ""
    # Array to store the generated commands
    temp_command_list = Array.new()
    # reads the entire common event into memory
    if File.exists?(event_file_location)
      temp_event_lines = File.readlines(event_file_location).reject { |x| x.empty? }
      templine = ""
      templine_indent = 0
      temp_event_id = 0

      loop do
        templine = temp_event_lines.shift.strip
        if templine.start_with?("CommonEvent ")
          loop do
            # read next line
            templine = temp_event_lines.shift.strip

            if templine.start_with?("Name ")
              event_name = templine[8..-2]
              templine = temp_event_lines.shift.strip
            end
            # ignore empty lines
            if templine != ""
              # all the data is inside ()
              begin
                temp_parameters = templine.split(/[()]/)
              rescue
                temp_parameters = templine.force_encoding('ASCII-8BIT').encode.split(/[()]/)
              end
              # init default values
              temp_command_code = 0
              temp_add_param = []
            
              case temp_parameters[0]
              when "Empty"
                temp_command_code = 0
              when "ShowTextAttributes"
                temp_command_code = 101
                # get face graphics string
                temp_array = temp_parameters[1][2..-2].split(",")
                temp_add_param = [temp_array.shift[0..-2]]
                # rest are int
                temp_add_param.push(temp_array[0].strip.to_i)
                temp_add_param.push(temp_array[1].strip.to_i)
                temp_add_param.push(temp_array[2].strip.to_i)
              when "ShowChoices"
                temp_command_code = 102
                templine_indent += 1
                temp_array = templine[15..-3].split("], ")
                default_choice = temp_array.pop.to_i
                temp_add_param = []
                temp_array[0][0..-2].split("\", \"").each do |choice|
                  temp_add_param.push(choice.gsub("\\\\C","\\C"))
                end
                temp_add_param = [temp_add_param]
                temp_add_param.push(default_choice)
              when "InputNumber"
                temp_command_code = 103
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ShowScrollingTextAttributes"
                temp_command_code = 105
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0].to_i]
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[1].strip == "true")
              when "Comment"
                temp_command_code = 108
                temp_add_param = [templine[10..-4]]
              when "ConditionalBranch"
                temp_command_code = 111
                templine_indent += 1
                # whatever the conditions are, they're stored as int
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
                # ...except for scripts, self switches, armors
                if temp_add_param[0] == 2
                  last_value = temp_add_param.pop
                  temp_add_param.pop
                  temp_add_param.push(temp_parameters[1].split(",")[1].strip.gsub("\"",""))
                  temp_add_param.push(last_value)
                elsif temp_add_param[0] == 9 or temp_add_param[0] == 10
                  temp_add_param.pop
                  temp_add_param.push(temp_parameters[1].split(",")[2][1..-2] == "true")
                elsif temp_add_param[0] == 12
                  temp_add_param.pop
                  temp_add_param.push(templine[24...-3].gsub("\\\"","\""))
                end
              when "Loop"
                temp_command_code = 112
                templine_indent += 1
              when "BreakLoop"
                temp_command_code = 113
              when "ExitEventProcessing"
                temp_command_code = 115
              when "CallCommonEvent"
                temp_command_code = 117
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "Label"
                temp_command_code = 118
                temp_add_param = [temp_parameters[1].gsub(/"/, '')[1..-2]]
              when "JumpToLabel"
                temp_command_code = 119
                temp_add_param = [temp_parameters[1].gsub(/"/, '')[1..-2]]
              when "ControlSwitches"
                temp_command_code = 121
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ControlVariables"
                temp_command_code = 122
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
                if temp_add_param[3] == 4
                  temp_add_param.pop
                  temp_add_param.push(temp_parameters[1].split(",")[-1][2..-3])
                end
              when "ControlSelfSwitch"
                temp_command_code = 123
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0][1..-2]]
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[1].to_i)
              when "ChangeGold"
                temp_command_code = 125
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeItems"
                temp_command_code = 126
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeWeapons"
                temp_command_code = 127
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_array_count = temp_array.count
                temp_boolean = (temp_array.pop.strip == "true") if temp_array_count > 4
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean) if temp_array_count > 4
              when "ChangeArmor"
                temp_command_code = 128
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_array_count = temp_array.count
                temp_boolean = (temp_array.pop.strip == "true") if temp_array_count > 4
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean) if temp_array_count > 4
              when "ChangePartyMember"
                temp_command_code = 129
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeBattleBGM"
                temp_command_code = 132
                temp_add_param = [RPG::BGM.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "ChangeSaveAccess"
                temp_command_code = 134
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ChangeEncounter"
                temp_command_code = 136
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "TransferPlayer"
                temp_command_code = 201
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "SetVehicleLocation"
                temp_command_code = 202
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "SetEventLocation"
                temp_command_code = 203
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ScrollMap"
                temp_command_code = 204
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "SetMoveRoute"
                temp_command_code = 205
                temp_init_number = temp_parameters[1].split(",")[0][1..-1].to_i
                temp_bool1 = temp_parameters[2].split(",")[0].split("=")[1].strip == "true"
                temp_bool2 = temp_parameters[2].split(",")[1].split("=")[1].strip == "true"
                temp_bool3 = temp_parameters[2].split(",")[2].split("=")[1].strip == "true"
                temp_move_command_list = []
                temp_parameters[2..-1].join().split("RPG::MoveCommand")[1..-1].each do |temp_move_command_params|
                  temp_move_code = temp_move_command_params.split("=")[1].split(",")[0].to_i
                  if temp_move_code == 44
                    temp_move_params = [RPG::SE.new(temp_move_command_params.split("=")[3].split("\"")[1].split("\\")[0], temp_move_command_params.split("=")[5].split(",")[0].to_i, temp_move_command_params.split("=")[4].split(",")[0].split("\\")[0].to_i)]
                  elsif temp_move_code == 41
                    temp_move_params = [temp_move_command_params.split("=")[2][1..-1].split("]")[0].split(",")[0].strip[4..-5], temp_move_command_params.split("=")[2][1..-1].split("]")[0].split(",")[1].to_i]
                  else
                    temp_move_params = temp_move_command_params.split("=")[2][1..-1].split("]")[0].split(",").map(&:to_i)
                  end
                  temp_move_command_list.push(RPG::MoveCommand.new(temp_move_code, temp_move_params))
                end
                temp_add_param = [temp_init_number, RPG::MoveRoute.new(temp_bool1,temp_bool2,temp_bool3, temp_move_command_list)]
              when "Unnamed"
                temp_command_code = 505
                temp_move_code = temp_parameters[2].split(",")[0].split("=")[1].to_i
                if temp_move_code == 44
                  temp_move_params = [RPG::SE.new(temp_parameters[3].split("=")[1].split(",")[0].split("\"")[1].split("\\")[0], temp_parameters[3].split("=")[3].to_i, temp_parameters[3].split("=")[2].split(",")[0].to_i)]
                elsif temp_move_code == 41
                  temp_move_params = [temp_parameters[2].split("=")[2][1..-2].split(",")[0].split("\"")[1].split("\\")[0], temp_parameters[2].split("=")[2][1..-2].split(",")[1].to_i]
                else
                  temp_move_params = temp_parameters[2].split("=")[2][1..-2].split(",").map(&:to_i)
                end
                temp_add_param = [RPG::MoveCommand.new(temp_move_code,temp_move_params)]
              when "ChangeTransparency"
                temp_command_code = 211
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ShowAnimation"
                temp_command_code = 212
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "ShotBalloonIcon"
                temp_command_code = 213
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "EraseEvent"
                temp_command_code = 214
              when "ChangePlayerFollowers"
                temp_command_code = 216
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "GatherFollowers"
                temp_command_code = 217
              when "FadeoutScreen"
                temp_command_code = 221
              when "FadeinScreen"
                temp_command_code = 222
              when "TintScreen"
                temp_command_code = 223
                red = temp_parameters[2].split("=")[1].split(",")[0].to_f
                green = temp_parameters[2].split("=")[2].split(",")[0].to_f
                blue = temp_parameters[2].split("=")[3].split(",")[0].to_f
                gray = temp_parameters[2].split("=")[4].to_f
                temp_add_param = [Tone.new(red, green, blue, gray), temp_parameters[3].split(",")[1].to_i, temp_parameters[3][1..-2].split(",")[2].strip=="true"]
              when "FlashScreen"
                temp_command_code = 224
                red = temp_parameters[2].split("=")[1].split(",")[0].to_f
                green = temp_parameters[2].split("=")[2].split(",")[0].to_f
                blue = temp_parameters[2].split("=")[3].split(",")[0].to_f
                alpha = temp_parameters[2].split("=")[4].to_f
                temp_add_param = [Color.new(red, green, blue, alpha), temp_parameters[3].split(",")[1].to_i, temp_parameters[3][1..-2].split(",")[2].strip=="true"]
              when "ShakeScreen"
                temp_command_code = 225
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "Wait"
                temp_command_code = 230
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ShowPicture"
                temp_command_code = 231
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i]
                # image filename
                temp_add_param.push(temp_array[1].gsub(/"/, '').strip)
                temp_add_param.push(*temp_array[2..-1].map(&:to_i))
              when "MovePicture"
                temp_command_code = 232
                temp_array = temp_parameters[1][1..-2].split(",")
                # last value is a boolean value, get that out of the way first
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = [temp_array[0].to_i]
                temp_add_param.push(nil) # 2nd value seems to always be null/nil
                temp_add_param.push(*temp_array[2..-1].map(&:to_i))
                temp_add_param.push(temp_boolean) # add the boolean value last
              when "ErasePicture"
                temp_command_code = 235
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "SetWeatherEffects"
                temp_command_code = 236
                temp_add_param = Array.new
                temp_array = temp_parameters[1][1..-2].split(",")
                if temp_array[0] == ":rain"
                  temp_add_param.push(:rain)
                elsif temp_array[0] == ":storm"
                  temp_add_param.push(:storm)
                elsif temp_array[0] == ":snow"
                  temp_add_param.push(:snow)
                else
                  temp_add_param.push(:none)
                end
                temp_add_param.push(temp_array[1].to_i)
                temp_add_param.push(temp_array[2].to_i)
                temp_add_param.push(temp_array[3].strip == "true")
              when "PlayBGM"
                temp_command_code = 241
                temp_add_param = [RPG::BGM.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[1].split("=")[1].to_i, temp_parameters[2].split(",")[2].split("=")[1].to_i)]
              when "FadeoutBGM"
                temp_command_code = 242
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "SaveBGM"
                temp_command_code = 243
              when "ReplayBGM"
                temp_command_code = 244
              when "PlayBGS"
                temp_command_code = 245
                temp_add_param = [RPG::BGS.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "FadeoutBGS"
                temp_command_code = 246
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "PlayME"
                temp_command_code = 249
                temp_add_param = [RPG::ME.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "PlaySE"
                temp_command_code = 250
                temp_add_param = [RPG::SE.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "ChangeMapDisplay"
                temp_command_code = 281
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ChangeTileset"
                temp_command_code = 282
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ChangeBattleBack"
                temp_command_code = 283
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0].strip[1..-2],temp_parameters[1][1..-2].split(",")[1].strip[1..-2]]
              when "ChangeParallaxBack"
                temp_command_code = 284
                temp_array = temp_parameters[1][1..-2].split(",")
                last_value = temp_array.pop.to_i
                second_to_last_value = temp_array.pop.to_i
                temp_bool_last = (temp_array.pop.strip == "true")
                temp_bool_first = (temp_array.pop.strip == "true")
                temp_add_param = [temp_array[0][1..-2].strip, temp_bool_first, temp_bool_last, second_to_last_value, last_value]
              when "BattleProcessing"
                temp_command_code = 301
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_bool_last = (temp_array.pop.strip == "true")
                temp_bool_first = (temp_array.pop.strip == "true")
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_bool_first)
                temp_add_param.push(temp_bool_last)
              when "ShopProcessing"
                temp_command_code = 302
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "ChangeHP"
                temp_command_code = 311
                temp_add_param = temp_parameters[1][1..-2].split(",")[0..-2].map(&:to_i)
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[-1].strip == "true")
              when "ChangeState"
                temp_command_code = 313
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "RecoverAll"
                temp_command_code = 314
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeLevel"
                temp_command_code = 316
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "ChangeParameters"
                temp_command_code = 317
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeSkills"
                temp_command_code = 318
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeActorName"
                temp_command_code = 320
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i, temp_array[1][2..-2]]
              when "ChangeActorClass"
                temp_command_code = 321
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeActorGraphic"
                temp_command_code = 322
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i]
                temp_add_param.push(temp_array[1][2..-2])
                temp_add_param.push(temp_array[2].to_i)
                temp_add_param.push(temp_array[3][2..-2])
                temp_add_param.push(temp_array[4].to_i)
              when "ChangeVehicleGraphic"
                temp_command_code = 323
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i]
                temp_add_param.push(temp_array[1][2..-2])
                temp_add_param.push(temp_array[2].to_i)
              when "ChangeEnemyState"
                temp_command_code = 333
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ShowBattleAnimation"
                temp_command_code = 337
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ForceAction"
                temp_command_code = 339
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "AbortBattle"
                temp_command_code = 340
              when "OpenMenuScreen"
                temp_command_code = 351
              when "OpenSaveScreen"
                temp_command_code = 352
              when "ReturnToTitleScreen"
                temp_command_code = 354
              when "Script"
                # start custom script
                temp_command_code = 355
                temp_add_param = [templine[9..-4].gsub('\\"','"')]
              when "ScriptMore"
                # continue custom script
                temp_command_code = 655
                temp_add_param = [templine[13..-4].gsub('\\"','"')]
              when "ShowText"
                temp_command_code = 401
                temp_string = templine[11..-4]
                if temp_string.length > 5
                  temp_string = temp_string.split("\") //")[0]
                  temp_string.gsub!("\\\\","\\")
                  temp_string = temp_string[0..4] + temp_string[5..-1].gsub("\\n","\n").gsub("\n[","\\n[").gsub("\nc[","\\nc[").gsub("\na[","\\na[").gsub("\nw[","\\nw[").gsub("\ni[","\\ni[")
                  temp_string = temp_string.gsub("\\\"","\"") if temp_string.include?("\\\"")
                end
                next if temp_string == ""
                temp_add_param = [temp_string]
              when "When"
                temp_command_code = 402
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0].to_i]
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[1][2..-2].gsub("\\\\C","\\C"))
              when "WhenCancel"
                temp_command_code = 403
              when "ChoicesEnd"
                temp_command_code = 404
                templine_indent -= 1
              when "ShowScrollingText"
                temp_command_code = 405
                temp_add_param = [templine[20..-4]]
              when "CommentMore"
                temp_command_code = 408
                temp_add_param = [templine[14..-4]]
              when "Else"
                temp_command_code = 411
              when "BranchEnd"
                temp_command_code = 412
                templine_indent -= 1
              when "RepeatAbove"
                temp_command_code = 413
                templine_indent -= 1
              when "IfWin"
                temp_command_code = 601
                templine_indent += 1
              when "IfEscape"
                temp_command_code = 602
              when "BattleProcessingEnd"
                temp_command_code = 604
                templine_indent -= 1
              when "ShopItem"
                temp_command_code = 605
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              else
                p "unknown templine #{templine}" if $TEST
              end

              if [102,111,112,402,403,411,601,602].include?(temp_command_code)
                temp_command_list.push(RPG::EventCommand.new(temp_command_code,templine_indent-1,temp_add_param))
              else
                temp_command_list.push(RPG::EventCommand.new(temp_command_code,templine_indent,temp_add_param))
              end
            end
            break unless temp_event_lines.length > 0
          end
          break unless temp_event_lines.length > 0
        end
      end
    end
    return [temp_command_list, event_name]
  end

  alias nw_load_normal_database load_normal_database
  def load_normal_database
    nw_load_normal_database
    loading_title = "Loading System Data... please wait."
    Graphics.change_title(loading_title)
    convert_placenames if File.exists?("Patch\\static_translations.rb")
    load_skills_from_file("Translated Files\\Skills.txt")
    load_states_from_file("Translated Files\\States.txt")
    load_items_from_file("Translated Files\\Items.txt")
    load_armors_from_file("Translated Files\\Armors.txt")
    load_weapons_from_file("Translated Files\\Weapons.txt")
    load_classes_from_file("Translated Files\\Classes.txt")
    load_actors_from_file("Translated Files\\Actors.txt")
    load_enemies_from_file("Translated Files\\Enemies.txt")
    load_troops_from_file("Translated Files\\Troops.txt") if File.exists?("Patch\\translator_core_maps.rb")
    load_terms_from_file("Translated Files\\System\\Terms.txt")
    load_skill_types_from_file("Translated Files\\System\\Skill Types.txt")
    load_weapon_types_from_file("Translated Files\\System\\Weapon Types.txt")
    load_armor_types_from_file("Translated Files\\System\\Armor Types.txt")
    load_elements_from_file("Translated Files\\System\\Elements.txt")
    # fix for common event numbering
    current_event_id = 0
    $data_common_events[1...-1].each do |event|
      current_event_id += 1
      event.id = current_event_id if event.id == 0
      if current_event_id.between?(1001,2998)
        unless event.nil?
          events_from_file = get_events_from_file("Translated Files\\CommonEvents\\CommonEvent#{event.id}.txt", event.id)
          new_list = events_from_file[0]
          new_list = event.list if (!new_list.nil? and new_list.empty? and !event.list.nil?)
          unless new_list.nil?
            loading_title = "Loading Common Event Data [#{((event.id - 1001) * 100 / 1998)}%]"
            Graphics.change_title(loading_title)
            event.name = events_from_file[1] unless events_from_file[1].empty?
            event.list = new_list unless new_list.empty?
          end
        end
      end
    end
  end
end
class Game_Interpreter
  def command_117
    common_event = $data_common_events[@params[0]]
    if common_event
      new_list = DataManager.get_events_from_file("Translated Files\\CommonEvents\\CommonEvent#{@params[0]}.txt", @params[0])[0]
      common_event.list = new_list unless (new_list.nil? && new_list.empty?)
      
      # Default functionality, do not touch
      child = Game_Interpreter.new(@depth + 1)
      child.setup(common_event.list, same_map? ? @event_id : 0)
      child.run
    end
  end
end
class Game_Novel
  def setup(event_id)
    @event_id = event_id
    new_list = DataManager.get_events_from_file("Translated Files\\CommonEvents\\CommonEvent#{event_id}.txt", event_id)[0]
    if (!new_list.nil? && !new_list.empty?)
      @interpreter.setup(new_list)
    else
      @interpreter.setup($data_common_events[@event_id].list)
    end
  end
end
class Game_CommonEvent
  def initialize(common_event_id)
    new_list = DataManager.get_events_from_file("Translated Files\\CommonEvents\\CommonEvent#{common_event_id}.txt", common_event_id)[0]
    $data_common_events[common_event_id].list = new_list unless (new_list.nil? && new_list.empty?)
    @event = $data_common_events[common_event_id]
    refresh
  end
end
class Game_Temp
  def reserved_common_event
    common_event_id = common_event_reserved? ? @common_events[0] : 0
    new_list = DataManager.get_events_from_file("Translated Files\\CommonEvents\\CommonEvent#{common_event_id}.txt", common_event_id)[0]
    $data_common_events[common_event_id].list = new_list unless (new_list.nil? && new_list.empty?)
    return $data_common_events[common_event_id]
  end
end
class Game_Interpreter
  def setup_reserved_common_event
    if $game_temp.common_event_reserved?
      new_list = DataManager.get_events_from_file("Translated Files\\CommonEvents\\CommonEvent#{$game_temp.reserved_common_event.id}.txt", $game_temp.reserved_common_event.id)[0]
      $game_temp.reserved_common_event.list = new_list unless (new_list.nil? && new_list.empty?)
      setup($game_temp.reserved_common_event.list)
      $game_temp.shift_common_event
      true
    else
      false
    end
  end
end