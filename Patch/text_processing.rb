class Window_Base
  def process_character(c, text, pos)
    case c
    when "\n"   # 改行
      process_new_line(text, pos)
    when "\f"   # 改ページ
      process_new_page(text, pos)
    when "\e"   # 制御文字
      process_escape_character(obtain_escape_code(text), text, pos)
    when "\g"
      if text.start_with?("[")
        process_escape_graphics(text, pos)
      else
        process_normal_character(c, pos)
      end
    else        # 普通の文字
      process_normal_character(c, pos)
    end
  end
end

class Word
  def replace_name(b, a)
    return if @common

    w = Marshal.load(Marshal.dump(self))
    w.instance_eval do
      @words.gsub("【".force_encoding('UTF-8').encode, "<").gsub("】".force_encoding('UTF-8').encode, ">").gsub(/<#{b}>/, "\\n<#{a}>")
    end
    w
  end
end

#==============================================================================
# ■ Scene_Title
#==============================================================================
class Scene_Title < Scene_Base
  #--------------------------------------------------------------------------
  # ● 前景の作成
  #--------------------------------------------------------------------------
  def create_foreground
    @foreground_sprite = Sprite.new
    @foreground_sprite.bitmap = Bitmap.new(Graphics.width, Graphics.height)
    @foreground_sprite.z = 100
    draw_game_title if $data_system.opt_draw_title
    @foreground_sprite.bitmap.font.size = 24
    rect = Rect.new(4, 0, Graphics.width, @foreground_sprite.bitmap.font.size)
    @foreground_sprite.bitmap.draw_text(rect, NWPatch.ver_str)
  end
end


#==============================================================================
#    Global Text Codes [VXA]
#    Version: 1.0a
#    Author: modern algebra (rmrk.net)
#    Date: April 5, 2012
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#  Description:
#
#    This script allows you to use special message codes in any window, not
#   just message windows and help windows. Want to add an icon next to the 
#   menu commands? With this script you can do that and more.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#  Instructions:
#
#    Simply paste this script into its own slot above Main and below Materials
#   and other custom scripts.
#
#    There are two settings for this script - automatic and manual. If set to
#   automatic, then all you will need to do is put any of the special message
#   codes in anything and they will automatically work. If set to manual, then
#   you will also need to type the following code somewhere in the text field 
#   to activate it: \*
#
#    The following default codes are available:
#
# \c[n] - Set the colour of the text being drawn to the nth colour of the 
#     Windowskin palette. 0 is the normal color and 16 is the system color.
# \i[n] - Draw icon with index n.
# \p[n] - Draw the name of the actor in the xth position in the party. 1 is 
#     the party leader, 2 is the second member, etc.
# \n[n] - Draw the name of the actor with ID n
# \v[n] - Draw the value of variable with ID n.
# \g - Draws the unit of currency.
#
#    Depending on whether you are using any custom message script, you may have 
#   additional message codes at your disposal. This script is mostly compatible
#   with my ATS and every code except \x and ones related to message control 
#   will work.
#==============================================================================

$imported = {} unless $imported
$imported[:MAGlobalTextCodes] = true
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#  Editable Region
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
#  MAGTC_MANUAL_CODES - If this is true, then you must put a \* code in any 
# field that you want to have codes interpreted in. Otherwise, codes will 
# always automatically be interpreted. The recommended value for this is true,
# as the process for drawing text with codes is much slower than the process 
# for drawing text without codes.
MAGTC_MANUAL_CODES = true
#  MAGTC RCODES - This feature is designed to overcome the space limitations in
# much of the database - since codes take so much of that space, it might be
# difficult to write everything you want into one of those fields. This feature
# permits you to write the term you want in to the following array, and then 
# access it in the database with the code \r[n], where n is the ID you assign 
# to the phrase in the following way:
#
#    n => "replacement",
#
# Please note that: it is =>, not =; the replacement must be within quotation 
# marks; and there must be a comma after every line. If using double quotation
# marks ("", not ''), then you need to use two backslashes to access codes 
# instead of one (\\c[1], not \c[1]).
#
#  EXAMPLE:
#   0 => "\\i[112]\\c[14]New Game\\c[0]",
#
#  Under the New Game field in the Terms of the Database, all I would then need 
# to put is:
#    \*\r[0]
#  and it would be the same as if I had put:
#    \*\i[112]\c[14]New Game\c[0]
MAGTC_RCODES = { # <- Do not touch
  0 => "\\i[112]\\c[14]New Game\\c[0]", # Example
  1 => "", # You can make as many of these as you want
  2 => "\\i[40]Blood Hound",
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
#  END Editable Region
#//////////////////////////////////////////////////////////////////////////////
}
MAGTC_RCODES.default = ""

#==============================================================================
# ** Window_Base
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#  Summary of Changes:
#    aliased methods - draw_text; convert_escape_characters; process_new_line;
#      reset_font_settings
#    new methods - magtc_align_x; magtc_test_process_escape_character;
#      magtc_calc_line_width
#==============================================================================

class Window_Base
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Draw Text
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias ma_gtc_drwtxt_3fs1 draw_text
  def draw_text(*args, &block)
    # Get the arguments
    if args[0].is_a?(Rect)
        x, y, w, h = args[0].x, args[0].y, args[0].width, args[0].height
        text, align = *args[1, 2]
      else
        x, y, w, h, text, align = *args[0, 6]
      end
    align = 0 unless align
    #  Draw normally if text is not a string, draw normally if the text is not
    # long enough to hold a code, and draw normally when the script is set to 
    # manual and \* is included in the text
      if !text.is_a?(String) || text.size < 2 || (MAGTC_MANUAL_CODES && text[/\\\*/].nil?)
        ma_gtc_drwtxt_3fs1(*args, &block) # Run Original Method
      else
      @magtc_reset_font = contents.font.dup # Do not automatically reset font
      @magtc_rect, @magtc_align = Rect.new(x, y, w, h), align
      # Get first line of the text to test for alignment
      @magtc_test_line = convert_escape_characters(text[/.*/])
      y += [(h - calc_line_height(@magtc_test_line)) / 2, 0].max
      # Draw text with message codes
        draw_text_ex(magtc_align_x(x), y, text)
      @magtc_reset_font = nil # Do not automatically reset font
      @magtc_rect, @magtc_align = nil, nil # Reset Rect and Alignment
      end
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Convert Escape Characters
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias ma_gtc_convescchar_5tk9 convert_escape_characters
  def convert_escape_characters(text, *args, &block)
    # Remove \* codes
    new_text = text.gsub(/\\\*/, "")
    # Substitute for the R Codes
    new_text.gsub!(/\\[Rr]\[(\d+)\]/) { MAGTC_RCODES[$1.to_i].to_s }
    ma_gtc_convescchar_5tk9(new_text, *args, &block) # Call Original Method
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Reset Font Settings
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias magtc_resetfonts_4ga5 reset_font_settings
  def reset_font_settings(*args, &block)
    magtc_resetfonts_4ga5(*args, &block) # Call Original Method
    contents.font = @magtc_reset_font if @magtc_reset_font
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Process New Line
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  alias magtc_prcsnewl_5gn9 process_new_line
  def process_new_line(text, pos, *args, &block)
    magtc_prcsnewl_5gn9(text, pos, *args, &block) # Run Original Method
    if @magtc_align && @magtc_rext
      @magtc_test_line = text[/.*/] # Get new line
      pos[:x] = magtc_align_x       # Get the correct x, depending on alignment
    end
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Get Alignment X
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def magtc_align_x(start_x = @magtc_rect.x)
    return start_x unless (@magtc_rect && @magtc_align && @magtc_test_line) || @magtc_align != 0
    tw = magtc_calc_line_width(@magtc_test_line)
    case @magtc_align
    when 1 then return start_x + ((@magtc_rect.width - tw) / 2)
    when 2 then return start_x + (@magtc_rect.width - tw)
    end
    start_x
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Calc Line Width
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def magtc_calc_line_width(line)
    # Remove all escape codes
    line = line.clone
    line.gsub!(/[\n\r\f]/, "")
    real_contents = contents # Preserve Real Contents
    # Create a dummy contents
    self.contents = Bitmap.new(@magtc_rect.width, @magtc_rect.height)
    reset_font_settings
    pos = {x: 0, y: 0, new_x: 0, height: calc_line_height(line)}
    tw = 0
    while line[/^(.*?)\e(.*)/]
      tw += text_size($1).width
      line = $2
      # Remove all ancillaries to the code, like parameters
      code = obtain_escape_code(line)
      magtc_test_process_escape_character(code, line, pos)
    end
    #  Add width of remaining text, as well as the value of pos[:x] under the 
    # assumption that any additions to it are because the special code is 
    # replaced by something which requires space (like icons)
    tw += text_size(line).width + pos[:x]
    self.contents.dispose # Dispose dummy contents
    self.contents = real_contents # Restore real contents
    return tw
  end
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # * Test Process Escape Character
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  def magtc_test_process_escape_character(code, text, pos)
    if $imported[:ATS_SpecialMessageCodes] && ['X', 'HL'].include?(code.upcase)
      obtain_escape_param(text)
      return
    end
    process_escape_character(code, text, pos)
  end
end

#==============================================================================
# EX's scripts for overwriting enemy/place/etc names.
# - * - Encoding: utf-8 - * -
=begin
=Extension Script(s) For MGQ Paradox
=Library Note Name Data setting section by MGQ EX

Adds Features For MGQ Paradox
* ENEMY_SPECIAL_NAME		Used To Supplement Enemy Note: <図鑑名称:
* RACE_SPECIAL_NAME			Used To Supplement Category & Note: <カテゴリー：
* PLACE_NAME				Used To Supplement Place Names
* Various Word Wrap Corrections
* Includes Scripts: YEA - Ace Message System (Unofficial Edit)
					Basic Window Resizer v1.1 (Unofficial Edit)

== Update history
Date Version Author Comment
08/14/2015 3.0,0 MGQ EX


=end



#Overwrite Script:  図鑑/本体
#==============================================================================
# ■ Window_Library_RightMain
#==============================================================================
class Window_Library_RightMain < Window_Selectable
  #--------------------------------------------------------------------------
  # ● キャラ説明描画(アクター, エネミー)
  #--------------------------------------------------------------------------
  def draw_chara_description(y, chara)
    # 描画域の生成
    rect = standard_rect(y)
    rect.height = self.contents.height - rect.y
    @dest_rect = rect
    @src_rect.y = 0 if @last_ext != @ext
    # 解説文章の取得
    desc = ["No Details"]
    if chara.is_a?(RPG::Enemy)
      key = chara.id
      desc = ENEMY_DESCRIPTION[key] if ENEMY_DESCRIPTION.key?(key)
    end

    # Generating a bit map of commentary text (key to the cacheext)
    if @description[@ext].nil? || @description[@ext].disposed?
      @description[@ext] = Bitmap.new(rect.width, desc.size * line_height)
      @src_rect = @description[@ext].rect
      i = 0
#=begin
      #Add WordWrap For English Text
        str = ""
        #Get Description From Array To String
        desc.each{|txt|
          if txt == ""
            str = str + "\n\n"
          else
            str = str + txt + " "
          end
        }
        #Process Actor
        if chara.is_a?(RPG::Actor)
          desc = []
          #Change String To Line Array
          str2 = str.split("\n")
          str = ""
          str2.each{|s|
            #If Line Blank Retain Else Reformat Line
            if s == ""
                str = str + "\n\n"
            else
                str = str + reformat_wrapped(s)
            end
          }
          str2 = ""
          #Take Reformatted Description And Overwrite Old
          str2 = str.split("\n")
          str2.each{|s|
            if s == ""
                desc<<""
            else
                desc<<s
            end
          }
        
        #Process Enemy
        if chara.is_a?(RPG::Enemy)
          desc = []
          #Change String To Line Array
          str2 = str.split("\n")
          str = ""
          str2.each{|s|
            #If Line Blank Retain Else Reformat Line
            if s == ""
                str = str + "\n\n"
            else
                str = str + reformat_wrapped(s)
            end
            }
          str2 = ""
          #Take Reformatted Description And Overwrite Old
          str2 = str.split("\n")
          #File.open("Library/Enemy.txt", "a") { |f|
          #  f.write("<<enemy_description/"+key.to_s+"/note>>"+"\n")
            str2.each{|s|
              if s == ""
                  desc<<""
          #        f.write("\n")
              else
                  desc<<s
          #        f.write(s + "\n")
              end
              }
          #  f.write("\n")
          #}
      end
      
      #Recalculate Dimensions Of Content
      @description[@ext] = Bitmap.new(rect.width, desc.size * line_height)
      @src_rect = @description[@ext].rect
end

      desc.each{|txt|
        r = standard_rect(i * line_height)
        @description[@ext].draw_text(r, txt)
        i += 1
      }
    end
    bmp = @description[@ext]
    # ビットマップの範囲を設定
    @src_rect.height = bmp.height - @src_rect.y
    # 解説文章を描画域へ転送する。
    self.contents.blt(@dest_rect.x, @dest_rect.y, bmp, @src_rect)
    # スクロールカーソルを転送する。
    # 上
    if @src_rect.y > 0 && bmp.height > @dest_rect.height
      arrow_rect = Rect.new(88, 14, 14, 10)
      self.contents.blt((self.contents.width - 14) / 2, rect.y - 10,
        self.windowskin, arrow_rect)
    end
    # 下
    if @src_rect.height > @dest_rect.height
      arrow_rect = Rect.new(88, 38, 14, 10)
      self.contents.blt((self.contents.width - 14) / 2, rect.y + rect.height - 10,
        self.windowskin, arrow_rect)
    end
    # 戻る
    reset_font_settings
    return rect.y + rect.height
  end
  
  #--------------------------------------------------------------------------
  # ● 自動改行テキスト表示
  #--------------------------------------------------------------------------
  def draw_text_auto_line(rect, text)
    array = []
    s = ""    
    s = reformat_wrapped(text).split("\n")
    s.each{|str| array.push(str)}
    #array.push(reformat_wrapped(text).split("\n"))
    array.each{|str| draw_text(rect, str); rect.y += rect.height}
    return rect
  end
  
  #--------------------------------------------------------------------------
  # ● 自動改行テキスト表示　配列の区切りで改行
  #--------------------------------------------------------------------------
  def draw_text_auto_line_ex(rect, text)
    array = []
    s = ""
    s = reformat_wrapped(text).split("\n")
    s.each{|str| array.push(str)}
    array.each{|str| draw_text(rect, str); rect.y += rect.height}
    return rect
  end
  
  def reformat_wrapped(s, width=35)
    lines = []
    line = ""
    s.split(/\s+/).each do |word|
      if line.size + word.size >= width
        lines << line
        line = word
      elsif line.empty?
       line = word
      else
       line << " " << word
     end
     end
     lines << line if line
    return lines.join "\n"
  end
  
  #--------------------------------------------------------------------------
  # ● Unique Actor Abilities
  #--------------------------------------------------------------------------
  def draw_actor_fix_ability(y, actor)
    fix_abilities = ACTOR_FIX_ABILITY[actor.id]
    return y unless fix_abilities
    rect = standard_rect(y)
    reset_font_settings
    
    change_color(system_color)
    draw_text(rect, FIX_ABILITY_NAME)
    rect.y += rect.height
    change_color(special_color)
    draw_text(rect, fix_abilities.first)
    rect.y += rect.height
    change_color(normal_color)
    
    all_text = ""
    fix_abilities[1...fix_abilities.size].each{|fix_ability|
      all_text += fix_ability + " "
    }
    all_text.slice!(-1, 1)
    rect = draw_text_auto_line_ex(rect, all_text)
    return rect.y
  end
  
  #--------------------------------------------------------------------------
  # ● Draw Common Parts (Weapons, Armor, Accessories, Items)
  #--------------------------------------------------------------------------
  def draw_items_common(item)
    rect = standard_rect
    reset_font_settings
    # アイテム名の描画
    draw_item_name(item, rect.x, rect.y)
    rect.y = self.contents.height - (line_height * 6) # Original 5
    
    # 解説の描画
    change_color(system_color)
    draw_text(rect, "Details")
    rect.y += rect.height
    change_color(normal_color)
    all_text = ""
    item.description.each_line do |d|
      d.slice!(/\[(.*?)\]/)
      d.chomp!
      next if d == ""
      all_text += d
      all_text += "\n"
    end
    rect = draw_text_auto_line_ex(rect, all_text)
    
    return line_height + LINE_HEIGHT
  end
end



#==============================================================================
# ■ Game_Actor
#==============================================================================
class Game_Actor < Game_Battler
#--------------------------------------------------------------------------
  # ○ レベルアップメッセージの表示
  #     new_skills : 新しく習得したスキルの配列
  #--------------------------------------------------------------------------
  def display_level_up(new_skills, kind)
    $game_message.new_page
    case kind
      when :base;  prefix = Vocab::level
      when :class; prefix = self.class.name + ' level'
      when :tribe; prefix = self.tribe.name + ' level'
    end

    @name = actor.name if @name == nil
    $game_message.add(sprintf(Vocab::LevelUp, @name, prefix, @level[kind]))
    new_skills.each { |skill|
      # ひきも記の「メッセージ制御文字拡張」競合対策
      $game_message.add(sprintf(Vocab::ObtainSkill, "\\S[#{skill.id}]"))
    }
  end
end

#==============================================================================
# ■ Scene_Ability
#==============================================================================
class Scene_Ability < Scene_MenuBase
  #--------------------------------------------------------------------------
  # ● Start Screen/Ability Editor Edit 518
  #--------------------------------------------------------------------------
  alias :ex_alt_start :start
  def start
    ex_alt_start
    # Add Correction For Height & Alignment
    correction
  end
  
  #--------------------------------------------------------------------------
  # ● Corrects Height & Alignment
  #--------------------------------------------------------------------------
  def correction
    #Help Window + Ability Window
    top_area = @help_window.height + @ability_type_window.height
    #Help Window X Lines
    #Ability Window Align To Help Window
    @ability_type_window.y = @help_window.height
    # Align Middle Windows To Ability Window
    @equip_ability_window.y = top_area
    @stand_ability_window.y = top_area
    #Shrink Middle Windows To Fit Between Second & Bottom
    @stand_ability_window.height = Graphics.height - (top_area + @key_help_window.height)
    @equip_ability_window.height = Graphics.height - (top_area + @key_help_window.height)
  end
  
  #--------------------------------------------------------------------------
  # ● Create Alternate Ability Help Window
  #--------------------------------------------------------------------------
  def create_ability_help_window
    @help_window = Window_Help.new(3)
    @help_window.viewport = @viewport
  end
end

class Window_Message < Window_Base
  alias_method :original_process_normal_character, :process_normal_character
  def process_normal_character(c, pos)
    @reduce_flag = false
    original_process_normal_character(c, pos)
  end
end

#==============================================================================
# ▼ Word Wrapping and Squishing Fixes for MGQ Part 3 By JoSmiHnTh
#==============================================================================
class Window_Help < Window_Base
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(line_number = 4)
    super(0, 0, Graphics.width, fitting_height(line_number))
  end

  def refresh
    contents.clear
    counter = -1
    #@lines.size.times {|i|
    @text.lines.each_with_index {|line, index|
      text = reformat_wrapped(line)
      txt_array = text.split("\n")
      if txt_array.size <= 1
        counter += 1
        #draw_line(index)
        draw_line_ex(counter, text)
      else
        txt_array.each{|new_line|
          counter += 1
          draw_line_ex(counter, new_line)
        }
      end
    }
  end

  def draw_line_ex(line_number, text)
    rect = rect = Rect.new(4, 0 + (line_number * line_height), contents.width - 4, line_height)
    contents.clear_rect(rect)
    draw_text_ex(rect.x, rect.y, text)
  end
  
  def set_text(text)
    if text != @text
      @text = reformat_wrapped(text)
      refresh
    end
  end

  def reformat_wrapped(s, width=60)
    lines = []
    line = ""
    line_string = []
    string1 = ""
    
    #Handle Target Items Differently   
    if s.include?("Target:") || s.include?("Power:") || s.include?("Effect:")
      #Handle These Line(s) Differently
      s.split(/\r?\n/).each_with_index do |item, i|
        if item.include?("Power:")
          line_string << item
        elsif item.include?("Target:")
          line_string << item
        elsif item.include?("Effect:")
          line_string << item
        else
          #Common Stuff
          string1 += item + " "
        end
      end
    else
      s.split(/\r?\n/).each_with_index do |item, i|
        #Common Stuff
        string1 += item + " "
      end
    end
    
    # Add Common Stuff To End
    line_string << string1 if string1 != ""
    
    # Reset String 1 For Formatting
    string1 = "" 
    
    # Process All Lines
    line_string.each{|string_x|
      lines = []
      line = ""
      string_x.split(/\s+/).each do |word|
        if line.size + word.size >= width
          lines << line
          line = word
        elsif line.empty?
         line = word
        else
         line << " " << word
        end
      end
      
      lines << line if line
      string_x = lines.join "\n"
      if string1 == ""
        string1 += string_x
      else
        string1 += "\n"+string_x
      end
    }

    return string1
  end
end

#==============================================================================
# ■ Foo::JobChange::Window_SortEval
#==============================================================================
class Foo::JobChange::Window_SortEval < Window_Base
  #--------------------------------------------------------------------------
  # ● Display Sorting Method (New)
  #--------------------------------------------------------------------------
  def draw_eval
    texts = {
      :id => "ID Order",
      :name => "By Name"
    }
    
    if texts.include?(eval)
      text = texts[eval]
    else
      # Added To Translate Categories
      text = eval.to_s
      NWConst::Library::RACE_SPECIAL_NAME.each{|jap_name, tran_name| 
        if jap_name.to_s.force_encoding('UTF-8').encode == text.to_s
          text = tran_name
          break
        end
      }
    end
    
    change_color(system_color)
    rect = Rect.new(0,0,self.contents.width,line_height)
    draw_text(rect, text, 1)
  end
end

#==============================================================================
# ■ Foo::PTEdit::Window_SortEval
#==============================================================================
class Foo::PTEdit::Window_SortEval < Window_Base
  #--------------------------------------------------------------------------
  # ● Display Sorting Method (New)
  #--------------------------------------------------------------------------
  def draw_eval
    texts = {
      :id => "ID Order",
      :name => "By Name"
    }
    
    if texts.include?(eval)
      text = texts[eval]
    else
      # Added To Translate Categories
      text = eval.to_s
      NWConst::Library::RACE_SPECIAL_NAME.each{|jap_name, tran_name| 
        if jap_name.to_s == text.to_s
          text = tran_name
          break
        end
      }
    end
    
    change_color(system_color)
    rect = Rect.new(0,0,self.contents.width,line_height)
    draw_text(rect, text, 1)
  end
end

#==============================================================================
# 
# ▼ Yanfly Engine Ace - Ace Message System v1.05
# -- Last Updated: 2012.01.13
# -- Level: Normal
# -- Requires: n/a
# 
#==============================================================================

$imported = {} if $imported.nil?
$imported["YEA-MessageSystem"] = true

#==============================================================================
# ▼ Updates
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# 2015.08.10 - Unofficial Update For Paradox 1.21
# 2012.07.21 - Fixed REGEXP error at line 824
# 2012.01.13 - Bug Fixed: Negative tags didn't display other party members.
# 2012.01.12 - Compatibility Update: Message Actor Codes
# 2012.01.10 - Added Feature: \pic[x] text code.
# 2012.01.04 - Bug Fixed: \ic tag was \ii. No longer the case.
#            - Added: Scroll Text window now uses message window font.
# 2011.12.31 - Started Script and Finished.
# 
#==============================================================================
# ▼ Introduction
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# While RPG Maker VX Ace certainly improved the message system a whole lot, it
# wouldn't hurt to add in a few more features, such as name windows, converting
# textcodes to write out the icons and/or names of items, weapons, armours, and
# more in quicker fashion. This script also gives the developer the ability to
# adjust the size of the message window during the game, give it a separate
# font, and to give the player a text fast-forward feature.
# 
#==============================================================================
# ▼ Instructions
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# To install this script, open up your script editor and copy/paste this script
# to an open slot below ▼ Materials/素材 but above ▼ Main. Remember to save.
# 
# -----------------------------------------------------------------------------
# Message Window text Codes - These go inside of your message window.
# -----------------------------------------------------------------------------
#  Default:    Effect:
#    \v[x]     - Writes variable x's value.
#    \n[x]     - Writes actor x's name.
#    \p[x]     - Writes party member x's name.
#    \g        - Writes gold currency name.
#    \c[x]     - Changes the colour of the text to x.
#    \i[x]     - Draws icon x at position of the text.
#    \{        - Makes text bigger by 8 points.
#    \}        - Makes text smaller by 8 points.
#    \$        - Opens gold window.
#    \.        - Waits 15 frames (quarter second).
#    \|        - Waits 60 frames (a full second).
#    \!        - Waits until key is pressed.
#    \>        - Following text is instant.
#    \<        - Following text is no longer instant.
#    \^        - Skips to the next message.
#    \\        - Writes a "\" in the window.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Wait:       Effect:
#    \w[x]     - Waits x frames (60 frames = 1 second). Message window only.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  NameWindow: Effect:
#    \n<x>     - Creates a name box with x string. Left side. *Note
#    \nc<x>    - Creates a name box with x string. Centered. *Note
#    \nr<x>    - Creates a name box with x string. Right side. *Note
# 
#              *Note: Works for message window only.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Position:   Effect:
#    \px[x]    - Sets x position of text to x.
#    \py[x]    - Sets y position of text to y.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Picture:    Effect:
#    \pic[x]   - Draws picture x from the Graphics\Pictures folder.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Outline:    Effect:
#    \oc[x]    - Sets outline colour to x.
#    \oo[x]    - Sets outline opacity to x.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Font:       Effect:
#    \fr       - Resets all font changes.
#    \fz[x]    - Changes font size to x.
#    \fn[x]    - Changes font name to x.
#    \fb       - Toggles font boldness.
#    \fi       - Toggles font italic.
#    \fo       - Toggles font outline.
#    \fs       - Toggles font shadow.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Actor:      Effect:
#    \af[x]    - Shows face of actor x. *Note
#    \ac[x]    - Writes out actor's class name. *Note
#    \as[x]    - Writes out actor's subclass name. Req: Class System. *Note
#    \an[x]    - Writes out actor's nickname. *Note
# 
#              *Note: If x is 0 or negative, it will show the respective
#               party member's face instead.
#                   0 - Party Leader
#                  -1 - 1st non-leader member.
#                  -2 - 2nd non-leader member. So on.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Salvaged From ATS: Special Message Codes [VXA] by modern algebra (rmrk.net)
#
#  Enemy:      Effect:
#    \en[x]    - Writes out enemy's name.
#
#  Party:      Effect:
#    \pg       - Draws the amount of money the party has.
#    \g        - Draws the unit of currency.
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Names:      Effect:
#    \nc[x]    - Writes out class x's name.
#    \ni[x]    - Writes out item x's name.
#    \nw[x]    - Writes out weapon x's name.
#    \na[x]    - Writes out armour x's name.
#    \ns[x]    - Writes out skill x's name.
#    \nt[x]    - Writes out state x's name.
#    \ne[x]    - Writes out element x's name.
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
#  Icon Names: Effect:
#    \ic[x]    - Writes out class x's name including icon. *
#    \ii[x]    - Writes out item x's name including icon.
#    \iw[x]    - Writes out weapon x's name including icon.
#    \ia[x]    - Writes out armour x's name including icon.
#    \is[x]    - Writes out skill x's name including icon.
#    \it[x]    - Writes out state x's name or icon.
#    \ie[x]    - Writes out element x's name or icon.
# 
#              *Note: Requires YEA - Class System
# 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# 
# And those are the text codes added with this script. Keep in mind that some
# of these text codes only work for the Message Window. Otherwise, they'll work
# for help descriptions, actor biographies, and others.
# 
#==============================================================================
# ▼ Compatibility
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# This script is made strictly for RPG Maker VX Ace. It is highly unlikely that
# it will run with RPG Maker VX without adjusting.
# 
#==============================================================================

module YEA
  module MESSAGE
    
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # - General Message Settings -
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # The following below will adjust the basic settings and that will affect
    # the majority of the script. Adjust them as you see fit.
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # This button is the button used to make message windows instantly skip
    # forward. Hold down for the effect. Note that when held down, this will
    # speed up the messages, but still wait for the pauses. However, it will
    # automatically go to the next page when prompted.
    #Disabled for Paradox, A on keyboard works for this.
    #TEXT_SKIP = :A     # Input::A is the shift button on keyboard.
    
    # This variable adjusts the number of visible rows shown in the message
    # window. If you do not wish to use this feature, set this constant to 0.
    # If the row value is 0 or below, it will automatically default to 4 rows.
    VARIABLE_ROWS  = 0
    
    # This variable adjusts the width of the message window shown. If you do
    # not wish to use this feature, set this constant to 0. If the width value
    # is 0 or below, it will automatically default to the screen width.
    VARIABLE_WIDTH = 0
    
    # This is the amount of space that the message window will indent whenever
    # a face is used. Default: 112
    FACE_INDENT_X = 112
    
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # - Name Window Settings -
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # The name window is a window that appears outside of the main message
    # window box to display whatever text is placed inside of it like a name.
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    NAME_WINDOW_X_BUFFER = 0       # Buffer x position of the name window.
    NAME_WINDOW_Y_BUFFER = 0       # Buffer y position of the name window.
    NAME_WINDOW_PADDING  = 20      # Padding added to the horizontal position.
    NAME_WINDOW_OPACITY  = 255     # Opacity of the name window.
    NAME_WINDOW_COLOUR   = 0       # Text colour used by default for names.
    
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # - Message Font Settings -
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # Ace Message System separates the in-game system font form the message
    # font. Adjust the settings here for your fonts.
    #=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # This array constant determines the fonts used. If the first font does not
    # exist on the player's computer, the next font in question will be used
    # in place instead and so on.
    MESSAGE_WINDOW_FONT_NAME = ["VL Gothic", "VL Gothic Regular", "Courier New"]
    
    # These adjust the other settings regarding the way the game font appears
    # including the font size, whether or not the font is bolded by default,
    # italic by default, etc.
    MESSAGE_WINDOW_FONT_SIZE    = 24       # Font size.
    MESSAGE_WINDOW_FONT_BOLD    = false    # Default bold?
    MESSAGE_WINDOW_FONT_ITALIC  = false    # Default italic?
    MESSAGE_WINDOW_FONT_OUTLINE = true     # Default outline?
    MESSAGE_WINDOW_FONT_SHADOW  = false    # Default shadow?
    
  end # MESSAGE
end # YEA

#==============================================================================
# ▼ Editing anything past this point may potentially result in causing
# computer damage, incontinence, explosion of user's head, coma, death, and/or
# halitosis so edit at your own risk.
#==============================================================================

#==============================================================================
# ■ Variable
#==============================================================================

module Variable
  
  #--------------------------------------------------------------------------
  # self.message_rows
  #--------------------------------------------------------------------------
  def self.message_rows
    return 4 if YEA::MESSAGE::VARIABLE_ROWS <= 0
    return 4 if $game_variables[YEA::MESSAGE::VARIABLE_ROWS] <= 0
    return $game_variables[YEA::MESSAGE::VARIABLE_ROWS]
  end
  
  #--------------------------------------------------------------------------
  # self.message_width
  #--------------------------------------------------------------------------
  def self.message_width
    return Graphics.width if YEA::MESSAGE::VARIABLE_WIDTH <= 0
    return Graphics.width if $game_variables[YEA::MESSAGE::VARIABLE_WIDTH] <= 0
    return $game_variables[YEA::MESSAGE::VARIABLE_WIDTH]
  end
  
end # Variable

#==============================================================================
# ■ Game_Interpreter
#==============================================================================

class Game_Interpreter
  
  #--------------------------------------------------------------------------
  # overwrite method: command_101
  # Updated for Paradox
  #--------------------------------------------------------------------------
  def command_101
    wait_for_message
    $game_message.face_name = @params[0]
    $game_message.face_index = @params[1]
    $game_message.background = @params[2]
    $game_message.position = @params[3]
    while continue_message_string?
      @index += 1
      if @list[@index].code == 401
        $game_message.add(@list[@index].parameters[0])
      end
      break if $game_message.texts.size >= Variable.message_rows
    end
    case next_event_code
    when 102
      @index += 1
      setup_choices(@list[@index].parameters)
    when 103
      @index += 1
      setup_num_input(@list[@index].parameters)
    when 104
      @index += 1
      setup_item_choice(@list[@index].parameters)
    when 355
      while next_command_ex_choice?
        @index += 1
        command_355
      end
    end
    wait_for_message
  end
  
  #--------------------------------------------------------------------------
  # new method: continue_message_string?
  #--------------------------------------------------------------------------
  def continue_message_string?
    return true if next_event_code == 101 && Variable.message_rows > 4
    return next_event_code == 401
  end
  
end # Game_Interpreter

#==============================================================================
# ■ Window_Base
#==============================================================================

class Window_Base < Window
  
  #--------------------------------------------------------------------------
  # new method: setup_message_font
  #--------------------------------------------------------------------------
  def setup_message_font
    @message_font = true
    change_color(normal_color)
    contents.font.out_color = Font.default_out_color
    contents.font.name = YEA::MESSAGE::MESSAGE_WINDOW_FONT_NAME
    contents.font.size = YEA::MESSAGE::MESSAGE_WINDOW_FONT_SIZE
    contents.font.bold = YEA::MESSAGE::MESSAGE_WINDOW_FONT_BOLD
    contents.font.italic = YEA::MESSAGE::MESSAGE_WINDOW_FONT_ITALIC
    contents.font.outline = YEA::MESSAGE::MESSAGE_WINDOW_FONT_OUTLINE
    contents.font.shadow = YEA::MESSAGE::MESSAGE_WINDOW_FONT_SHADOW
  end
  
  #--------------------------------------------------------------------------
  # alias method: reset_font_settings
  #--------------------------------------------------------------------------
  alias window_base_reset_font_settings_ams reset_font_settings
  def reset_font_settings
    if @message_font
      setup_message_font
    else
      window_base_reset_font_settings_ams
      contents.font.out_color = Font.default_out_color
      contents.font.outline = Font.default_outline
      contents.font.shadow = Font.default_shadow
    end
  end
  
  #--------------------------------------------------------------------------
  # alias method: convert_escape_characters
  #--------------------------------------------------------------------------
  alias window_base_convert_escape_characters_ams convert_escape_characters
  def convert_escape_characters(text)
    result = text
    result = window_base_convert_escape_characters_ams(result)
    result = convert_ace_message_system_new_escape_characters(result)
    return result
  end
  
  #--------------------------------------------------------------------------
  # new method: convert_ace_message_system_new_escape_characters
  # Updated for Paradox
  #--------------------------------------------------------------------------
  def convert_ace_message_system_new_escape_characters(result)
    #msgbox result
    #---
    #result.gsub!(/\edialogue\[([^\]]+)\]/) { LanguageFileSystem::get_dialogue($1.to_s) }
    #---
    result.gsub!(/\eFR/i) { "\eAMSF[0]" }
    result.gsub!(/\eFB/i) { "\eAMSF[1]" }
    result.gsub!(/\eFI/i) { "\eAMSF[2]" }
    result.gsub!(/\eFO/i) { "\eAMSF[3]" }
    result.gsub!(/\eFS/i) { "\eAMSF[4]" }
    #---
    result.gsub!(/\eAC\[([-+]?\d+)\]/i) { escape_actor_class_name($1.to_i) }
    result.gsub!(/\eAS\[([-+]?\d+)\]/i) { escape_actor_subclass_name($1.to_i) }
    result.gsub!(/\eAN\[([-+]?\d+)\]/i) { escape_actor_nickname($1.to_i) }
    #---
    result.gsub!(/\eNC\[(\d+)\]/i) { $data_classes[$1.to_i].name rescue "" }
    result.gsub!(/\eNI\[(\d+)\]/i) { $data_items[$1.to_i].name rescue "" }
    result.gsub!(/\eNW\[(\d+)\]/i) { $data_weapons[$1.to_i].name rescue "" }
    result.gsub!(/\eNA\[(\d+)\]/i) { $data_armors[$1.to_i].name rescue "" }
    result.gsub!(/\eNS\[(\d+)\]/i) { $data_skills[$1.to_i].name rescue "" }
    result.gsub!(/\eNT\[(\d+)\]/i) { $data_states[$1.to_i].name rescue "" }
    result.gsub!(/\eNE\[(\d+)\]/i)  { $data_system.elements[$1.to_i].to_s rescue ""  }
    #---
    result.gsub!(/\eEN\[(\d+)\]/i)  { $data_enemies[$1.to_i].name rescue "" }
    result.gsub!(/\ePG/i) { $game_party.gold }
    #---
    result.gsub!(/\eIC\[(\d+)\]/i) { escape_icon_item($1.to_i, :class) }
    result.gsub!(/\eII\[(\d+)\]/i) { escape_icon_item($1.to_i, :item) }
    result.gsub!(/\eIW\[(\d+)\]/i) { escape_icon_item($1.to_i, :weapon) }
    result.gsub!(/\eIA\[(\d+)\]/i) { escape_icon_item($1.to_i, :armour) }
    result.gsub!(/\eIS\[(\d+)\]/i) { escape_icon_item($1.to_i, :skill) }
    result.gsub!(/\eIT\[(\d+)\]/i) { escape_icon_item($1.to_i, :state) }
    #---
    result.gsub!(/\eIE\[(\d+)\]/i)  { escape_icon_item($1.to_i, :element) }
    #---
    # From Script: メッセージ制御文字拡張 Ver1.1
    result.gsub!(/\eJ\[(\d+)\]/i) { actor_class_name($1.to_i) }
    result.gsub!(/\eK\[(\d+)\]/i) { actor_nickname($1.to_i) }
    result.gsub!(/\eT\[(\d+)\]/i) { escape_icon_item($1.to_i, :item) }
    result.gsub!(/\eW\[(\d+)\]/i) { escape_icon_item($1.to_i, :weapon) }
    result.gsub!(/\eA\[(\d+)\]/i) { escape_icon_item($1.to_i, :armour) }
    result.gsub!(/\eS\[(\d+)\]/i) { escape_icon_item($1.to_i, :skill) }
    #---
    #result.gsub!(/\eN/i) { "\n" }
    return result
  end
  
  #--------------------------------------------------------------------------
  # new method: escape_actor_class_name
  #--------------------------------------------------------------------------
  def escape_actor_class_name(actor_id)
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    return actor.class.name
  end
  
  #--------------------------------------------------------------------------
  # new method: actor_subclass_name
  #--------------------------------------------------------------------------
  def escape_actor_subclass_name(actor_id)
    return "" unless $imported["YEA-ClassSystem"]
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    return "" if actor.subclass.nil?
    return actor.subclass.name
  end
  
  #--------------------------------------------------------------------------
  # new method: escape_actor_nickname
  #--------------------------------------------------------------------------
  def escape_actor_nickname(actor_id)
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    return actor.nickname
  end
  
  #--------------------------------------------------------------------------
  # new method: escape_icon_item
  #--------------------------------------------------------------------------
  def escape_icon_item(data_id, type)
    case type
    when :class
      return "" unless $imported["YEA-ClassSystem"]
      icon = $data_classes[data_id].icon_index
      name = $data_items[data_id].name
    when :item
      icon = $data_items[data_id].icon_index
      name = $data_items[data_id].name
    when :weapon
      icon = $data_weapons[data_id].icon_index
      name = $data_weapons[data_id].name
    when :armour
      icon = $data_armors[data_id].icon_index
      name = $data_armors[data_id].name
    when :skill
      icon = $data_skills[data_id].icon_index
      name = $data_skills[data_id].name
    when :state
      if LanguageFileSystem::enable_stat_icons
        icon = $data_states[data_id].icon_index
      else
        icon = 0
      end
      name = $data_states[data_id].name
    when :element
      if LanguageFileSystem::enable_elem_icons
        case data_id
        when 2
          icon = 59
        when 3
          icon = 144
        when 4
          icon = 145
        when 5
          icon = 146
        when 6
          icon = 149
        when 7
          icon = 148
        when 8
          icon = 147
        when 9
          icon = 150
        when 10
          icon = 151
        when 35
          icon = 176
        when 36
          icon = 18
        else
          icon = 0
        end
      else
        icon = 0
      end
      name = $data_system.elements[data_id]
    else; return ""
    end
    if icon != nil
      if icon > 0
        case type
        when :state
          if LanguageFileSystem::enable_stat_icons
            text = "\eI[#{icon}]"
          else
            text = name
          end
        when :element
          if LanguageFileSystem::enable_elem_icons
            text = "\eI[#{icon}]"
          else
            text = name
          end
        else
          text = "\eI[#{icon}]" + name
        end
      else
        text = name
      end
    else
      text = name
    end
    return text
  end
  
  #--------------------------------------------------------------------------
  # alias method: process_escape_character
  #--------------------------------------------------------------------------
  alias window_base_process_escape_character_ams process_escape_character
  def process_escape_character(code, text, pos)
    case code.upcase
    #---
    when 'FZ'
      contents.font.size = obtain_escape_param(text)
    when 'FN'
      text.sub!(/\[(.*?)\]/, "")
      font_name = $1.to_s
      font_name = Font.default_name if font_name.nil?
      contents.font.name = font_name.to_s
    #---
    when 'OC'
      colour = text_color(obtain_escape_param(text))
      contents.font.out_color = colour
    when 'OO'
      contents.font.out_color.alpha = obtain_escape_param(text)
    #---
    when 'AMSF'
      case obtain_escape_param(text)
      when 0; reset_font_settings
      when 1; contents.font.bold = !contents.font.bold
      when 2; contents.font.italic = !contents.font.italic
      when 3; contents.font.outline = !contents.font.outline
      when 4; contents.font.shadow = !contents.font.shadow
      end
    #---
    when 'PX'
      pos[:x] = obtain_escape_param(text)
    when 'PY'
      pos[:y] = obtain_escape_param(text)
    #---
    when 'PIC'
      text.sub!(/\[(.*?)\]/, "")
      bmp = Cache.picture($1.to_s)
      rect = Rect.new(0, 0, bmp.width, bmp.height)
      contents.blt(pos[:x], pos[:y], bmp, rect)
    #---
    else
      window_base_process_escape_character_ams(code, text, pos)
    end
  end
  
end # Window_Base

#==============================================================================
# ■ Window_ChoiceList
#==============================================================================

class Window_ChoiceList < Window_Command
  
  #--------------------------------------------------------------------------
  # alias method: initialize
  #--------------------------------------------------------------------------
  alias window_choicelist_initialize_ams initialize
  def initialize(message_window)
    window_choicelist_initialize_ams(message_window)
    setup_message_font
  end
  
end # Window_ChoiceList

#==============================================================================
# ■ Window_ScrollText
#==============================================================================

class Window_ScrollText < Window_Base
  
  #--------------------------------------------------------------------------
  # alias method: initialize
  #--------------------------------------------------------------------------
  alias window_scrolltext_initialize_ams initialize
  def initialize
    window_scrolltext_initialize_ams
    setup_message_font
  end
  
end # Window_ScrollText

#==============================================================================
# ■ Window_NameMessage
#==============================================================================

class Window_NameMessage < Window_Base
  
  #--------------------------------------------------------------------------
  # initialize
  #--------------------------------------------------------------------------
  def initialize(message_window)
    @message_window = message_window
    super(0, 0, Graphics.width, fitting_height(1))
    self.opacity = YEA::MESSAGE::NAME_WINDOW_OPACITY
    self.z = @message_window.z + 1
    self.openness = 0
    setup_message_font
    @close_counter = 0
    deactivate
  end
  
  #--------------------------------------------------------------------------
  # update
  #--------------------------------------------------------------------------
  def update
    super
    return if self.active
    return if self.openness == 0
    return if @closing
    @close_counter -= 1
    return if @close_counter > 0
    close
  end
  
  #--------------------------------------------------------------------------
  # start_close
  #--------------------------------------------------------------------------
  def start_close
    @close_counter = 4
    deactivate
  end
  
  #--------------------------------------------------------------------------
  # force_close
  #--------------------------------------------------------------------------
  def force_close
    @close_counter = 0
    deactivate
    close
  end
  
  #--------------------------------------------------------------------------
  # start
  #--------------------------------------------------------------------------
  def start(text, x_position)
    @text = text.clone
    set_width
    create_contents
    set_x_position(x_position)
    set_y_position
    refresh
    activate
    open
  end
  
  #--------------------------------------------------------------------------
  # set_width
  #--------------------------------------------------------------------------
  def set_width
    text = @text.clone
    dw = standard_padding * 2 + text_size(text).width
    dw += YEA::MESSAGE::NAME_WINDOW_PADDING * 2
    dw += calculate_size(text.slice!(0, 1), text) until text.empty?
    self.width = dw
  end
  
  #--------------------------------------------------------------------------
  # calculate_size
  #--------------------------------------------------------------------------
  def calculate_size(code, text)
    case code
    when "\e"
      return calculate_escape_code_width(obtain_escape_code(text), text)
    else
      return 0
    end
  end
  
  #--------------------------------------------------------------------------
  # calculate_escape_code_width
  #--------------------------------------------------------------------------
  def calculate_escape_code_width(code, text)
    dw = -text_size("\e").width - text_size(code).width
    case code.upcase
    when 'C', 'OC', 'OO'
      dw += -text_size("[" + obtain_escape_param(text).to_s + "]").width
      return dw
    when 'I'
      dw += -text_size("[" + obtain_escape_param(text).to_s + "]").width
      dw += 24
      return dw
    when '{'
      make_font_bigger
    when '}'
      make_font_smaller
    when 'FZ'
      contents.font.size = obtain_escape_param(text)
    when 'FN'
      text.sub!(/\[(.*?)\]/, "")
      font_name = $1.to_s
      font_name = Font.default_name if font_name.nil?
      contents.font.name = font_name.to_s
    when 'AMSF'
      case obtain_escape_param(text)
      when 0; reset_font_settings
      when 1; contents.font.bold = !contents.font.bold
      when 2; contents.font.italic = !contents.font.italic
      when 3; contents.font.outline = !contents.font.outline
      when 4; contents.font.shadow = !contents.font.shadow
      end
    else
      return dw
    end
  end
  
  #--------------------------------------------------------------------------
  # set_y_position
  #--------------------------------------------------------------------------
  def set_x_position(x_position)
    case x_position
    when 1 # Left
      self.x = @message_window.x
      self.x += YEA::MESSAGE::NAME_WINDOW_X_BUFFER
    when 2 # 3/10
      self.x = @message_window.x
      self.x += @message_window.width * 3 / 10
      self.x -= self.width / 2
    when 3 # Center
      self.x = @message_window.x
      self.x += @message_window.width / 2
      self.x -= self.width / 2
    when 4 # 7/10
      self.x = @message_window.x
      self.x += @message_window.width * 7 / 10
      self.x -= self.width / 2
    when 5 # Right
      self.x = @message_window.x + @message_window.width
      self.x -= self.width
      self.x -= YEA::MESSAGE::NAME_WINDOW_X_BUFFER
    end
    self.x = [[self.x, Graphics.width - self.width].min, 0].max
  end
  
  #--------------------------------------------------------------------------
  # set_y_position
  #--------------------------------------------------------------------------
  def set_y_position
    case $game_message.position
    when 0
      self.y = @message_window.height
      self.y -= YEA::MESSAGE::NAME_WINDOW_Y_BUFFER
    else
      self.y = @message_window.y - self.height
      self.y += YEA::MESSAGE::NAME_WINDOW_Y_BUFFER
    end
  end
  
  #--------------------------------------------------------------------------
  # refresh
  #--------------------------------------------------------------------------
  def refresh
    contents.clear
    reset_font_settings
    @text = sprintf("\eC[%d]%s", YEA::MESSAGE::NAME_WINDOW_COLOUR, @text)
    draw_text_ex(YEA::MESSAGE::NAME_WINDOW_PADDING, 0, @text)
  end
  
end # Window_NameMessage

#==============================================================================
# ■ Window_Message
#==============================================================================

class Window_Message < Window_Base
  #--------------------------------------------------------------------------
  # override method: setup_reduce_over_line
  #--------------------------------------------------------------------------
  def setup_reduce_over_line(text, pos)
  end
  #--------------------------------------------------------------------------
  # alias method: initialize
  #--------------------------------------------------------------------------
  alias window_message_initialize_ams initialize
  def initialize
    window_message_initialize_ams
    setup_message_font
  end
  
  #--------------------------------------------------------------------------
  # overwrite method: window_width
  #--------------------------------------------------------------------------
  def window_width
    return Variable.message_width
  end
  
  #--------------------------------------------------------------------------
  # overwrite method: window_height
  #--------------------------------------------------------------------------
  def window_height
    return fitting_height(Variable.message_rows)
  end
  
  #--------------------------------------------------------------------------
  # alias method: create_all_windows
  #--------------------------------------------------------------------------
  alias window_message_create_all_windows_ams create_all_windows
  def create_all_windows
    window_message_create_all_windows_ams
    @name_window = Window_NameMessage.new(self)
  end
  
  #--------------------------------------------------------------------------
  # overwrite method: create_back_bitmap
  #--------------------------------------------------------------------------
  def create_back_bitmap
    @back_bitmap = Bitmap.new(width, height)
    rect1 = Rect.new(0, 0, Graphics.width, 12)
    rect2 = Rect.new(0, 12, Graphics.width, fitting_height(4) - 24)
    rect3 = Rect.new(0, fitting_height(4) - 12, Graphics.width, 12)
    @back_bitmap.gradient_fill_rect(rect1, back_color2, back_color1, true)
    @back_bitmap.fill_rect(rect2, back_color1)
    @back_bitmap.gradient_fill_rect(rect3, back_color1, back_color2, true)
  end
  
  #--------------------------------------------------------------------------
  # alias method: dispose_all_windows
  #--------------------------------------------------------------------------
  alias window_message_dispose_all_windows_ams dispose_all_windows
  def dispose_all_windows
    window_message_dispose_all_windows_ams
    @name_window.dispose
  end
  
  #--------------------------------------------------------------------------
  # alias method: update_all_windows
  #--------------------------------------------------------------------------
  alias window_message_update_all_windows_ams update_all_windows
  def update_all_windows
    window_message_update_all_windows_ams
    @name_window.update
    @name_window.back_opacity = self.back_opacity
    @name_window.opacity = self.opacity
  end
  
  #--------------------------------------------------------------------------
  # alias method: update_show_fast
  # Disabled for Paradox
  #--------------------------------------------------------------------------
  #alias window_message_update_show_fast_ams update_show_fast
  #def update_show_fast
  #  @show_fast = true if Input.press?(YEA::MESSAGE::TEXT_SKIP)
  #  window_message_update_show_fast_ams
  #end
  
  #--------------------------------------------------------------------------
  # overwrite method: input_pause
  # Disabled for Paradox
  #--------------------------------------------------------------------------
  #def input_pause
  #  self.pause = true
  #  wait(10)
  #  Fiber.yield until Input.trigger?(:B) || Input.trigger?(:C) ||
  #    Input.press?(YEA::MESSAGE::TEXT_SKIP)
  #  Input.update
  #  self.pause = false
  #end
  
  #--------------------------------------------------------------------------
  # overwrite method: convert_escape_characters
  #--------------------------------------------------------------------------
  def convert_escape_characters(text)
    result = super(text.to_s.clone)
    result = namebox_escape_characters(result)
    result = message_escape_characters(result)
    #result.gsub!(/\eN/i) { "\n" }
    return result
  end
  
  #--------------------------------------------------------------------------
  # new method: namebox_escape_characters
  #--------------------------------------------------------------------------
  def namebox_escape_characters(result)
    # Updated 08-10-2015 To Also Remove New Line
    result.gsub!(/\eN\<(.+?)\>\n/i)  { namewindow($1, 1) }
    result.gsub!(/\eN1\<(.+?)\>\n/i) { namewindow($1, 1) }
    result.gsub!(/\eN2\<(.+?)\>\n/i) { namewindow($1, 2) }
    result.gsub!(/\eNC\<(.+?)\>\n/i) { namewindow($1, 3) }
    result.gsub!(/\eN3\<(.+?)\>\n/i) { namewindow($1, 3) }
    result.gsub!(/\eN4\<(.+?)\>\n/i) { namewindow($1, 4) }
    result.gsub!(/\eN5\<(.+?)\>\n/i) { namewindow($1, 5) }
    result.gsub!(/\eNR\<(.+?)\>\n/i) { namewindow($1, 5) }
    
    result.gsub!(/\eN\<(.+?)\>\eN/i)  { namewindow($1, 1) }
    result.gsub!(/\eN1\<(.+?)\>\eN/i) { namewindow($1, 1) }
    result.gsub!(/\eN2\<(.+?)\>\eN/i) { namewindow($1, 2) }
    result.gsub!(/\eNC\<(.+?)\>\eN/i) { namewindow($1, 3) }
    result.gsub!(/\eN3\<(.+?)\>\eN/i) { namewindow($1, 3) }
    result.gsub!(/\eN4\<(.+?)\>\eN/i) { namewindow($1, 4) }
    result.gsub!(/\eN5\<(.+?)\>\eN/i) { namewindow($1, 5) }
    result.gsub!(/\eNR\<(.+?)\>\eN/i) { namewindow($1, 5) }
    
    result.gsub!(/\eN\<(.+?)\>/i)  { namewindow($1, 1) }
    result.gsub!(/\eN1\<(.+?)\>/i) { namewindow($1, 1) }
    result.gsub!(/\eN2\<(.+?)\>/i) { namewindow($1, 2) }
    result.gsub!(/\eNC\<(.+?)\>/i) { namewindow($1, 3) }
    result.gsub!(/\eN3\<(.+?)\>/i) { namewindow($1, 3) }
    result.gsub!(/\eN4\<(.+?)\>/i) { namewindow($1, 4) }
    result.gsub!(/\eN5\<(.+?)\>/i) { namewindow($1, 5) }
    result.gsub!(/\eNR\<(.+?)\>/i) { namewindow($1, 5) }
    return result
  end
  
  #--------------------------------------------------------------------------
  # new method: namebox
  #--------------------------------------------------------------------------
  def namewindow(text, position)
    @name_text = text
    @name_position = position
    return ""
  end
  
  #--------------------------------------------------------------------------
  # new method: message_escape_characters
  #--------------------------------------------------------------------------
  def message_escape_characters(result)
    result.gsub!(/\eAF\[(-?\d+)]/i) { change_face($1.to_i) }
    return result
  end
  
  #--------------------------------------------------------------------------
  # new method: change_face
  #--------------------------------------------------------------------------
  def change_face(actor_id)
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    $game_message.face_name = actor.face_name
    $game_message.face_index = actor.face_index
    return ""
  end
  
  #--------------------------------------------------------------------------
  # alias method: new_page
  #--------------------------------------------------------------------------
  alias window_message_new_page_ams new_page
  def new_page(text, pos)
    adjust_message_window_size
    window_message_new_page_ams(text, pos)
  end
  
  #--------------------------------------------------------------------------
  # overwrite method: new_line_x
  #--------------------------------------------------------------------------
  def new_line_x
    return $game_message.face_name.empty? ? 0 : YEA::MESSAGE::FACE_INDENT_X
  end
  
  #--------------------------------------------------------------------------
  # new method: adjust_message_window_size
  #--------------------------------------------------------------------------
  def adjust_message_window_size
    self.height = window_height
    self.width = window_width
    create_contents
    update_placement
    self.x = (Graphics.width - self.width) / 2
    start_name_window
  end
  
  #--------------------------------------------------------------------------
  # new method: clear_name_window
  #--------------------------------------------------------------------------
  def clear_name_window
    @name_text = ""
    @name_position = 0
  end
  
  #--------------------------------------------------------------------------
  # new method: start_name_window
  #--------------------------------------------------------------------------
  def start_name_window
    return if @name_text == ""
    @name_window.start(@name_text, @name_position)
  end
  
  #--------------------------------------------------------------------------
  # overwrite method: fiber_main
  #--------------------------------------------------------------------------
  def fiber_main
    $game_message.visible = true
    update_background
    update_placement
    loop do
      process_all_text if $game_message.has_text?
      process_input
      $game_message.clear
      @gold_window.close
      @name_window.start_close
      Fiber.yield
      break unless text_continue?
    end
    close_and_wait
    $game_message.visible = false
    @fiber = nil
  end
  
  #--------------------------------------------------------------------------
  # alias method: open_and_wait
  #--------------------------------------------------------------------------
  alias window_message_open_and_wait_ams open_and_wait
  def open_and_wait
    clear_name_window
    adjust_message_window_size
    window_message_open_and_wait_ams
  end
  
  #--------------------------------------------------------------------------
  # alias method: close_and_wait
  #--------------------------------------------------------------------------
  alias window_message_close_and_wait_ams close_and_wait
  def close_and_wait
    @name_window.force_close
    window_message_close_and_wait_ams
  end
  
  #--------------------------------------------------------------------------
  # alias method: all_close?
  #--------------------------------------------------------------------------
  alias window_message_all_close_ams all_close?
  def all_close?
    return window_message_all_close_ams && @name_window.close?
  end
  
  #--------------------------------------------------------------------------
  # alias method: process_escape_character
  #--------------------------------------------------------------------------
  alias window_message_process_escape_character_ams process_escape_character
  def process_escape_character(code, text, pos)
    case code.upcase
    when 'W' # Wait
      wait(obtain_escape_param(text))
    else
      window_message_process_escape_character_ams(code, text, pos)
    end
  end
  
end # Window_Message

#==============================================================================
# 
# ■ Word
#==============================================================================
class Word
  #--------------------------------------------------------------------------
  # ● Object Initializer
  #--------------------------------------------------------------------------
  def initialize(word_data, face_name, face_index)
    # Words Will Still Be Filtered Later
    @words = word_data     #@words = Word.convert_new_escape_characters(word_data).split("\\n")
    @face_name = face_name
    @face_index = face_index
  end
  #--------------------------------------------------------------------------
  # ● Run
  #--------------------------------------------------------------------------
  def execute
    $game_message.face_name = @face_name
    $game_message.face_index = @face_index
    $game_message.background = 0
    $game_message.position = 2
    # YEA Script Will Still Filter This
    $game_message.add(@words)     #@words.each{|line| $game_message.add(line)}
  end
  
  #--------------------------------------------------------------------------
  # new method: convert_ace_message_system_new_escape_characters
  # Copy Of YEA - Ace Message System Function For Combat Dialoge
  # Updated For Paradox 08-10-2015
  #--------------------------------------------------------------------------
  def self.convert_new_escape_characters(result)
    #---
    result.gsub!(/\eFR/i) { "\eAMSF[0]" }
    result.gsub!(/\eFB/i) { "\eAMSF[1]" }
    result.gsub!(/\eFI/i) { "\eAMSF[2]" }
    result.gsub!(/\eFO/i) { "\eAMSF[3]" }
    result.gsub!(/\eFS/i) { "\eAMSF[4]" }
    #---
    result.gsub!(/\eAC\[([-+]?\d+)\]/i) { Word.escape_actor_class_name($1.to_i) }
    result.gsub!(/\eAS\[([-+]?\d+)\]/i) { Word.escape_actor_subclass_name($1.to_i) }
    result.gsub!(/\eAN\[([-+]?\d+)\]/i) { Word.escape_actor_nickname($1.to_i) }
    #---
    #---
    result.gsub!(/\\AC\[([-+]?\d+)\]/i) { Word.escape_actor_class_name($1.to_i) }
    result.gsub!(/\\AS\[([-+]?\d+)\]/i) { Word.escape_actor_subclass_name($1.to_i) }
    result.gsub!(/\\AN\[([-+]?\d+)\]/i) { Word.escape_actor_nickname($1.to_i) }
    #---
    result.gsub!(/\eN\[(\d+)\]/i) { $game_actors[$1.to_i].name rescue "" }
    #---
    #---
    result.gsub!(/\\n\[(\d+)\]/i) { $game_actors[$1.to_i].name rescue "" }
    #---
    result.gsub!(/\eNC\[(\d+)\]/i) { $data_classes[$1.to_i].name rescue "" }
    result.gsub!(/\eNI\[(\d+)\]/i) { $data_items[$1.to_i].name rescue "" }
    result.gsub!(/\eNW\[(\d+)\]/i) { $data_weapons[$1.to_i].name rescue "" }
    result.gsub!(/\eNA\[(\d+)\]/i) { $data_armors[$1.to_i].name rescue "" }
    result.gsub!(/\eNS\[(\d+)\]/i) { $data_skills[$1.to_i].name rescue "" }
    result.gsub!(/\eNT\[(\d+)\]/i) { $data_states[$1.to_i].name rescue "" }
    result.gsub!(/\eNE\[(\d+)\]/i)  { $data_system.elements[$1.to_i].to_s rescue ""  }
    #---
    #---
    result.gsub!(/\\NC\[(\d+)\]/i) { $data_classes[$1.to_i].name rescue "" }
    result.gsub!(/\\NI\[(\d+)\]/i) { $data_items[$1.to_i].name rescue "" }
    result.gsub!(/\\NW\[(\d+)\]/i) { $data_weapons[$1.to_i].name rescue "" }
    result.gsub!(/\\NA\[(\d+)\]/i) { $data_armors[$1.to_i].name rescue "" }
    result.gsub!(/\\NS\[(\d+)\]/i) { $data_skills[$1.to_i].name rescue "" }
    result.gsub!(/\\NT\[(\d+)\]/i) { $data_states[$1.to_i].name rescue "" }
    result.gsub!(/\\NE\[(\d+)\]/i)  { $data_system.elements[$1.to_i].to_s rescue ""  }
    #---
    result.gsub!(/\eEN\[(\d+)\]/i)  { $data_enemies[$1.to_i].name rescue "" }
    result.gsub!(/\ePG/i) { $game_party.gold }
    #---
    #---
    result.gsub!(/\\EN\[(\d+)\]/i)  { $data_enemies[$1.to_i].name rescue "" }
    result.gsub!(/\\PG/i) { $game_party.gold }
    #---
    result.gsub!(/\eIC\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :class) }
    result.gsub!(/\eII\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :item) }
    result.gsub!(/\eIW\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :weapon) }
    result.gsub!(/\eIA\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :armour) }
    result.gsub!(/\eIS\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :skill) }
    result.gsub!(/\eIT\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :state) }
    result.gsub!(/\eIE\[(\d+)\]/i)  { Word.escape_icon_item($1.to_i, :element) }
    #---
    #---
    result.gsub!(/\\IC\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :class) }
    result.gsub!(/\\II\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :item) }
    result.gsub!(/\\IW\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :weapon) }
    result.gsub!(/\\IA\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :armour) }
    result.gsub!(/\\IS\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :skill) }
    result.gsub!(/\\IT\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :state) }
    result.gsub!(/\\IE\[(\d+)\]/i)  { Word.escape_icon_item($1.to_i, :element) }
    #---
    #---
    # From Script: メッセージ制御文字拡張 Ver1.1
    result.gsub!(/\eJ\[(\d+)\]/i) { actor_class_name($1.to_i) }
    result.gsub!(/\eK\[(\d+)\]/i) { actor_nickname($1.to_i) }
    result.gsub!(/\eT\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :item) }
    result.gsub!(/\eW\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :weapon) }
    result.gsub!(/\eA\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :armour) }
    result.gsub!(/\eS\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :skill) }
    #---
    #---
    # From Script: メッセージ制御文字拡張 Ver1.1
    result.gsub!(/\\J\[(\d+)\]/i) { actor_class_name($1.to_i) }
    result.gsub!(/\\K\[(\d+)\]/i) { actor_nickname($1.to_i) }
    result.gsub!(/\\T\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :item) }
    result.gsub!(/\\W\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :weapon) }
    result.gsub!(/\\A\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :armour) }
    result.gsub!(/\\S\[(\d+)\]/i) { Word.escape_icon_item($1.to_i, :skill) }
    #---
    return result
  end
  
  #--------------------------------------------------------------------------
  # new method: escape_actor_class_name
  #--------------------------------------------------------------------------
  def self.escape_actor_class_name(actor_id)
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    return actor.class.name
  end
  
  #--------------------------------------------------------------------------
  # new method: actor_subclass_name
  #--------------------------------------------------------------------------
  def self.escape_actor_subclass_name(actor_id)
    return "" unless $imported["YEA-ClassSystem"]
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    return "" if actor.subclass.nil?
    return actor.subclass.name
  end
  
  #--------------------------------------------------------------------------
  # new method: escape_actor_nickname
  #--------------------------------------------------------------------------
  def self.escape_actor_nickname(actor_id)
    actor_id = $game_party.members[actor_id.abs].id if actor_id <= 0
    actor = $game_actors[actor_id]
    return "" if actor.nil?
    return actor.nickname
  end
  
  #--------------------------------------------------------------------------
  # new method: escape_icon_item
  #--------------------------------------------------------------------------
  def self.escape_icon_item(data_id, type)
    case type
    when :class
      return "" unless $imported["YEA-ClassSystem"]
      icon = $data_classes[data_id].icon_index
      name = $data_items[data_id].name
    when :item
      icon = $data_items[data_id].icon_index
      name = $data_items[data_id].name
    when :weapon
      icon = $data_weapons[data_id].icon_index
      name = $data_weapons[data_id].name
    when :armour
      icon = $data_armors[data_id].icon_index
      name = $data_armors[data_id].name
    when :skill
      icon = $data_skills[data_id].icon_index
      name = $data_skills[data_id].name
    when :state
      if LanguageFileSystem::enable_stat_icons
        icon = $data_states[data_id].icon_index
      else
        icon = 0
      end
      name = $data_states[data_id].name
    when :element
      if LanguageFileSystem::enable_elem_icons
        case data_id
        when 2
          icon = 59
        when 3
          icon = 144
        when 4
          icon = 145
        when 5
          icon = 146
        when 6
          icon = 149
        when 7
          icon = 148
        when 8
          icon = 147
        when 9
          icon = 150
        when 10
          icon = 151
        when 35
          icon = 176
        when 36
          icon = 18
        else
          icon = 0
        end
      else
        icon = 0
      end
      name = $data_system.elements[data_id]
    else; return ""
    end
    if icon != nil
      if icon > 0
        case type
        when :state
          if LanguageFileSystem::enable_stat_icons
            text = "\eI[#{icon}]"
          else
            text = name
          end
        when :element
          if LanguageFileSystem::enable_elem_icons
            text = "\eI[#{icon}]"
          else
            text = name
          end
        else
          text = "\eI[#{icon}]" + name
        end
      else
        text = name
      end
    else
      text = name
    end
    return text
  end
end

#========================================================================
# ** Word Wrapping Message Boxes, by: KilloZapit
#------------------------------------------------------------------------
# Changes message boxes so it will automatically wrap long lines.
#
# Note: I consider this script to be public domain, and put no
# restrictions on it's use whatsoever. My only request is that
# a link back to the script is provided so more people can
# access it if they want to.
#
# Version the Second:
#   Now strips color codes and icon codes so they don't break words.
#   Also calculates icon width along with text width for words.
# Version the Third:
#   Now also strips delays and other timing related codes.
#   Splits for non-icon control codes before counting icons.
#   Control codes can now break lines in case of font changes.
#   Added some comments to clarify some code.
# Version the Forth:
#   Fixed a small bug that might cause a error when counting icons.
#   Added a small notice for copyright questions.
# Version the Fifth:
#   Added "collapse" mode, which elimanates extra spaces.
#   Can now use "whitespace" mode outside of wordwrap mode if needed.
# Version the Sixth:
#   Fixed problems with collapsed whitespace not wraping words right.
# Version the Seventh:
#   Added option to add a margin to the right hand side of the window.
#------------------------------------------------------------------------
# Also adds the following new escape sequences:
#
# \ww  - Word Wrap: turns word wrap on if it's off
# \nw  - No Wrap: Turns word wrap off
# \ws  - WhiteSpace mode: Converts newlines to spaces (like HTML)
# \nl  - New Line: Preserves hard returns
# \cs  - Collapse whiteSpace: Eliminates extra spaces (also like HTML)
# \pre - PRE-formatted: Preserves spaces
# \br  - line BRake: manual newline for whitespace mode
# \rm  - Right Margin: extra space on the right side of the window
#========================================================================

# Standard config module.
module KZIsAwesome
  module WordWrap

    # change this if you don't want wordwrap on by default.
    DEFAULT_WORDWRAP = true

    # change this if you want white space mode on by default.
    DEFAULT_WHITESPACE = false
   
    # change this if you want white space mode on by default.
    DEFAULT_COLLAPSE = true
    
    # change this to add a right margin to the window.
    DEFAULT_RIGHT_MARGIN = 0

  end
end

class Window_Base < Window
  include KZIsAwesome::WordWrap

  alias_method :initialize_kz_window_base, :initialize
  def initialize(x, y, width, height)
    initialize_kz_window_base(x, y, width, height)
    @wordwrap = DEFAULT_WORDWRAP
    @convert_newlines = DEFAULT_WHITESPACE
    @collapse_whitespace = DEFAULT_COLLAPSE
    @right_margin = DEFAULT_RIGHT_MARGIN
    @lastc = "\n"
  end

  alias_method :process_character_kz_window_base, :process_character
  def process_character(c, text, pos)
    c = ' ' if @convert_newlines && c == "\n"
    if @wordwrap && c =~ /[ \t]/
      c = '' if @collapse_whitespace && @lastc =~ /[\s\n\f]/
      if pos[:x] + get_next_word_size(c, text) > contents.width - @right_margin
        process_new_line(text, pos)
      else
        process_normal_character(c, pos)
      end
      @lastc = c
    else
      @lastc = c
      process_character_kz_window_base(c, text, pos)
    end
  end

  def get_next_word_size(c, text)
    # Split text by the next space/line/page and grab the first split
    nextword = text.split(/[\s\n\f]/, 2)[0]
    if nextword
      icons = 0
      if nextword =~ /\e/i
        # Get rid of color codes and YEA Message system outline colors
        nextword = nextword.split(/\e[oOcC]+\[\d*\]/).join
        # Get rid of message timing control codes
        nextword = nextword.split(/\e[\.\|\^<>!]/).join
        # Split text by the first non-icon escape code
        # (the hH is for compatibility with the Icon Hues script)
        nextword = nextword.split(/\e[^iIhH]+/, 2)[0]
        # Erase and count icons in remaining text
        nextword.gsub!(/\e[iIhH]+\[[\d,]*\]/) do
          icons += 1
          ''
        end if nextword
      end
      wordsize = (nextword ? text_size(c + nextword).width : text_size( c ).width)
      wordsize += icons * 24
    else
      wordsize = text_size( c ).width
    end
    return wordsize
  end

  alias_method :process_escape_character_kz_window_base, :process_escape_character
  def process_escape_character(code, text, pos)
    #temp fix for Nil Class
    code = 'WW' if code == nil
    
    case code.upcase
    when 'WW'
      @wordwrap = true
    when 'NW'
      @wordwrap = false
    when 'WS'
      @convert_newlines = true
    when 'NL'
      @convert_newlines = false
    when 'CS'
      @collapse_whitespace = true
    when 'PRE'
      @collapse_whitespace = false
    when 'BR'
      process_new_line(text, pos)
      @lastc = "\n"
    when 'RM'
      @right_margin = obtain_escape_param(text)
    else
      process_escape_character_kz_window_base(code, text, pos)
    end

    # Recalculate the next word size and insert line breaks
    # (Needed primarily for font changes)
    if pos[:x] + get_next_word_size('', text) > contents.width
      process_new_line(text, pos)
    end
  end

end

#==============================================================================
#
# Fix in-battle word wrap & squishing.
#
#==============================================================================
class Window_BattleLog < Window_Selectable
  #--------------------------------------------------------------------------
  # ● Refresh
  #--------------------------------------------------------------------------
  def refresh
    contents.clear
    counter = -1
    #@lines.size.times {|i|
    @lines.each_with_index {|line, index|
      text = reformat_wrapped(line)
      txt_array = text.split("\n")
      if txt_array.size <= 1
        counter += 1
        #draw_line(index)
        draw_line_ex(counter, text)
      else
        txt_array.each{|new_line|
          counter += 1
          draw_line_ex(counter, new_line)
        }
      end
    }
  end
  
  #--------------------------------------------------------------------------
  # ● Draw Line
  #--------------------------------------------------------------------------
  def draw_line_ex(line_number, text)
    rect = item_rect_for_text(line_number)
    contents.clear_rect(rect)
    draw_text_ex(rect.x, rect.y, text)
  end
  
  #--------------------------------------------------------------------------
  # ● Reformat Wrapped
  #--------------------------------------------------------------------------
  def reformat_wrapped(s, width=60)
      lines = []
      line = ""
      s.split(/\s+/).each do |word|
        if line.size + word.size >= width
          lines << line
          line = word
        elsif line.empty?
         line = word
        else
         line << " " << word
       end
       end
       lines << line if line
      return lines.join "\n"
    end
end

#==============================================================================
#
# Enable Message Window Text Hiding.
#
#==============================================================================

=begin
===============================================================================
 Message Visibility by efeberk
 Version: RGSS3
===============================================================================
 This script will allow to player sets message window visible or unvisible with 
 a key.
 
 Example : Press F8 to hide message window and repress F8 to show message 
 window.
--------------------------------------------------------------------------------
=end

module EFE
  
  KEY = :F8

end

class Window_Message < Window_Base
  
  alias efeberk_window_message_update update
  def update
    efeberk_window_message_update
    if Input.trigger?(EFE::KEY)
      self.visible = !self.visible
      
      # Update For YEA - Ace Message System
      # Name Box Window
      @name_window.visible = self.visible if $imported["YEA-MessageSystem"]
      # Other Message Windows That Appear On Screen
      @gold_window.visible = self.visible
      @choice_window.visible = self.visible
      @number_window.visible = self.visible
      @item_window.visible = self.visible
    end
  end  
end
class Foo::JobChange::Window_ClassStatus < Window_Base
  def set_auto_font_size(text, width)
  end
  def draw_text_job_desc(rect, text)
    reset_font_settings
    contents.font.size -= 6
    text = convert_escape_characters(text)
    pos = {:x => rect.x, :y => rect.y, :new_x => rect.x, :height => line_height}
    set_auto_font_size(text, rect.width)
    process_character(text.slice!(0, 1), text, pos) until text.empty?
  end
end

class Window_BattleLog < Window_Selectable
  def display_miss(target, item)
    return unless item
    fmt = target.actor? ? Vocab::ActorNoHit : Vocab::EnemyNoHit
    Sound.play_miss
    add_text(format(fmt.force_encoding('UTF-8').encode, target.name))
    wait
  end
end