class << DataManager
  def read_event_data_from_file(map_file_location)
    templine = ""
    # reads the entire map into memory
    if File.exists?(map_file_location)
      map_file_data = File.readlines(map_file_location).reject { |x| x.empty? }

      # Array for storing the event pages
      # Order: EventID, PageID, EventCommands
      temp_events = Array.new(9999) { Array.new(100) { Array.new() } }
      loop do
        if templine.start_with?("CommonEvent") or templine.start_with?("Troop ")
          # get the event number
          if templine.start_with?("CommonEvent")
            temp_event_id = templine[11..-1].to_i
          else
            temp_event_id = templine[6..-1].to_i
          end
          templine_indent = 0
          temp_page_id = 1
          loop do
            temp_command_code = 0
            temp_parameters = []
            break unless map_file_data.length > 0
            # get and remove the first string from the array
            templine = map_file_data.shift.strip
            # skip those empty lines to stop creating empty eventcommands
            next if (templine == "" or templine.start_with?("Name "))
            if templine.start_with?("Page ")
              temp_page_id = templine[4..-1].to_i
              templine_indent = 0
              temp_add_param = []
              next
            end
            # the next event was reached
            break if (templine.start_with?("CommonEvent ") or templine.start_with?("Troop "))
            temp_parameters = templine.split(/[()]/)

            temp_add_param = Array.new()

            # this turns the gameselector strings back to codes
            # also adds special handling for codes that need it
            case temp_parameters[0]
              when "Empty"
                temp_command_code = 0
              when "ShowTextAttributes"
                temp_command_code = 101
                # get face graphics string
                temp_array = temp_parameters[1][2..-2].split(",")
                temp_add_param = [temp_array.shift[0..-2]]
                # rest are int
                temp_add_param.push(temp_array[0].strip.to_i)
                temp_add_param.push(temp_array[1].strip.to_i)
                temp_add_param.push(temp_array[2].strip.to_i)
              when "ShowChoices"
                temp_command_code = 102
                templine_indent += 1
                temp_array = templine[15..-3].split("], ")
                default_choice = temp_array.pop.to_i
                temp_add_param = []
                temp_array[0][0..-2].split("\", \"").each do |choice|
                  temp_add_param.push(choice.gsub("\\\\C","\\C"))
                end
                temp_add_param = [temp_add_param]
                temp_add_param.push(default_choice)
              when "InputNumber"
                temp_command_code = 103
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ShowScrollingTextAttributes"
                temp_command_code = 105
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0].to_i]
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[1].strip == "true")
              when "Comment"
                temp_command_code = 108
                temp_add_param = [templine[10..-4]]
              when "ConditionalBranch"
                temp_command_code = 111
                templine_indent += 1
                # whatever the conditions are, they're stored as int
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
                # ...except for scripts, self switches, armors
                if temp_add_param[0] == 2
                  last_value = temp_add_param.pop
                  temp_add_param.pop
                  temp_add_param.push(temp_parameters[1].split(",")[1].strip.gsub("\"",""))
                  temp_add_param.push(last_value)
                elsif temp_add_param[0] == 9 or temp_add_param[0] == 10
                  temp_add_param.pop
                  temp_add_param.push(temp_parameters[1].split(",")[2][1..-2] == "true")
                elsif temp_add_param[0] == 12
                  temp_add_param.pop
                  temp_add_param.push(templine[24...-3].gsub("\\\"","\""))
                end
              when "Loop"
                temp_command_code = 112
                templine_indent += 1
              when "BreakLoop"
                temp_command_code = 113
              when "ExitEventProcessing"
                temp_command_code = 115
              when "CallCommonEvent"
                temp_command_code = 117
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "Label"
                temp_command_code = 118
                temp_add_param = [temp_parameters[1].gsub(/"/, '')[1..-2]]
              when "JumpToLabel"
                temp_command_code = 119
                temp_add_param = [temp_parameters[1].gsub(/"/, '')[1..-2]]
              when "ControlSwitches"
                temp_command_code = 121
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ControlVariables"
                temp_command_code = 122
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
                if temp_add_param[3] == 4
                  temp_add_param.pop
                  temp_add_param.push(temp_parameters[1].split(",")[-1][2..-3])
                end
              when "ControlSelfSwitch"
                temp_command_code = 123
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0][1..-2]]
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[1].to_i)
              when "ChangeGold"
                temp_command_code = 125
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeItems"
                temp_command_code = 126
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeWeapons"
                temp_command_code = 127
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_array_count = temp_array.count
                temp_boolean = (temp_array.pop.strip == "true") if temp_array_count > 4
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean) if temp_array_count > 4
              when "ChangeArmor"
                temp_command_code = 128
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_array_count = temp_array.count
                temp_boolean = (temp_array.pop.strip == "true") if temp_array_count > 4
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean) if temp_array_count > 4
              when "ChangePartyMember"
                temp_command_code = 129
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeBattleBGM"
                temp_command_code = 132
                temp_add_param = [RPG::BGM.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "ChangeSaveAccess"
                temp_command_code = 134
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ChangeEncounter"
                temp_command_code = 136
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "TransferPlayer"
                temp_command_code = 201
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "SetVehicleLocation"
                temp_command_code = 202
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "SetEventLocation"
                temp_command_code = 203
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ScrollMap"
                temp_command_code = 204
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "SetMoveRoute"
                temp_command_code = 205
                temp_init_number = temp_parameters[1].split(",")[0][1..-1].to_i
                temp_bool1 = temp_parameters[2].split(",")[0].split("=")[1].strip == "true"
                temp_bool2 = temp_parameters[2].split(",")[1].split("=")[1].strip == "true"
                temp_bool3 = temp_parameters[2].split(",")[2].split("=")[1].strip == "true"
                temp_move_command_list = []
                temp_parameters[2..-1].join().split("RPG::MoveCommand")[1..-1].each do |temp_move_command_params|
                  temp_move_code = temp_move_command_params.split("=")[1].split(",")[0].to_i
                  if temp_move_code == 44
                    temp_move_params = [RPG::SE.new(temp_move_command_params.split("=")[3].split("\"")[1].split("\\")[0], temp_move_command_params.split("=")[5].split(",")[0].to_i, temp_move_command_params.split("=")[4].split(",")[0].split("\\")[0].to_i)]
                  elsif temp_move_code == 41
                    temp_move_params = [temp_move_command_params.split("=")[2][1..-1].split("]")[0].split(",")[0].strip[4..-5], temp_move_command_params.split("=")[2][1..-1].split("]")[0].split(",")[1].to_i]
                  else
                    temp_move_params = temp_move_command_params.split("=")[2][1..-1].split("]")[0].split(",").map(&:to_i)
                  end
                  temp_move_command_list.push(RPG::MoveCommand.new(temp_move_code, temp_move_params))
                end
                temp_add_param = [temp_init_number, RPG::MoveRoute.new(temp_bool1,temp_bool2,temp_bool3, temp_move_command_list)]
              when "Unnamed"
                temp_command_code = 505
                temp_move_code = temp_parameters[2].split(",")[0].split("=")[1].to_i
                if temp_move_code == 44
                  temp_move_params = [RPG::SE.new(temp_parameters[3].split("=")[1].split(",")[0].split("\"")[1].split("\\")[0], temp_parameters[3].split("=")[3].to_i, temp_parameters[3].split("=")[2].split(",")[0].to_i)]
                elsif temp_move_code == 41
                  temp_move_params = [temp_parameters[2].split("=")[2][1..-2].split(",")[0].split("\"")[1].split("\\")[0], temp_parameters[2].split("=")[2][1..-2].split(",")[1].to_i]
                else
                  temp_move_params = temp_parameters[2].split("=")[2][1..-2].split(",").map(&:to_i)
                end
                temp_add_param = [RPG::MoveCommand.new(temp_move_code,temp_move_params)]
              when "ChangeTransparency"
                temp_command_code = 211
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ShowAnimation"
                temp_command_code = 212
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "ShotBalloonIcon"
                temp_command_code = 213
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "EraseEvent"
                temp_command_code = 214
              when "ChangePlayerFollowers"
                temp_command_code = 216
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "GatherFollowers"
                temp_command_code = 217
              when "FadeoutScreen"
                temp_command_code = 221
              when "FadeinScreen"
                temp_command_code = 222
              when "TintScreen"
                temp_command_code = 223
                red = temp_parameters[2].split("=")[1].split(",")[0].to_f
                green = temp_parameters[2].split("=")[2].split(",")[0].to_f
                blue = temp_parameters[2].split("=")[3].split(",")[0].to_f
                gray = temp_parameters[2].split("=")[4].to_f
                temp_add_param = [Tone.new(red, green, blue, gray), temp_parameters[3].split(",")[1].to_i, temp_parameters[3][1..-2].split(",")[2].strip=="true"]
              when "FlashScreen"
                temp_command_code = 224
                red = temp_parameters[2].split("=")[1].split(",")[0].to_f
                green = temp_parameters[2].split("=")[2].split(",")[0].to_f
                blue = temp_parameters[2].split("=")[3].split(",")[0].to_f
                alpha = temp_parameters[2].split("=")[4].to_f
                temp_add_param = [Color.new(red, green, blue, alpha), temp_parameters[3].split(",")[1].to_i, temp_parameters[3][1..-2].split(",")[2].strip=="true"]
              when "ShakeScreen"
                temp_command_code = 225
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "Wait"
                temp_command_code = 230
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ShowPicture"
                temp_command_code = 231
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i]
                # image filename
                temp_add_param.push(temp_array[1].gsub(/"/, '').strip)
                temp_add_param.push(*temp_array[2..-1].map(&:to_i))
              when "MovePicture"
                temp_command_code = 232
                temp_array = temp_parameters[1][1..-2].split(",")
                # last value is a boolean value, get that out of the way first
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = [temp_array[0].to_i]
                temp_add_param.push(nil) # 2nd value seems to always be null/nil
                temp_add_param.push(*temp_array[2..-1].map(&:to_i))
                temp_add_param.push(temp_boolean) # add the boolean value last
              when "ErasePicture"
                temp_command_code = 235
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "SetWeatherEffects"
                temp_command_code = 236
                temp_add_param = Array.new
                temp_array = temp_parameters[1][1..-2].split(",")
                if temp_array[0] == ":rain"
                  temp_add_param.push(:rain)
                elsif temp_array[0] == ":storm"
                  temp_add_param.push(:storm)
                elsif temp_array[0] == ":snow"
                  temp_add_param.push(:snow)
                else
                  temp_add_param.push(:none)
                end
                temp_add_param.push(temp_array[1].to_i)
                temp_add_param.push(temp_array[2].to_i)
                temp_add_param.push(temp_array[3].strip == "true")
              when "PlayBGM"
                temp_command_code = 241
                temp_add_param = [RPG::BGM.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[1].split("=")[1].to_i, temp_parameters[2].split(",")[2].split("=")[1].to_i)]
              when "FadeoutBGM"
                temp_command_code = 242
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "SaveBGM"
                temp_command_code = 243
              when "ReplayBGM"
                temp_command_code = 244
              when "PlayBGS"
                temp_command_code = 245
                temp_add_param = [RPG::BGS.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "FadeoutBGS"
                temp_command_code = 246
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "PlayME"
                temp_command_code = 249
                temp_add_param = [RPG::ME.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "PlaySE"
                temp_command_code = 250
                temp_add_param = [RPG::SE.new(temp_parameters[2].split(",")[0][8..-3], temp_parameters[2].split(",")[2].split("=")[1].to_i, temp_parameters[2].split(",")[1].split("=")[1].to_i)]
              when "ChangeMapDisplay"
                temp_command_code = 281
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ChangeTileset"
                temp_command_code = 282
                temp_add_param = [temp_parameters[1][1..-2].to_i]
              when "ChangeBattleBack"
                temp_command_code = 283
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0].strip[1..-2],temp_parameters[1][1..-2].split(",")[1].strip[1..-2]]
              when "ChangeParallaxBack"
                temp_command_code = 284
                temp_array = temp_parameters[1][1..-2].split(",")
                last_value = temp_array.pop.to_i
                second_to_last_value = temp_array.pop.to_i
                temp_bool_last = (temp_array.pop.strip == "true")
                temp_bool_first = (temp_array.pop.strip == "true")
                temp_add_param = [temp_array[0][1..-2].strip, temp_bool_first, temp_bool_last, second_to_last_value, last_value]
              when "BattleProcessing"
                temp_command_code = 301
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_bool_last = (temp_array.pop.strip == "true")
                temp_bool_first = (temp_array.pop.strip == "true")
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_bool_first)
                temp_add_param.push(temp_bool_last)
              when "ShopProcessing"
                temp_command_code = 302
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "ChangeHP"
                temp_command_code = 311
                temp_add_param = temp_parameters[1][1..-2].split(",")[0..-2].map(&:to_i)
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[-1].strip == "true")
              when "ChangeState"
                temp_command_code = 313
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "RecoverAll"
                temp_command_code = 314
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeLevel"
                temp_command_code = 316
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_boolean = temp_array.pop.strip == "true"
                temp_add_param = temp_array.map(&:to_i)
                temp_add_param.push(temp_boolean)
              when "ChangeParameters"
                temp_command_code = 317
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeSkills"
                temp_command_code = 318
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeActorName"
                temp_command_code = 320
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i, temp_array[1][2..-2]]
              when "ChangeActorClass"
                temp_command_code = 321
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ChangeActorGraphic"
                temp_command_code = 322
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i]
                temp_add_param.push(temp_array[1][2..-2])
                temp_add_param.push(temp_array[2].to_i)
                temp_add_param.push(temp_array[3][2..-2])
                temp_add_param.push(temp_array[4].to_i)
              when "ChangeVehicleGraphic"
                temp_command_code = 323
                temp_array = temp_parameters[1][1..-2].split(",")
                temp_add_param = [temp_array[0].to_i]
                temp_add_param.push(temp_array[1][2..-2])
                temp_add_param.push(temp_array[2].to_i)
              when "ChangeEnemyState"
                temp_command_code = 333
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ShowBattleAnimation"
                temp_command_code = 337
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "ForceAction"
                temp_command_code = 339
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              when "AbortBattle"
                temp_command_code = 340
              when "OpenMenuScreen"
                temp_command_code = 351
              when "OpenSaveScreen"
                temp_command_code = 352
              when "ReturnToTitleScreen"
                temp_command_code = 354
              when "Script"
                # start custom script
                temp_command_code = 355
                temp_add_param = [templine[9..-4].gsub('\\"','"')]
              when "ScriptMore"
                # continue custom script
                temp_command_code = 655
                temp_add_param = [templine[13..-4].gsub('\\"','"')]
              when "ShowText"
                temp_command_code = 401
                temp_string = templine[11..-4]
                if temp_string.length > 5
                  temp_string = temp_string.split("\") //")[0]
                  temp_string.gsub!("\\\\","\\")
                  temp_string = temp_string[0..4] + temp_string[5..-1].gsub("\\n","\n").gsub("\n[","\\n[").gsub("\nc[","\\nc[").gsub("\na[","\\na[").gsub("\nw[","\\nw[").gsub("\ni[","\\ni[")
                  temp_string = temp_string.gsub("\\\"","\"") if temp_string.include?("\\\"")
                end
                next if temp_string == ""
                temp_add_param = [temp_string]
              when "When"
                temp_command_code = 402
                temp_add_param = [temp_parameters[1][1..-2].split(",")[0].to_i]
                temp_add_param.push(temp_parameters[1][1..-2].split(",")[1][2..-2].gsub("\\\\C","\\C"))
              when "WhenCancel"
                temp_command_code = 403
              when "ChoicesEnd"
                temp_command_code = 404
                templine_indent -= 1
              when "ShowScrollingText"
                temp_command_code = 405
                temp_add_param = [templine[20..-4]]
              when "CommentMore"
                temp_command_code = 408
                temp_add_param = [templine[14..-4]]
              when "Else"
                temp_command_code = 411
              when "BranchEnd"
                temp_command_code = 412
                templine_indent -= 1
              when "RepeatAbove"
                temp_command_code = 413
                templine_indent -= 1
              when "IfWin"
                temp_command_code = 601
                templine_indent += 1
              when "IfEscape"
                temp_command_code = 602
              when "BattleProcessingEnd"
                temp_command_code = 604
                templine_indent -= 1
              when "ShopItem"
                temp_command_code = 605
                temp_add_param = temp_parameters[1][1..-2].split(",").map(&:to_i)
              else
                p "unknown templine #{templine}" if $TEST
              end
          
            # indent needs to be lower for these special cases
            # see codes above to see what they are if needed
            if [102,111,112,402,403,411,601,602].include?(temp_command_code)
              temp_events[temp_event_id][temp_page_id].push(RPG::EventCommand.new(temp_command_code,templine_indent-1,temp_add_param))
            else
              temp_events[temp_event_id][temp_page_id].push(RPG::EventCommand.new(temp_command_code,templine_indent,temp_add_param))
            end
          end
        else
          break unless map_file_data.length > 0
          templine = map_file_data.shift.strip
        end
        break unless map_file_data.length > 0
      end
      return temp_events
    end
    return []
  end
end

class Game_Map
  def setup_events
    @events = {}
    
    map_file_location = "Translated Files\\Maps\\Map#{@map_id.to_s.rjust(3, "0")}.txt" if @map_id
    temp_events = DataManager.read_event_data_from_file(map_file_location)

    @map.events.each do |i, event|
      event.pages.each do |page|
        page.list = temp_events[i][event.pages.index(page)] unless (temp_events[i][event.pages.index(page)].nil? or temp_events[i][event.pages.index(page)].empty?)
      end
      # Default functionality, do not touch
      @events[i] = Game_Event.new(@map_id, event)
    end
    @common_events = parallel_common_events.collect do |common_event|
      Game_CommonEvent.new(common_event.id)
    end
    refresh_tile_events
  end
  #--------------------------------------------------------------------------
  # ● Get map display name
  #--------------------------------------------------------------------------
  def display_name
    line = ""
    map_infos_file = "Translated Files\\MapInfos.txt"
    if File.exists?(map_infos_file)
      lines_from_file = File.readlines(map_infos_file).reject { |x| x.empty? }
      while lines_from_file.count > 0
        map_id_from_line = 0
        line = lines_from_file.shift.strip
        if line.start_with?("MapInfo ")
          map_id_from_line = line[8..-1].to_i
          line = lines_from_file.shift.strip
          @map.display_name = line[8..-2].gsub("\\'","\'") if map_id_from_line == @map_id
        end
      end
    end
    @map.display_name.gsub(/\\V\[(\d+)\]/i) { $game_variables[$1.to_i] }
  end
end