module SaveData
  AutoCommandName = "Load Autosave"
end

module LibraryH
  CALL_COMMAND_NAME = "Scene Recollection"
end

class Window_PopupConfirmInner < Window_Command
  def make_command_list
    add_command("Yes", :ok)
    add_command("No", :cancel)
  end
end

class Window_SelectActorSlot < Window_SelectActor
  def make_command_list
    return unless @item
    add_command("Not equipped", :cancel, true)
    @actors.each do |actor|
      slots = actor.slot_list(@item.etype_id).select { |id| actor.equip_change_ok?(id) }
      slots.each do |slot_id|
        cmd = slots.size == 1 ? actor.name : "#{actor.name}(#{actor.slot_name(slot_id)})"
        add_command(cmd, :ok, true, [actor, slot_id])
      end
    end
  end
end

class Window_BattleEnemy < Window_Selectable
  def draw_item(index)
    e = $game_troop.alive_members[index]
    name = e.name.clone
    color = normal_color
    if @friend_draw
      if !BattleManager.follower_disable? && $game_party.followable?(e)
        color = text_color(23)
      end
      name += "(Affinity:#{e.enemy.ex_dungeon_enemy? ? 'None' : e.friend})"
    end
    change_color(color, enable?(e))
    rect = item_rect_for_text(index)
    e.state_icons[0..1].reverse_each do |icon|
      rect.width -= 26
      draw_icon(icon, rect.x + rect.width, rect.y)
    end
    draw_text(rect, name)
  end
end

class Window_SaveFile < Window_Base
  def draw_playtime(x, y, width, align)
    header = DataManager.load_header(@file_index)
    return unless header
    text = sprintf("Time Played:%s", header[:playtime_s])
    draw_text(x, y, width, line_height, text, align)
  end
  def draw_level(x, y, width, align)
    header = DataManager.load_header(@file_index)
    return unless header
    text = sprintf("Luka Level:%s", header[:luca_level])
    draw_text(x, y, width, line_height, text, align)
  end
end

module Vocab
  #--------------------------------------------------------------------------
  # ○ 基本挿替
  #--------------------------------------------------------------------------
  # Evasion         = "しかし%sは素早くかわした！"
  ActorNoHit      = "But %s quickly dodged!"
  EnemyNoHit      = "But %s quickly dodged!"
  ActorNoDamage   = "%s took no damage!"
  EnemyNoDamage   = "%s took no damage!"
  Block = "%s blocked the attack with their shield!"
  #--------------------------------------------------------------------------
  # ● ユーザ定義
  #--------------------------------------------------------------------------
  Giveup = [
    "Luka yields to temptation and stops fighting!", 
    "His companions desert and leave him to his fate..."
  ]
  BindingStart = [
    "%s is bound!",
    "%s is being raped!",
    "%s is being raped!",
    "But %s is already bound!"
  ]
  TemptationActionFailure = "But Luka has already been defeated!"

  Ability = "Ability"
  Shortage            = "But there isn't enough %s!"
  SkillSealedFailure  = "But that skill is sealed!"
  ObtainJobExp        = "%s job XP gained."  
  Stealed             = "Stole %s from %s!"
  StealFailure        = "Couldn't steal anything from %s!"
  StealedItemEmpty    = "%s has nothing to steal!"
  Stand               = "%s refused to admit defeat!"
  Invalidate          = "It had no effect on %s!"
  DefenseWall         = "%s was defended by a wall!"
  PayLife             = "%s was spent!"
  PayLifeFailure      = "%s was debilitated!"
  OverDriveSuccess    = "%s stopped time!"
  OverDriveFailure    = "But time was already stopped!"
  BindResistSuccess   = "...and escaped from %s's restraint!"
  BindResistFailure   = "...but couldn't escape from %s's restraint!"
  EternalBindResist   = "...but is still being held down by %s!"
  PleasureFinished    = " came!"
  Predation           = "%s was devoured!"
  ReStoration         = "%s absorbed %s!"
  ThrowItem           = "%s threw a %s!"
  PhysicalReflection = "%s reflects the attack!"
  NotObtainItem = "%s obtained but you can't carry any more..."
  ACTOR_DAMAGE = "%s takes %s %s damage!"
  ENEMY_DAMAGE = "%s takes %s %s damage!"
  STATE_BOOST = "Condition bonus!!"
  EX_CATEGORY_BOOST = "Slayer bonus!!"

  AUTOBATTLE = "Auto"
  BATTLE_STATES = "Status"
  ENCHANT_STONE = "Gem"
  ClassLevel = "Job Level"
  TribeLevel = "Race Level"
  
  module AutoBattle
    NORMAL = "Random"
    NOT_MP_SKILL = "Conserve MP"
    REPEAT = "Repeat"
    ATTACK_ONLY = "Normal Attack"
  end

  PartySaveMessage = "Which slot do you want to register a party?"
  PartyLoadMessage = "Which party do you want to call?"

  class << self
    # 能力値 (短)
    def params_a(param_id)
      ["ATK", "DEF", "MAG", "WIL", "AGI", "DEX"][param_id]
    end

    # 仮想キー
    def key_a
      { :gamepad => "1", :keyboard => "Shift" }[$game_system.conf[:key_text]]
    end

    def key_b
      { :gamepad => "2", :keyboard => "X" }[$game_system.conf[:key_text]]
    end

    def key_c
      { :gamepad => "3", :keyboard => "Z" }[$game_system.conf[:key_text]]
    end

    def key_x
      { :gamepad => "4", :keyboard => "A" }[$game_system.conf[:key_text]]
    end

    def key_y
      { :gamepad => "5", :keyboard => "S" }[$game_system.conf[:key_text]]
    end

    def key_z
      { :gamepad => "6", :keyboard => "D" }[$game_system.conf[:key_text]]
    end

    def key_l
      { :gamepad => "7", :keyboard => "Q" }[$game_system.conf[:key_text]]
    end

    def key_r
      { :gamepad => "8", :keyboard => "W" }[$game_system.conf[:key_text]]
    end

    # パーティコマンド
    def giveup
      "Give Up"
    end

    def shift_change
      "Party"
    end

    def library
      "Library"
    end

    def config
      "Config"
    end

    def all_attack
      "Auto Atk"
    end

    def item_get_message(item, value, is_display_value = false)
      message = item.window_name
      if value == 0
        format(NotObtainItem, message)
      else
        #Below was reordered so the number is leading(before the item that is obtained)
        format(ObtainItem,(item.enchant_item? || !is_display_value ? "":" #{value}") + message )
      end
    end
  end
end
  
module Help
  class << self
    #--------------------------------------------------------------------------
    # ● アビリティ画面の上部ヘルプメッセージ
    #--------------------------------------------------------------------------
    def ability
      "Please choose an ability type"
    end

    #--------------------------------------------------------------------------
    # ● アビリティ画面の下部ヘルプメッセージ
    #--------------------------------------------------------------------------
    def ability_key
      "#{Vocab.key_c}:Select　#{Vocab.key_b}:Cancel　#{Vocab.key_a}:Remove".force_encoding('UTF-8').encode
    end

    #--------------------------------------------------------------------------
    # ● スキル画面の下部ヘルプメッセージ
    #--------------------------------------------------------------------------
    def skill_type_key
      t = ""
      t += "\\C[0]"
      t += "#{Vocab.key_a}:Hide in battle + Disable in auto mode"
      t += "(Disabled)" unless $game_system.conf[:bt_stype]
      t += "\n"
      t += "\\C[16]" if Input.press?(:X)
      t += "#{Vocab.key_x}＋↑/↓:Sort by skill type"
      t += "#{Vocab.key_x}＋#{Vocab.key_b}:Reset skill order"
      t.force_encoding('UTF-8').encode
    end

    #--------------------------------------------------------------------------
    # ● コンフィグ-色調のヘルプメッセージ
    #--------------------------------------------------------------------------
    def config_tone
      [
        "Set red value.\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "Set green value.\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "Set blue value.\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "Restore defaults.",
        "Return"
      ]
    end

    #--------------------------------------------------------------------------
    # ● コンフィグ-音量のヘルプメッセージ
    #--------------------------------------------------------------------------
    def config_sound
      [
        "BGM Volume\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "BGS Volume\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "ME Volume\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "SE Volume\r\n←/→:-/+#{Vocab.key_l}/#{Vocab.key_r}:Big -/+",
        "Restore defaults.",
        "Return."
      ]
    end

    #--------------------------------------------------------------------------
    # ● パーティ編成画面のヘルプメッセージ
    #--------------------------------------------------------------------------
    def party_edit
      [
        "#{Vocab.key_a}:Remove",
        "#{Vocab.key_x}:Status",
        "#{Vocab.key_y}:Sort",
        "#{Vocab.key_z}:Warp To",
      ]
    end

    #--------------------------------------------------------------------------
    # ● 転職画面のヘルプメッセージ
    #--------------------------------------------------------------------------
    def job_change
      ["#{Vocab.key_y}/#{Vocab.key_z}:Sort"]
    end

    #--------------------------------------------------------------------------
    # ● スロット画面の操作説明テキスト
    #--------------------------------------------------------------------------
    def slot_description
      {
        :stand => "→:Increase Wager　#{Vocab.key_c}:Spin Slots\n←:Decrease Wager　#{Vocab.key_b}:Quit".force_encoding('UTF-8').encode,
        :play  => "#{Vocab.key_c}:Stop Reel"
      }
    end

    #--------------------------------------------------------------------------
    # ● ショップ画面の装備品情報変更テキスト
    #--------------------------------------------------------------------------
    def shop_equip_change
      "←/→:Info Change".force_encoding('UTF-8').encode
    end

    #--------------------------------------------------------------------------
    # ● ショップ画面の装備品性能比較テキスト
    #--------------------------------------------------------------------------
    def shop_param_compare
      "#{Vocab.key_x}:Stat Change"
    end

    #--------------------------------------------------------------------------
    # ● 性能差比較　装備情報ウインドウのボタン説明１
    #--------------------------------------------------------------------------
    def item_info_change
      "#{Vocab.key_x}:Info Change"
    end

    #--------------------------------------------------------------------------
    # ● 性能差比較　装備情報ウインドウのボタン説明２
    #--------------------------------------------------------------------------
    def item_info_exit
      "#{Vocab.key_x}:Close"
    end

    #--------------------------------------------------------------------------
    # ● 図鑑画面のヘルプメッセージ
    #--------------------------------------------------------------------------
    def library
      {
        :blank      => "This entry is blank.",
        :discovery  => "This entry's details are unknown.",
        :return_top => "Return to top.",
        :close_lib  => "Close and return to last screen.",
        :btn_detail => "#{Vocab.key_c}:View Details",
        :btn_column => "↑/↓:Select".force_encoding('UTF-8').encode,
        :btn_jump   => "#{Vocab.key_l}/#{Vocab.key_r}:Jump",
        :btn_page   => "←/→:Page".force_encoding('UTF-8').encode,
        :btn_scroll => "#{Vocab.key_y}/#{Vocab.key_z}:Scroll Text",
        :btn_equip => "#{Vocab.key_x}:Equip Info"
      }
    end

    #--------------------------------------------------------------------------
    # ● 装備品情報変更テキスト
    #--------------------------------------------------------------------------
    def equip_info_change
      "←/→,#{Vocab.key_l}/#{Vocab.key_r}:Switch info page".force_encoding('UTF-8').encode
    end

    #--------------------------------------------------------------------------
    # ● 装備品情報変更テキスト
    #--------------------------------------------------------------------------
    def equip_info_next
      "#{Vocab.key_x}:Next page"
    end

    #--------------------------------------------------------------------------
    # ● 装備品情報変更テキスト
    #--------------------------------------------------------------------------
    def equip_info_hide
      "#{Vocab.key_b}:Close info page"
    end
  end
end

module NWConst::Config
  CONTENTS = [
    {:key => :window_tone,  :name => "Window Color", :sub => false,
     :help => "Change the Window Color"},  
    {:key => :sound_volume, :name => "Volume Settings", :sub => false,
     :help => "Change in-game volume."},
    {:key => :key_text,     :name => "Button Explanation", :sub => true,
     :help => "Change display of button help.\r\n←/→ Select"},
    {:key => :map_dash,     :name => "Auto Dash", :sub => true,
     :help => "[Map]Change whether you walk or dash by default.\r\n←/→ "},
    {:key => :map_speed,   :name => "Dash Speed", :sub => true,
     :help => "[Map]Change dash speed.\r\n←/→ Slower/Faster"},
    {:key => :bt_skip,      :name => "Speech Cut-ins", :sub => true,
     :help => "[Battle]Change display of battle speech cut-ins.\r\n←/→ Select"},
    {:key => :bt_auto,      :name => "Combat Log", :sub => true,
     :help => "[Battle]Change combat log display setting.\r\n←/→ Select"},
    {:key => :bt_wait,      :name => "Battle Wait", :sub => true,
     :help => "[Battle]Change battle wait settings.\r\n←/→ Select"},
    {:key => :bt_result,    :name => "Battle Result Speed", :sub => true,
     :help => "[Battle]Change battle result speed when using button eval<Vocab.key_x>.\r\n←/→Select"},   
    {:key => :bt_stype,     :name => "Display Skill Types", :sub => true,
     :help => "[Battle]Change whether to display skill types in battle.\r\n←/→ Select"},
    {:key => :ls_auto,      :name => "Auto Advance Mode", :sub => true,
     :help => "[Defeat Event]Enable/Disable Auto Mode in H-Scenes.\r\n←/→ On/Off"},
    {:key => :ls_wait,      :name => "Auto Wait", :sub => true,
     :help => "[Defeat Event]Change Auto Text Speed.\r\n←/→ Slower/Faster"},
    {:key => :ls_predation, :name => "Skip Vore Scenes", :sub => true,
     :help => "[Defeat Event]Choose whether to hide vore scenes.\r\n←/→ On/Off"},     
    {:key => :ls_skip,      :name => "Loss Events Skip", :sub => true,
     :help => "Change settings for loss events\r\n←/→ Select"},
    {:key => :default,      :name => "Defaults", :sub => false,
     :help => "Reset all settings to the default."},
    {:key => :return, :name => "Return", :sub => false,
     :help => "Return to Main Menu"},
  ]
  DATA_TEXT = {
    :key_text => {
      :gamepad => {:name => "Gamepad", :help => "Display help using gamepad keys"},
      :keyboard => {:name => "Keyboard", :help => "Display help using keyboard keys"},
    },
    :map_dash => {
      false => {:name => "Walk", :help => "Walk by default"},
      true  => {:name => "Dash", :help => "Dash by default"},
    },
    :map_speed => {
      0 => {:name => "Normal", :help => "Normal dash speed"},
      1 => {:name => "Fast", :help => "Fast dash speed"},
      2 => {:name => "Fastest", :help => "Fastest dash speed"},      
    },
    :bt_skip => {
      false => {:name => "Display", :help => "Display battle lines and cut-ins"},
      true  => {:name => "Omit", :help => "Skip battle lines and cut-ins"},
    },
    :bt_auto => {
      false => {:name => "Manual", :help => "Manual advance for battle messages"},
      true  => {:name => "Auto", :help => "Automatic advance for battle messages"},
    },
    :bt_wait => {
      100 => {:name => "Normal", :help => "Default wait times in battle"},
      50 =>  {:name => "Fast", :help => "Half wait times in battle"},
      25 =>  {:name => "Fastest", :help => "Quarter wait times in battle"},      
    },
    :bt_result => {
      0   => {:name => "No Skip", :help => "Do not skip"},
      1   => {:name => "Slow", :help => "Wait on each character"},
      2   => {:name => "Fast", :help => "Wait on each line or page"},
      nil => {:name => "Instant", :help => "Skip all at once"},
    },
    :bt_stype => {
      true => {:name => "Hide", :help => "Hide selected skill types in battle"},
      false  => {:name => "Show all", :help => "Display all skill types in battle"},
    },
    :ls_auto => {
      false => {:name => "Off", :help => "Do not use auto mode during scenes"},
      true  => {:name => "On", :help => "Use auto mode during scenes"},
    },
    :ls_wait => {
      10 => {:name => "Slow", :help => "Pause twice as long during scenes"},
      5 =>  {:name => "Normal", :help => "Use default timings during scenes"},
      3 =>  {:name => "Fast", :help => "Pause half as long during scenes"},      
    },
    :ls_predation => {
      false => {:name => "Watch", :help => "Display vore scenes"},
      true  => {:name => "Skip", :help => "Skip vore scenes"},
    },
    :ls_skip => {
      0 => {:name => "Always View", :help => "Always show loss scenes"},
      1 => {:name => "Skip Already Seen", :help => "Only show new loss scenes"},
      2 => {:name => "Always Ask", :help => "Ask whether to show each loss scene"},
    },
  }
end

class Window_MenuCommand < Window_Command
  def add_original_commands
    add_command("Library", :library)
    add_command("Config", :config)
  end
end

module NWConst::Library
  INDEX_STRING = {
    :lib_top        => "Library",
    :lib_return     => "Return to Top",
    :lib_close      => "Close Library",
    :memory_close   => "End Scene",
    :lib_actor      => "Character Book",
    :lib_enemy      => "Monster Book",
    :lib_weapon     => "Weapon Book",
    :lib_armor      => "Armor Book",
    :lib_accessory  => "Accessory Book",
    :lib_item       => "Item Book",
    :lib_record     => "Adventure Log",
    :lib_medal      => "Achievements",
    :lib_class      => "Job Info",
    :lib_tribe      => "Race Info",
  }
  RECORD_STRING = {
    1 => "Current Adventure",
    2 => "All Adventures",
  }
  ENCOUNTER_ENEMY_PLACE_NAME = "Encountered in:"
  GET_ITEM_PLACE_NAME = "Acquired from:"
  GET_ITEM_DROP_NAME  = "Dropped by:"
  GET_ITEM_STEAL_NAME = "Stolen from:"
  GET_ITEM_ACTOR_NAME = "Received from ally:"  
  GET_ITEM_NO_NAME    = "None"
  GET_ITEM_ETC_NAME   = " etc."

 #Enemy Special Names
  ENEMY_SPECIAL_NAME = [
    ["バニースライム娘（ボス）", "Bunny Slime (Boss)"],
    ["ヌルコ", "Nuruko"],
    ["ダークエルフ（剣）", "Dark Elf Fencer"],
    ["ダークエルフ（召喚）", "Dark Elf Mage"],
    ["ゾンビ娘Ａ", "Zombie Girl A"],
    ["ゾンビ娘Ｂ", "Zombie Girl B"],
    ["ゾンビ娘Ｃ", "Zombie Girl C"],
    ["ゾンビ娘Ｄ", "Zombie Girl D"],
    ["フェアリーズＡ", "Fairies A"],
    ["フェアリーズＢ", "Fairies B"],
    ["フェアリーズＣ", "Fairies C"],
    ["フェアリーズＤ", "Fairies D"],
    ["フェアリーズＥ", "Fairies E"],
    ["フェアリーズＦ", "Fairies F"],
    ["リザードシーフＡ", "Lizard Thief A"],
    ["リザードシーフＢ", "Lizard Thief B"],
    ["アリスフィーズ16世（冥府）", "Alipheese The 16th (Hades)"],
    ["ラナエル（異世界）", "Archangel Ranael (Alt)"],
    ["ナガエル（異世界）", "Principality Nagael (Alt)"],
    ["一反木綿娘Ａ", "Ittan-Momen A"],
    ["一反木綿娘Ｂ", "Ittan-Momen B"],
    ["一反木綿娘Ｃ", "Ittan-Momen C"],
    ["カエル娘Ａ", "Frog Girl A"],
    ["カエル娘Ｂ", "Frog Girl B"],
    ["アルマエルマ（1回目）", "Alma Elma (1)"],
    ["グランベリア（1回目）", "Granberia (1)"],
    ["グノーシス（1回目）", "Gnosis (1)"],
    ["シオン（1回目）", "Zion (1)"],
    ["グールＡ", "Ghoul A"],
    ["グールＢ", "Ghoul B"],
    ["グールＣ", "Ghoul C"],
    ["リリス（1回目）", "Lilith (1)"],
    ["ベルゼバブＡ", "Beelzebub A"],
    ["ベルゼバブＢ", "Beelzebub B"],
    ["ベルゼバブＣ", "Beelzebub C"],
    ["サキュバスＡ", "Succubus A"],
    ["サキュバスＢ", "Succubus B"],
    ["サキュバスＣ", "Succubus C"],
    ["サキュバスＤ", "Succubus D"],
    ["グランベリア（2回目）", "Granberia (2)"],
    ["ドリアード（緑）", "Dryad"],
    ["ドリアード（青）", "Dark Dryad"],
    ["アラクネＡ", "Arachne A"],
    ["アラクネＢ", "Arachne B"],
    ["アラクネＣ", "Arachne C"],
    ["黒華（1回目）", "Black Dahlia (1)"],
    ["黒蛇（1回目）", "Black Mamba (1)"],
    ["黒薔薇（1回目）", "Black Rose (1)"],
    ["黒のアリス（第1形態）", "Black Alice (1st Form)"],
    ["黒のアリス（第2形態）", "Black Alice (2nd Form)"],
    ["黒のアリス（第3形態）", "Black Alice (3rd Form)"],
    ["天使兵Ａ", "Angel Soldier A"],
    ["天使兵Ｂ", "Angel Soldier B"],
    ["天使兵Ｃ", "Angel Soldier C"],
    ["天使兵Ｄ", "Angel Soldier D"],
    ["天使兵Ｅ", "Angel Soldier E"],
    ["トリニティＡ", "Trinity A"],
    ["トリニティＢ", "Trinity B"],
    ["トリニティＣ", "Trinity C"],
    ["エデン（1回目）", "Eden (1)"],
    ["フェルナンデス（ゾンビ）", "Fernandez (Zombie)"],
    ["タイタニア（ゾンビ）", "Titania (Zombie)"],
    ["ロザ（ゾンビ）", "Roza (Zombie)"],
    ["クィーンラミア（ゾンビ）", "Queen Lamia (Zombie)"],
    ["クィーンハーピー（ゾンビ）", "Former Queen Harpy (Zombie)"],
    ["クィーンスキュラ（ゾンビ）", "Queen Scylla (Zombie)"],
    ["アルマエルマ（2回目）", "Alma Elma (2)"],
    ["ギルゴーン（1回目）", "Gilgorn (1)"],
    ["サキュバス（SQ）", "Nightmare (SQ)"],
    ["ベル（1回目）", "Bell (1)"],
    ["リラ（1回目）", "Lyla (1)"],
    ["ミュゼット（1回目）", "Musette (1)"],
    ["フルビュア（1回目）", "Fulbeua (1)"],
    ["妲己（1回目）", "Daji (1)"],
    ["エスト（1回目）", "Est (1)"],
    ["エスト（最終）", "Est (Final)"],
    ["エスト（2回目）", "Est (2)"],
    ["ベル（2回目）", "Bell (2)"],
    ["リラ（2回目）", "Lyla (2)"],
    ["ミュゼット（2回目）", "Musette (2)"],
    ["フルビュア（2回目）", "Fulbeua (2)"],
    ["ギルゴーン（2回目）", "Gilgorn (2)"],
    ["大明海", "Daimyokai"],
    ["妲己（2回目）", "Daji (2)"],
    ["サキュバス（半夢魔）", "Demi-Nightmare"],
    ["サキュバス（SR）", "Nightmare (SR)"],
    ["カサンドラ（大）", "Cassandra (Restored)"],
  ]

  #Race Special Names
  RACE_SPECIAL_NAME = [
    ["メイン", "Main"],
    ["アポトーシス", "Apoptosis"],
    ["エルフ", "Elf"],
    ["キメラ", "Chimera"],
    ["ゴースト", "Ghost"],
    ["スキュラ", "Scylla"],
    ["スライム", "Slime"],
    ["その他", "Other"],
    ["ゾンビ", "Zombie"],
    ["ドール", "Doll"],
    ["ラミア", "Lamia"],
    ["ロイド", "Roid"],
    ["亜人", "Demi-Human"],
    ["人間", "Human"],
    ["人魚", "Mermaid"],
    ["吸血鬼", "Vampire"],
    ["天使", "Angel"],
    ["女神", "Goddess"],
    ["妖狐", "Kitsune"],
    ["妖精", "Fairy"],
    ["妖魔", "Yoma"],
    ["植物", "Plant"],
    ["海棲種", "Sea-Dweller"],
    ["淫魔", "Succubus"],
    ["竜", "Dragon"],
    ["虫", "Insect"],
    ["邪神", "Evil Goddess"],
    ["陸棲種", "Land-Dweller"],
    ["魔獣", "Beast"],
    ["魔王", "Monster Lord"],
    ["鳥", "Harpy"],
    ["コラボ", "Collab"],
  ]

  #Skill Special Names
  SKILL_SPECIAL_NAME = [
    ["アークイビー（女）", "Arc Ivy (F)"],
    ["アークイビー（男）", "Arc Ivy (M)"],
    ["あかなめ乳首這い（女）", "Akaname Nipple Crawl (F)"],
    ["あかなめ乳首這い（男）", "Akaname Nipple Crawl (M)"],
    ["あかなめ舌園（女）", "Akaname Tongue Garden (F)"],
    ["あかなめ舌園（発動）", "Akaname Tongue Garden (Init)"],
    ["あかなめ舌園（継続）", "Akaname Tongue Garden (Cont)"],
    ["アクアストローク（女）", "Aqua Stroke (F)"],
    ["アクアストローク（男）", "Aqua Stroke (M)"],
    ["アクアプリズン（女）", "Aqua Prison (F)"],
    ["アクアプリズン（男）", "Aqua Prison (M)"],
    ["アゲハ口腔吸精（女）", "Swallowtail's Sucking (F)"],
    ["アゲハ口腔吸精（男）", "Swallowtail's Sucking (M)"],
    ["アゲハ袖愛撫（女）", "Swallowtail's Sleeve Caress (F)"],
    ["アゲハ袖愛撫（男）", "Swallowtail's Sleeve Caress (M)"],
    ["アネモネホールド（女）", "Anemone Hold (F)"],
    ["アネモネホールド（男）", "Anemone Hold (M)"],
    ["アビスドレイン（女）", "Abyss Drain (F)"],
    ["アビスドレイン（発動）", "Abyss Drain (Init)"],
    ["アビスドレイン（継続）", "Abyss Drain (Cont)"],
    ["アメーバードレイン（女）", "Amoeba Drain (F)"],
    ["アメーバードレイン（男）", "Amoeba Drain (M)"],
    ["アメーバーヘイズ（女）", "Amoeba Haze (F)"],
    ["アメーバーヘイズ（男）", "Amoeba Haze (M)"],
    ["アラクネスレッド（女）", "Arachne Thread (F)"],
    ["アラクネスレッド（男）", "Arachne Thread (M)"],
    ["アラクネ・リプロア（発動）", "Arachne Repro (Init)"],
    ["アラクネ・リプロア（継続）", "Arachne Repro (Cont)"],
    ["アラクネルイン（発動）", "Arachne Ruin (Init)"],
    ["アラクネルイン（継続）", "Arachne Ruin (Cont)"],
    ["アリゲーターボア（女）", "Alligator Vore (F)"],
    ["アリゲーターボア（男）", "Alligator Vore (M)"],
    ["アルケミスエウリア（女）", "Alchemist Euria (F)"],
    ["アルケミスエウリア（男）", "Alchemist Euria (M)"],
    ["アンシェントレイプ（発動）", "Ancient Rape (Init)"],
    ["アンシェントレイプ（継続）", "Ancient Rape (Cont)"],
    ["イクステンタクル（女）", "EX Tentacle (F)"],
    ["イクステンタクル（男）", "EX Tentacle (M)"],
    ["インフェルノ・デ・スキュラ（発動）", "Inferno de Scylla (Init)"],
    ["インフェルノ・デ・スキュラ（継続）", "Inferno de Scylla (Cont)"],
    ["インモラルレイド（女）", "Immoral Raid (F)"],
    ["インモラルレイド（男）", "Immoral Raid (M)"],
    ["ヴァルト機檻（女）", "Valto Mech Cage (F)"],
    ["ヴァルト機檻（発動）", "Valto Mech Cage (Init)"],
    ["ヴァルト機檻（継続）", "Valto Mech Cage (Cont)"],
    ["ヴェータラボア（女）", "Vetala Vore (F)"],
    ["ヴェータラボア（発動）", "Vetala Vore (Init)"],
    ["ヴェータラボア（継続）", "Vetala Vore (Cont)"],
    ["エアレイプ（発動）", "Air Rape (Init)"],
    ["エアレイプ（継続）", "Air Rape (Cont)"],
    ["エクスタシーキュラス（発動）", "Ecstasy Caress (Init)"],
    ["エクスタシーキュラス（継続）", "Ecstasy Caress (Cont)"],
    ["エクスタシーワーム（女）", "Ecstasy Worm (F)"],
    ["エクスタシーワーム（男）", "Ecstasy Worm (M)"],
    ["エミリのおもちゃ（女）", "Emily's Toy (F)"],
    ["エミリのおもちゃ（男）", "Emily's Toy (M)"],
    ["エリザベートの吸血（女）", "Elizabeth's Bloodsucking (F)"],
    ["エリザベートの吸血（男）", "Elizabeth's Bloodsucking (M)"],
    ["エルフの魔膣", "Elven Queen's Vagina"],
    ["エロスハンド（女）", "Eros's Hand (F)"],
    ["エロスハンド（男）", "Eros's Hand (M)"],
    ["カーミラの吸血（女）", "Carmilla's Blood Sucking(F)"],
    ["カーミラの吸血（男）", "Carmilla's Blood Sucking(M)"],
    ["からくりおっぱいギミック（女）", "Breast Doll's Gimmick (F)"],
    ["からくりおっぱいギミック（男）", "Breast Doll's Gimmick (M)"],
    ["からくりの手淫（女）", "Mechanical Hand Job (F)"],
    ["からくりの手淫（男）", "Mechanical Hand Job (M)"],
    ["カレス・イビー（女）", "Ivy Caress (F)"],
    ["カレス・イビー（男）", "Ivy Caress (M)"],
    ["カレスワーム（女）", "Worm Caress (F)"],
    ["カレスワーム（男）", "Worm Caress (M)"],
    ["キューブヘブン（女）", "Cube Heaven (F)"],
    ["キューブヘブン（男）", "Cube Heaven (M)"],
    ["グリーンドロウ（女）", "Green Draw (F)"],
    ["グリーンドロウ（男）", "Green Draw (M)"],
    ["グリズリーレイプ（発動）", "Grizzly Rape (Init)"],
    ["グリズリーレイプ（継続）", "Grizzly Rape (Cont)"],
    ["ケンタウロスレイプ（発動）", "Kentauros Rape (Init)"],
    ["ケンタウロスレイプ（継続）", "Kentauros Rape (Cont)"],
    ["ゴーストレイプ（発動）", "Ghost Rape (Init)"],
    ["ゴーストレイプ（継続）", "Ghost Rape (Cont)"],
    ["コブラアナリシア（女）", "Cobra Anal (F)"],
    ["コブラアナリシア（男）", "Cobra Anal (M)"],
    ["ご奉仕手淫（女）", "Service:Hand (F)"],
    ["ご奉仕手淫（男）", "Service:Hand (M)"],
    ["ご奉仕口淫（女）", "Service:Mouth (F)"],
    ["ご奉仕口淫（男）", "Service:Mouth (M)"],
    ["ご奉仕吸精（発動）", "Service:Sucking (Init)"],
    ["ご奉仕吸精（継続）", "Service:Sucking (Cont)"],
    ["ご奉仕天国（女）", "Service:Heaven (F)"],
    ["ご奉仕天国（男）", "Service:Heaven (M)"],
    ["ご奉仕締め上げ（女）", "Service:Holding (F)"],
    ["ご奉仕締め上げ（男）", "Service:Holding (M)"],
    ["ご奉仕触手（女）", "Service:Tentacle (F)"],
    ["ご奉仕触手（男）", "Service:Tentacle (M)"],
    ["サイコドレイン（女）", "Psycho Drain (F)"],
    ["サイコドレイン（男）", "Psycho Drain (M)"],
    ["サタニックプレッシャー（女）", "Satanic Pressure (F)"],
    ["サタニックプレッシャー（男）", "Satanic Pressure (M)"],
    ["サボテン令嬢の受精（発動）", "Cactus Pollination (Init)"],
    ["サボテン令嬢の受精（継続）", "Cactus Pollination (Cont)"],
    ["サボレスボーン（女）", "Sables Bon (F)"],
    ["サボレスボーン（発動）", "Sables Bon (Init)"],
    ["サボレスボーン（継続）", "Sables Bon (Cont)"],
    ["ジェネラルテイル（女）", "General's Tail (F)"],
    ["ジェネラルテイル（男）", "General's Tail (M)"],
    ["ジェネラルレイプ（発動）", "General's Rape (Init)"],
    ["ジェネラルレイプ（継続）", "General's Rape (Cont)"],
    ["ジェリードロウ（女）", "Jelly Draw (F)"],
    ["ジェリードロウ（男）", "Jelly Draw (M)"],
    ["ジェリーヘブン（女）", "Jelly Heaven (F)"],
    ["ジェリーヘブン（発動）", "Jelly Heaven (Init)"],
    ["ジェリーヘブン（継続）", "Jelly Heaven (Cont)"],
    ["ジェルプリズン（女）", "Gel Prison (F)"],
    ["ジェルプリズン（発動）", "Gel Prison (Init)"],
    ["ジェルプリズン（継続）", "Gel Prison (Cont)"],
    ["シスターレイプ（発動）", "Nun Rape (Init)"],
    ["シスターレイプ（継続）", "Nun Rape (Cont)"],
    ["しゃぶる（女）", "Suck (F)"],
    ["しゃぶる（男）", "Suck (M)"],
    ["シャングリラスクリュー（女）", "Shangri-La Screw (F)"],
    ["シャングリラスクリュー（男）", "Shangri-La Screw (M)"],
    ["ジントレル採精器（女）", "Syringe Extraction Method (F)"],
    ["ジントレル採精器（男）", "Syringe Extraction Method (M)"],
    ["スカートじゅるじゅる（女）", "Sucking Skirt (F)"],
    ["スカートじゅるじゅる（男）", "Sucking Skirt (M)"],
    ["ストーンレイプ（発動）", "Stone Rape (Init)"],
    ["ストーンレイプ（継続）", "Stone Rape (Cont)"],
    ["スネークエンド（発動）", "Snake End (Init)"],
    ["スネークエンド（継続）", "Snake End (Cont)"],
    ["スネークハンズ（女）", "Snake Hands (F)"],
    ["スネークハンズ（男）", "Snake Hands (M)"],
    ["スプラッシュタン（女）", "Tongue Splash (F)"],
    ["スプラッシュタン（男）", "Tongue Splash (M)"],
    ["スプレッドワーム（女）", "Worm Spread (F)"],
    ["スプレッドワーム（男）", "Worm Spread (M)"],
    ["スライム股間責め（女）", "Slime Genital Teasing (F)"],
    ["スライム股間責め（男）", "Slime Genital Teasing (M)"],
    ["せるか・すなめ（女）", "Sacrifice Tasting (F)"],
    ["せるか・すなめ（男）", "Sacrifice Tasting (M)"],
    ["ダチョウレイプ（発動）", "Ostrich Rape (Init)"],
    ["ダチョウレイプ（継続）", "Ostrich Rape (Cont)"],
    ["ツインエナジードレイン（発動）", "Twin Energy Drain (Init)"],
    ["ツインエナジードレイン（継続）", "Twin Energy Drain (Cont)"],
    ["ツインドレインテイル（女）", "Twin Tail Drain (F)"],
    ["ツインドレインテイル（男）", "Twin Tail Drain (M)"],
    ["ツタ拘束（女）", "Ivy Restraint (F)"],
    ["ツタ拘束（男）", "Ivy Restraint (M)"],
    ["テイルドレイン・ドーラ（女）", "Tail Drain:Dora (F)"],
    ["テイルドレイン・ドーラ（発動）", "Tail Drain:Dora (Init)"],
    ["テイルドレイン・ドーラ（継続）", "Tail Drain:Dora (Cont)"],
    ["テンタクルカレス（女）", "Tentacle Caress (F)"],
    ["テンタクルカレス（男）", "Tentacle Caress (M)"],
    ["トランシルヴァニアの夜陰（発動）", "Transylvania Midnight (Init)"],
    ["トランシルヴァニアの夜陰（継続）", "Transylvania Midnight (Cont)"],
    ["ドレインハンド（女）", "Drain Hand (F)"],
    ["ドレインハンド（男）", "Drain Hand (M)"],
    ["ドレインワーム（女）", "Drain Worm (F)"],
    ["ドレインワーム（男）", "Drain Worm (M)"],
    ["なめなめ（女）", "Lick (F)"],
    ["なめなめ（男）", "Lick (M)"],
    ["ヌルヌル触手股間責め（女）", "Slimy Tentacle Crotch Torture (F)"],
    ["ヌルヌル触手股間責め（男）", "Slimy Tentacle Crotch Torture (M)"],
    ["ネバネバ腺毛（女）", "Sticky Hairs (F)"],
    ["ネバネバ腺毛（男）", "Sticky Hairs (M)"],
    ["パープルドロウ（女）", "Purple Draw (F)"],
    ["パープルドロウ（男）", "Purple Draw (M)"],
    ["バキュームテイル（女）", "Vacuum Tail (F)"],
    ["バキュームテイル（男）", "Vacuum Tail (M)"],
    ["ハピネス・ロンド（発動）", "Happiness Rondo (Init)"],
    ["ハピネス・ロンド（継続）", "Happiness Rondo (Cont)"],
    ["バブルズシェイク（発動）", "Bubble Shake (Init)"],
    ["バブルズシェイク（継続）", "Bubble Shake (Cont)"],
    ["パラライズワーム（女）", "Paralyze Worm (F)"],
    ["パラライズワーム（男）", "Paralyze Worm (M)"],
    ["ビーナストラップ（女）", "Venus Trap (F)"],
    ["ビーナストラップ（男）", "Venus Trap (M)"],
    ["ビーナスベンド（女）", "Venus Bend (F)"],
    ["ビーナスベンド（男）", "Venus Bend (M)"],
    ["ヒッププレス（発動）", "Ass Press (Init)"],
    ["ヒッププレス（継続）", "Ass Press (Cont)"],
    ["ヒトデカレス（女）", "Starfish Caress (F)"],
    ["ヒトデカレス（男）", "Starfish Caress (M)"],
    ["ヒトデしがみつき（女）", "Starfish Cling (F)"],
    ["ヒトデしがみつき（男）", "Starfish Cling (M)"],
    ["ファナティックダリア（女）", "Fanatic Dahlia (F)"],
    ["ファナティックダリア（男）", "Fanatic Dahlia (M)"],
    ["プラントサック（女）", "Plant Suck (F)"],
    ["プラントサック（男）", "Plant Suck (M)"],
    ["プリエステス・ヘア（女）", "Priestess Hair (F)"],
    ["プリエステス・ヘア（男）", "Priestess Hair (M)"],
    ["プリンセスえっち（発動）", "Princess's Perversion (Init)"],
    ["プリンセスえっち（継続）", "Princess's Perversion (Cont)"],
    ["プリンセス尾ヒレずり（女）", "Princess's Tail Rub (F)"],
    ["プリンセス尾ヒレずり（男）", "Princess's Tail Rub (M)"],
    ["ブルードロウ（女）", "Blue Draw (F)"],
    ["ブルードロウ（男）", "Blue Draw (M)"],
    ["ブロブドロウ（女）", "Blob Draw (F)"],
    ["ブロブドロウ（男）", "Blob Draw (M)"],
    ["ブロブヘブン（女）", "Blob Heaven (F)"],
    ["ブロブヘブン（発動）", "Blob Heaven (Init)"],
    ["ブロブヘブン（継続）", "Blob Heaven (Cont)"],
    ["ぺトラ・スコルピオ（女）", "Petra Scorpio (F)"],
    ["ぺトラ・スコルピオ（発動）", "Petra Scorpio (Init)"],
    ["ぺトラ・スコルピオ（継続）", "Petra Scorpio (Cont)"],
    ["ヘブンズバスト（女）", "Heaven's Bust (F)"],
    ["ヘブンズバスト（発動）", "Heaven's Bust (Init)"],
    ["ヘブンズバスト（継続）", "Heaven's Bust (Cont)"],
    ["ヘブンズプリズン（女）", "Heaven's Prison (F)"],
    ["ヘブンズプリズン（発動）", "Heaven's Prison (Init)"],
    ["ヘブンズプリズン（継続）", "Heaven's Prison (Cont)"],
    ["ボアワーム（女）", "Vore Worm (F)"],
    ["ボアワーム（発動）", "Vore Worm (Init)"],
    ["ボアワーム（継続）", "Vore Worm (Cont)"],
    ["ポイズンドロウ（女）", "Poison Draw (F)"],
    ["ポイズンドロウ（男）", "Poison Draw (M)"],
    ["マーメイドヘア（女）", "Mermaid Hair (F)"],
    ["マーメイドヘア（男）", "Mermaid Hair (M)"],
    ["マグマスファル（女）", "Magma Bath (F)"],
    ["マグマスファル（男）", "Magma Bath (M)"],
    ["マグマドロウ（女）", "Magma Draw (F)"],
    ["マグマドロウ（男）", "Magma Draw (M)"],
    ["マッドシェイク（女）", "Mud Shake (F)"],
    ["マッドシェイク（男）", "Mud Shake (M)"],
    ["マッドドロウ（女）", "Mud Draw (F)"],
    ["マッドドロウ（男）", "Mud Draw (M)"],
    ["マンティスレイプ（発動）", "Mantis Rape (Init)"],
    ["マンティスレイプ（継続）", "Mantis Rape (Cont)"],
    ["ミイラパッケージ（女）", "Mummy Package (F)"],
    ["ミイラパッケージ（発動）", "Mummy Package (Init)"],
    ["ミイラパッケージ（継続）", "Mummy Package (Cont)"],
    ["ミイラバンデージ（女）", "Mummy Bandage (F)"],
    ["ミイラバンデージ（男）", "Mummy Bandage (M)"],
    ["ミニメルティウォッシュ（女）", "Mini Melty Wash (F)"],
    ["ミニメルティウォッシュ（男）", "Mini Melty Wash (M)"],
    ["ミノタウロスレイプ（発動）", "Minotauros Rape (Init)"],
    ["ミノタウロスレイプ（継続）", "Minotauros Rape (Cont)"],
    ["メルティウォッシュ（女）", "Melty Wash (F)"],
    ["メルティウォッシュ（男）", "Melty Wash (M)"],
    ["メルトシザーズ（女）", "Melt Scissors (F)"],
    ["メルトシザーズ（発動）", "Melt Scissors (Init)"],
    ["メルトシザーズ（継続）", "Melt Scissors (Cont)"],
    ["メロウラフレシア（発動）", "Mellow Rafflesia (Init)"],
    ["メロウラフレシア（継続）", "Mellow Rafflesia (Cont)"],
    ["メロソフィ・エンデ（発動）", "Melo Sophie:End (Init)"],
    ["メロソフィ・エンデ（継続）", "Melo Sophie:End (Cont)"],
    ["やまた・しろろひ（女）", "Eight Head Assault (F)"],
    ["やまた・しろろひ（男）", "Eight Head Assault (M)"],
    ["ラミアの魔膣（発動）", "Lamia's Devilish Vagina (Act.)"],
    ["ラミアの魔膣（継続）", "Lamia's Devilish Vagina (Cont.)"],
    ["リトン搾精器（女）", "Litton Energy Extractor (F)"],
    ["リトン搾精器（男）", "Litton Energy Extractor (M)"],
    ["リボリボハウルン（女）", "Ribo-Ribo Capsule (F)"],
    ["リボリボハウルン（発動）", "Ribo-Ribo Capsule (Init)"],
    ["リボリボハウルン（継続）", "Ribo-Ribo Capsule (Cont)"],
    ["ルーティー・カレス（女）", "Rooty Caress (F)"],
    ["ルーティー・カレス（男）", "Rooty Caress (M)"],
    ["ルーティー・ホールド（女）", "Rooty Hold (F)"],
    ["ルーティー・ホールド（男）", "Rooty Hold (M)"],
    ["ルクスルスロウン（女）", "Luxuru Throne (F)"],
    ["ルクスルスロウン（拘束）", "Luxuru Throne (Bind)"],
    ["ルクスルスロウン（継続）", "Luxuru Throne (Cont)"],
    ["レッドドロウ（女）", "Red Draw (F)"],
    ["レッドドロウ（男）", "Red Draw (M)"],
    ["レティア式触診（女）", "Genital Palpation (F)"],
    ["レティア式触診（男）", "Genital Palpation (M)"],
    ["ロウ固め（女）", "Wax Hardening (F)"],
    ["ロウ固め（男）", "Wax Hardening (M)"],
    ["ワイルドレイプ（発動）", "Wild Rape (Init)"],
    ["ワイルドレイプ（継続）", "Wild Rape (Cont)"],
    ["ワカメ愛撫（女）", "Seaweed Caress (F)"],
    ["ワカメ愛撫（男）", "Seaweed Caress (M)"],
    ["ワカメ蹂躙（女）", "Seaweed Violations (F)"],
    ["ワカメ蹂躙（男）", "Seaweed Violations (M)"],
    ["九十巻舌姦（女）", "Ninety Inch Tongue Wrap (F)"],
    ["九十巻舌姦（発動）", "Ninety Inch Tongue Wrap (Init)"],
    ["九十巻舌姦（継続）", "Ninety Inch Tongue Wrap (Cont)"],
    ["乱れ太夫（発動）", "Courtesan's Pride (Init)"],
    ["乱れ太夫（継続）", "Courtesan's Pride (Cont)"],
    ["人形の手遊び（女）", "Doll's Fingerplay (F)"],
    ["人形の手遊び（男）", "Doll's Fingerplay (M)"],
    ["人魚の洗い手慰（女）", "Mermaid's Cleaning Hand (F)"],
    ["人魚の洗い手慰（男）", "Mermaid's Cleaning Hand (M)"],
    ["体液搾取（女）", "Bodily Fluid Drinking (F)"],
    ["体液搾取（発動）", "Bodily Fluid Drinking (Init)"],
    ["体液搾取（継続）", "Bodily Fluid Drinking (Cont)"],
    ["全身おしゃぶり（女）", "Full-Body Suck (F)"],
    ["全身おしゃぶり（男）", "Full-Body Suck (M)"],
    ["全身吸精（女）", "Full Body Tentacle Drain (F)"],
    ["全身吸精（男）", "Full Body Tentacle Drain (M)"],
    ["全身洗浄（女）", "Full Body Wash (F)"],
    ["全身洗浄（男）", "Full Body Wash (M)"],
    ["全身触手責め（女）", "Full Body Tentacle Torture (F)"],
    ["全身触手責め（男）", "Full Body Tentacle Torture (M)"],
    ["八撫の足（女）", "Eight-Legged Stroke (F)"],
    ["八撫の足（男）", "Eight-Legged Stroke (M)"],
    ["凍てつく陵辱（発動）", "Freezing Rape (Init)"],
    ["凍てつく陵辱（継続）", "Freezing Rape (Cont)"],
    ["動力補給（発動）", "Power Replenishment (Init)"],
    ["動力補給（継続）", "Power Replenishment (Cont)"],
    ["十指手淫（女）", "Ten Finger Masturbation (F)"],
    ["十指手淫（男）", "Ten Finger Masturbation (M)"],
    ["取り込む（発動1）", "Assimilate (Init 1)"],
    ["取り込む（発動2）", "Assimilate (Init 2)"],
    ["口内嫐り（女）", "Oral Fixation (F)"],
    ["口内嫐り（男）", "Oral Fixation (M)"],
    ["同化吸収（女）", "Assimilation Drain (F)"],
    ["同化吸収（継続）", "Assimilation Drain (Cont)"],
    ["吸精（女）", "Fluid Suck (F)"],
    ["吸精（男）", "Fluid Suck (M)"],
    ["吸精ツタ（女）", "Sucking Ivy (F)"],
    ["吸精ツタ（男）", "Sucking Ivy (M)"],
    ["吸精ドレス（女）", "Dress Drain (F)"],
    ["吸精ドレス（男）", "Dress Drain (M)"],
    ["吸精の繭（発動）", "Semen Sucking Cocoon (Init)"],
    ["吸精の繭（継続）", "Semen Sucking Cocoon (Cont)"],
    ["吸精の髪（女）", "Draining Hair (F)"],
    ["吸精の髪（男）", "Draining Hair (M)"],
    ["吸精夜想曲（発動）", "Nocturnal Sucking (Init)"],
    ["吸精夜想曲（継続）", "Nocturnal Sucking (Cont)"],
    ["吸精触手（女）", "Sucking Tentacle (F)"],
    ["吸精触手（男）", "Sucking Tentacle (M)"],
    ["吸血吸精ワーム（女）", "Blood-Sucking Worm (F)"],
    ["吸血吸精ワーム（発動）", "Blood-Sucking Worm (Init)"],
    ["吸血吸精ワーム（継続）", "Blood-Sucking Worm (Cont)"],
    ["嗜虐に嗤う鉄処女（発動）", "Sadistic Iron Maiden (Init)"],
    ["嗜虐に嗤う鉄処女（継続）", "Sadistic Iron Maiden (Cont)"],
    ["土蜘蛛捕獲（女）", "Tsuchigumo Capture (F)"],
    ["土蜘蛛捕獲（男）", "Tsuchigumo Capture (M)"],
    ["地母神の手慰（女）", "Hand of the Earth Mother (F)"],
    ["地母神の手慰（男）", "Hand of the Earth Mother (M)"],
    ["夜夢の蜘蛛（女）", "Dream Spider (F)"],
    ["夜夢の蜘蛛（男）", "Dream Spider (M)"],
    ["夜想搾血（女）", "Nocturnal Bloodsucker (F)"],
    ["夜想搾血（男）", "Nocturnal Bloodsucker (M)"],
    ["大蛇嫐り（女）", "Serpent Teasing (F)"],
    ["大蛇嫐り（男）", "Serpent Teasing (M)"],
    ["大蛇締め（女）", "Serpent Squeeze (F)"],
    ["大蛇締め（男）", "Serpent Squeeze (M)"],
    ["天上のカーマストラ（発動）", "Heavenly Kamasutra (Init)"],
    ["天上のカーマストラ（継続）", "Heavenly Kamasutra (Cont)"],
    ["天境の宴（女）", "Heavenly Feast (F)"],
    ["天境の宴（男）", "Heavenly Feast (M)"],
    ["天魔搾獄（発動）", "Sweet Demonic Prison (Init)"],
    ["天魔搾獄（継続）", "Sweet Demonic Prison (Cont)"],
    ["天魔触撫（女）", "Sweet Demonic Tentacles (F)"],
    ["天魔触撫（男）", "Sweet Demonic Tentacles (M)"],
    ["奉仕手淫（女）", "Service:Hand (F)"],
    ["奉仕手淫（男）", "Service:Hand (M)"],
    ["女王の生殖壺（発動）", "Queen's Stud (Init)"],
    ["女王の生殖壺（継続）", "Queen's Stud (Cont)"],
    ["女王の艶技（女）", "Queen's Unmatched Skill (F)"],
    ["女王の艶技（男）", "Queen's Unmatched Skill (M)"],
    ["女王触手全身責め（女）", "Queen's Tentacle Body Torture (F)"],
    ["女王触手全身責め（男）", "Queen's Tentacle Body Torture (M)"],
    ["女王触手股間責め（女）", "Queen's Tentacle Groin Torture (F)"],
    ["女王触手股間責め（男）", "Queen's Tentacle Groin Torture (M)"],
    ["女皇の吸精（女）", "Empress's Draining (F)"],
    ["女皇の吸精（男）", "Empress's Draining (M)"],
    ["子作りえっち（発動）", "Cat in Heat (Init)"],
    ["子作りえっち（継続）", "Cat in Heat (Cont)"],
    ["子鬼レイプ（発動）", "Oni Rape (Init)"],
    ["子鬼レイプ（継続）", "Oni Rape (Cont)"],
    ["官能の手淫（女）", "Sensual Hands (F)"],
    ["官能の手淫（男）", "Sensual Hands (M)"],
    ["実りなき生殖（発動）", "Fruitless Reproduction (Init)"],
    ["実りなき生殖（継続）", "Fruitless Reproduction (Cont)"],
    ["小鬼の名器（発動）", "Goblin's Signature Vagina (Init)"],
    ["小鬼の名器（継続）", "Goblin's Signature Vagina (Cont)"],
    ["尻尾愛撫（女）", "Tail Caress (F)"],
    ["尻尾愛撫（男）", "Tail Caress (M)"],
    ["尻尾拘束愛撫（女）", "Bound Tail Caress (F)"],
    ["尻尾拘束愛撫（男）", "Bound Tail Caress (M)"],
    ["巨獣蹂躙（発動）", "Giant Beast's Violation (Init)"],
    ["巨獣蹂躙（継続）", "Giant Beast's Violation (Cont)"],
    ["巨腕握り締め（女）", "Giant Arm Sqeeze (F)"],
    ["巨腕握り締め（男）", "Giant Arm Sqeeze (M)"],
    ["巨膣ねじこみ（発動）", "Giant's Vaginal Screw (Init)"],
    ["巨膣ねじこみ（継続）", "Giant's Vaginal Screw (Cont)"],
    ["巻き付く（女）", "Coil (F)"],
    ["巻き付く（男）", "Coil (M)"],
    ["影十字嫐（女）", "Shadow Cross (F)"],
    ["影十字嫐（発動）", "Shadow Cross (Init)"],
    ["影十字嫐（継続）", "Shadow Cross (Cont)"],
    ["影口淫（女）", "Shadow Mouth (F)"],
    ["影口淫（男）", "Shadow Mouth (M)"],
    ["影舞髪淫（女）", "Dancing Shadow Hair (F)"],
    ["影舞髪淫（男）", "Dancing Shadow Hair (M)"],
    ["快楽のつまびき（女）", "Pleasurable Strumming (F)"],
    ["快楽のつまびき（男）", "Pleasurable Strumming (M)"],
    ["快楽の十字架（女）", "Cross of Pleasure (F)"],
    ["快楽の十字架（男）", "Cross of Pleasure (M)"],
    ["快楽の羽根愛撫（女）", "Feather Caress (F)"],
    ["快楽の羽根愛撫（男）", "Feather Caress (M)"],
    ["快楽の風（女）", "Winds of Pleasure (F)"],
    ["快楽の風（男）", "Winds of Pleasure (M)"],
    ["快楽蜜壺漬け（女）", "Honey Pot Soak (F)"],
    ["快楽蜜壺漬け（発動）", "Honey Pot Soak (Init)"],
    ["快楽蜜壺漬け（継続）", "Honey Pot Soak (Cont)"],
    ["恍惚の指使い（女）", "Fingers of Ecstasy (F)"],
    ["恍惚の指使い（男）", "Fingers of Ecstasy (M)"],
    ["恍惚の泡奉仕（女）", "Bubble Service Trance (F)"],
    ["恍惚の泡奉仕（男）", "Bubble Service Trance (M)"],
    ["恍惚の蠕動（女）", "Ecstasy Rub (F)"],
    ["恍惚の蠕動（男）", "Ecstasy Rub (M)"],
    ["悦びの弦（女）", "Strings of Joy (F)"],
    ["悦びの弦（男）", "Strings of Joy (M)"],
    ["悦びの手（女）", "Hand of Pleasure (F)"],
    ["悦びの手（男）", "Hand of Pleasure (M)"],
    ["悦楽の口淫（女）", "Mouth of Pleasure (F)"],
    ["悦楽の口淫（男）", "Mouth of Pleasure (M)"],
    ["悪夢の抱擁（女）", "Nightmare Embrace (F)"],
    ["悪夢の抱擁（発動）", "Nightmare Embrace (Init)"],
    ["悪夢の抱擁（継続）", "Nightmare Embrace (Cont)"],
    ["悪夢の締め付け（女）", "Nightmare Tightening (F)"],
    ["悪夢の締め付け（男）", "Nightmare Tightening (M)"],
    ["愛天使の淫欲（発動）", "Love Angel's Lust (Init)"],
    ["愛天使の淫欲（継続）", "Love Angel's Lust (Cont)"],
    ["戦乙女の天羽（女）", "War Maiden's Heavenly Wings (F)"],
    ["戦乙女の天羽（男）", "War Maiden's Heavenly Wings (M)"],
    ["戦乙女の嫐壺（発動）", "War Maiden's Vagina (Init)"],
    ["戦乙女の嫐壺（継続）", "War Maiden's Vagina (Cont)"],
    ["手誘・魔蜘蛛", "Hand Skill:Spider (F)"],
    ["拘束舐め回し（女）", "Binding Lick (F)"],
    ["拘束舐め回し（発動）", "Binding Lick (Init)"],
    ["拘束舐め回し（継続）", "Binding Lick (Cont)"],
    ["拘束触手愛撫（女）", "Bound Tentacle Caress (F)"],
    ["拘束触手愛撫（男）", "Bound Tentacle Caress (M)"],
    ["搾精イソギンチャクの蠕動（女）", "Anemone Peristalsis (F)"],
    ["搾精イソギンチャクの蠕動（男）", "Anemone Peristalsis (M)"],
    ["搾精の蠕動（女）", "Draining Peristalsis (F)"],
    ["搾精の蠕動（男）", "Draining Peristalsis (M)"],
    ["搾精球（女）", "Drain Sphere (F)"],
    ["搾精球（発動）", "Drain Sphere (Init)"],
    ["搾精球（継続）", "Drain Sphere (Cont)"],
    ["枷躙（女）", "Strumming Shackles (F)"],
    ["枷躙（発動）", "Strumming Shackles (Init)"],
    ["枷躙（継続）", "Strumming Shackles (Cont)"],
    ["歓喜の口（女）", "Mouth of Joy (F)"],
    ["歓喜の口（男）", "Mouth of Joy (M)"],
    ["毒の粘檻（女）", "Sticky Poison Cage (F)"],
    ["毒の粘檻（発動）", "Sticky Poison Cage (Init)"],
    ["毒の粘檻（継続）", "Sticky Poison Cage (Cont)"],
    ["氷の手撫（女）", "Ice Hand Job (F)"],
    ["氷の手撫（男）", "Ice Hand Job (M)"],
    ["流砂の罠（拘束）", "Quicksand Trap (Bind)"],
    ["流髪（女）", "Icicle Hair (F)"],
    ["流髪（男）", "Icicle Hair (M)"],
    ["淑女の口淫（女）", "Lady's Cunnilingus"],
    ["淑女の口淫（男）", "Lady's Fellatio"],
    ["淑女の手淫（女）", "Lady's Masturbation (F)"],
    ["淑女の手淫（男）", "Lady's Masturbation (M)"],
    ["淫ら髪撫（女）", "Indecent Hair (F)"],
    ["淫ら髪撫（男）", "Indecent Hair (M)"],
    ["淫舌ストローク（女）", "Tongue Stroke (F)"],
    ["淫舌ストローク（男）", "Tongue Stroke (M)"],
    ["淫邪の蛇壺（発動）", "Lewd Snake's Vagina (Init)"],
    ["淫邪の蛇壺（継続）", "Lewd Snake's Vagina (Cont)"],
    ["淫邪の蛇髪（女）", "Lewd Snake's Hair (F)"],
    ["淫邪の蛇髪（男）", "Lewd Snake's Hair (M)"],
    ["渇精の肉壺（発動）", "Thirsting Vagina (Init)"],
    ["渇精の肉壺（継続）", "Thirsting Vagina (Cont)"],
    ["溶解口淫（女）", "Dissolving Cunnilingus"],
    ["溶解口淫（男）", "Dissolving Fellatio"],
    ["溶解手淫（女）", "Dissolving Masturbation"],
    ["溶解手淫（男）", "Dissolving Hand Job"],
    ["濡れ尻尾愛撫（女）", "Wet Tail Caress (F)"],
    ["濡れ尻尾愛撫（男）", "Wet Tail Caress (M)"],
    ["濡れ濡れロール（女）", "Wet Wet Roll (F)"],
    ["濡れ濡れロール（男）", "Wet Wet Roll (M)"],
    ["火神連脚（女）", "Fire Spirit's Foot (F)"],
    ["火神連脚（男）", "Fire Spirit's Foot (M)"],
    ["火龍の抱擁（女）", "Fire Spirit's Embrace (F)"],
    ["火龍の抱擁（男）", "Fire Spirit's Embrace (M)"],
    ["牝木繚乱（発動）", "Bloom of the Tree (Init)"],
    ["牝木繚乱（継続）", "Bloom of the Tree (Cont)"],
    ["牡啜りの魔壺（発動）", "Male-Slurping Vagina (Init)"],
    ["牡啜りの魔壺（継続）", "Male-Slurping Vagina (Cont)"],
    ["甘いおしゃぶり（女）", "Sweet Pacifier (F)"],
    ["甘いおしゃぶり（男）", "Sweet Pacifier (M)"],
    ["甘いお口（女）", "Sweet Mouth (F)"],
    ["甘いお口（男）", "Sweet Mouth (M)"],
    ["男殺しの魔蜘蛛", "Hand Skill:Spider (M)"],
    ["疑似捕食機構（女）", "Pseudo-Predation Mechanism (F)"],
    ["疑似捕食機構（発動）", "Pseudo-Predation Mechanism (Init)"],
    ["疑似捕食機構（継続）", "Pseudo-Predation Mechanism (Cont)"],
    ["百合の墓場（発動）", "Graveyard of Lilies (Init)"],
    ["百合の墓場（継続）", "Graveyard of Lilies (Cont)"],
    ["百足責め（女）", "Centipede Torture (F)"],
    ["百足責め（男）", "Centipede Torture (M)"],
    ["百足蹂躙（女）", "Centipede Violation (F)"],
    ["百足蹂躙（男）", "Centipede Violation (M)"],
    ["真夏の夜の夢（女）", "A Midsummer Night's Dream (F)"],
    ["真夏の夜の夢（男）", "A Midsummer Night's Dream (M)"],
    ["破滅の蛇舌（女）", "Snake Tongue of Ruin (F)"],
    ["破滅の蛇舌（男）", "Snake Tongue of Ruin (M)"],
    ["竜撫（女）", "Dragon Hand (F)"],
    ["竜撫（男）", "Dragon Hand (M)"],
    ["粘糸巻き上げ（女）", "Sticky Thread Wrap (F)"],
    ["粘糸巻き上げ（男）", "Sticky Thread Wrap (M)"],
    ["絡み搾る魔性の髪（女）", "Devilish Draining Hair (F)"],
    ["絡み搾る魔性の髪（男）", "Devilish Draining Hair (M)"],
    ["絶技・筒涸らし（発動）", "Ultimate Skill:Pussy Exhaustion (Init)"],
    ["絶技・筒涸らし（継続）", "Ultimate Skill:Pussy Exhaustion (Cont)"],
    ["締め上げる（女）", "Constrict (F)"],
    ["締め上げる（男）", "Constrict (M)"],
    ["羽くすぐり（女）", "Wing Tickle (F)"],
    ["羽くすぐり（男）", "Wing Tickle (M)"],
    ["聖搾蛭（女）", "Sacred Leech (F)"],
    ["聖搾蛭（男）", "Sacred Leech (M)"],
    ["聖獣の愛撫（女）", "Holy Beast's Caress (F)"],
    ["聖獣の愛撫（男）", "Holy Beast's Caress (M)"],
    ["聖蔦の吸精（女）", "Holy Ivy Suction (F)"],
    ["聖蔦の吸精（男）", "Holy Ivy Suction (M)"],
    ["聖髪の天国（女）", "Holy Hair Heaven (F)"],
    ["聖髪の天国（男）", "Holy Hair Heaven (M)"],
    ["聖髪の昇天（女）", "Holy Hair Ascension (F)"],
    ["聖髪の昇天（男）", "Holy Hair Ascension (M)"],
    ["肉器への誘い（女）", "Flesh Hole Invitation (F)"],
    ["肉器への誘い（男）", "Flesh Hole Invitation (M)"],
    ["股間ベロベロ舐め（女）", "Groin Lick (F)"],
    ["股間ベロベロ舐め（男）", "Groin Lick (M)"],
    ["股間繊毛責め（女）", "Trichome Groin Torture (F)"],
    ["股間繊毛責め（男）", "Trichome Groin Torture (M)"],
    ["股間触手責め（女）", "Groin Tentacle Torture (F)"],
    ["股間触手責め（男）", "Groin Tentacle Torture (M)"],
    ["背徳の三重奏（女）", "Immoral Trio (F)"],
    ["背徳の三重奏（男）", "Immoral Trio (M)"],
    ["背徳の甘噛み（女）", "Corrupted Play-biting (F)"],
    ["背徳の甘噛み（男）", "Corrupted Play-biting (M)"],
    ["膣内窒息（発動）", "Vaginal Asphyxiation (Init)"],
    ["膣内窒息（継続）", "Vaginal Asphyxiation (Cont)"],
    ["至福の手淫（女）", "Blissful Masturbation (F)"],
    ["至福の手淫（男）", "Blissful Masturbation (M)"],
    ["舌巻き締め（女）", "Binding Tongue (F)"],
    ["舌巻き締め（男）", "Binding Tongue (M)"],
    ["舌巻き舐め回し（女）", "Bound Tongue Caress (F)"],
    ["舌巻き舐め回し（男）", "Bound Tongue Caress (M)"],
    ["舐め回し（女）", "Lick (F)"],
    ["舐め回し（男）", "Lick (M)"],
    ["花淫（女）", "Lewd Flowers (F)"],
    ["花淫（男）", "Lewd Flowers (M)"],
    ["苗床の抱擁（女）", "Seedbed Embrace (F)"],
    ["苗床の抱擁（男）", "Seedbed Embrace (M)"],
    ["苗床の抱擁（継続）", "Seedbed Embrace (Cont)"],
    ["蕩髪絶技（女）", "Ultimate Hair Charm (F)"],
    ["蛇夜伽（発動）", "Snake Mating (Init)"],
    ["蛇夜伽（継続）", "Snake Mating (Cont)"],
    ["蛭搾（女）", "Leech Suck (F)"],
    ["蛭搾（男）", "Leech Suck (M)"],
    ["蛸撫（女）", "Octopus Pat (F)"],
    ["蛸撫（男）", "Octopus Pat (M)"],
    ["蟲穴交尾（発動）", "Insect Mating (Init)"],
    ["蟲穴交尾（継続）", "Insect Mating (Cont)"],
    ["触手の戯れ（女）", "Tentacle Play (F)"],
    ["触手の戯れ（男）", "Tentacle Play (M)"],
    ["触手乳首責め（女）", "Tentacle Nipple Torture (F)"],
    ["触手乳首責め（男）", "Tentacle Nipple Torture (M)"],
    ["触手吸引（女）", "Tentacle Suck (F)"],
    ["触手吸引（男）", "Tentacle Suck (M)"],
    ["触手吸精（女）", "Tentacle Drain (F)"],
    ["触手吸精（男）", "Tentacle Drain (M)"],
    ["触手愛撫（女）", "Tentacle Caress (F)"],
    ["触手愛撫（男）", "Tentacle Caress (M)"],
    ["触手拘束（女）", "Tentacle Bind (F)"],
    ["触手拘束（男）", "Tentacle Bind (M)"],
    ["触手股間責め（女）", "Tentacle Groin Torture (F)"],
    ["触手股間責め（男）", "Tentacle Groin Torture (M)"],
    ["誘惑花粉の髪（女）", "Seduction Pollen Hair (F)"],
    ["誘惑花粉の髪（男）", "Seduction Pollen Hair (M)"],
    ["貴婦人の遊技（女）", "Lady's Amusement (F)"],
    ["貴婦人の遊技（男）", "Lady's Amusement (M)"],
    ["軟体にゅるにゅるレイプ（発動）", "Soft Body Rape (Init)"],
    ["軟体にゅるにゅるレイプ（継続）", "Soft Body Rape (Cont)"],
    ["逆レイプするのじゃ！", "I'll Rape You! (Init)"],
    ["逆レイプなのじゃ！", "I'll Rape You! (Cont)"],
    ["陵辱テンタクル（継続）", "Tentacle Rape (Cont)"],
    ["陵辱テンタクル（男）", "Tentacle Rape (M)"],
    ["陶酔のウツボカズラ（女）", "Intoxicating Pitcher (F)"],
    ["陶酔のウツボカズラ（発動）", "Intoxicating Pitcher (Init)"],
    ["陶酔のウツボカズラ（継続）", "Intoxicating Pitcher (Cont)"],
    ["集団締め付け（女）", "Group Tightening (F)"],
    ["集団締め付け（男）", "Group Tightening (M)"],
    ["雪女の抱擁（女）", "Yuki-Onna's Hug (F)"],
    ["雪女の抱擁（男）", "Yuki-Onna's Hug (M)"],
    ["魔犬の姦淫（発動）", "Evil Dog's Adultery (Init)"],
    ["魔犬の姦淫（継続）", "Evil Dog's Adultery (Cont)"],
    ["魔王の夜伽（発動）", "Monster Lord's All-Night Vigil (Init)"],
    ["魔王の夜伽（継続）", "Monster Lord's All-Night Vigil (Cont)"],
    ["魔花の搾檻（女）", "Flower Prison (F)"],
    ["魔花の搾檻（発動）", "Flower Prison (Init)"],
    ["魔花の搾檻（継続）", "Flower Prison (Cont)"],
    ["麻痺おしゃぶり（女）", "Paralysis Pacifier (F)"],
    ["麻痺おしゃぶり（男）", "Paralysis Pacifier (M)"],
  ]

  ILLUSTRATOR_NAME = [
    ["人外モドキ", "Jingai Modoki"],
    ["あかざわRED", "Akazawa RED"],
    ["瀬戸内", "Setouchi"],
    ["健康クロス", "Kenkou Cross"],
    ["アレキシ", "Arekishi"],
    ["海の山", "Uminoyama"],
    ["ネコまんま", "Neko Manma"],
    ["すがもん", "SugaMon"],
    ["いろどり", "Irodori"],
    ["日下部", "kusakabe"],
    ["デルフィナス", "Delphinus"],
    ["鰯の頭", "Iwashi no Atama"],
    ["シルク", "Silk"],
    ["クール教信者", "Cool-Kyou Shinja"],
    ["ステルス改行", "Stealth Changing Line"],
    ["しぃずぴぃ", "Shimpi"],
    ["雪村信二", "Yukimura Shinji"],
    ["磊磊ちかさ", "rai-rai chika sa"],
    ["とーます", "Thomas"],
    ["真喜屋", "makiya"],
    ["しき", "Shiki"],
    ["えぺ", "Epée"],
    ["RPGツクール", "RPG Maker"],
  ]

  #Custom Special Sale Notes
  SPECIAL_SALE = [
    [1, "非売品を素材にしていますが、本当に売却しますか？", "Synthesis result, base item not sold anywhere, confirm sale?"],
    [2, "非売品ですが、本当に売却しますか？", "Item not sold anywhere, confirm sale?"],
    [3, "非売品かつ合成の素材になりますが、本当に売却しますか？", "Synthesis material, not sold anywhere, confirm sale?"],
    [4, "合成の素材になりますが、本当に売却しますか？", "Synthesis material, confirm sale?"],
    [5, "ちぃぱっぱはいらないよ……", "A Chi Pa Pa! I do not need that..."],
  ]

  PLACE = [
    :"Iliasville",
    :"Ilias Temple",
    :"Iliasburg",
    :"Nameless Slums",
    :"Pornof",
    :"Happiness Village",
    :"Harpy Village",
    :"Midas Village",
    :"Enrika",
    :"Rostrum Village",
    :"Iliasport",
    :"Port Natalia",
    :"Luddite Village",
    :"San Ilia",
    :"Underground Library",
    :"Monte Carlo",
    :"Rubiana",
    :"Sabasa",
    :"Grandeur",
    :"Magistea Village",
    :"Lily's Mansion",
    :"Saloon",
    :"Port Marle",
    :"Navy Headquarters",
    :"Finoa",
    :"Grand Noah",
    :"Yamatai Village",
    :"Esta",
    :"Plansect Village",
    :"Lima Village Ruins",
    :"Goddard",
    :"Grangold",
    :"Gold Port",
    :"Succubus Village",
    :"Lady's Village",
    :"Devastated Plains",
    :"Elf Village",
    :"Kitsune Village",
    :"Tanuki Village",
    :"Snow Heaven",
    :"Pocket Castle",
    :"Sealed Castle Mithra",
    :"Tartarus (Ilias Continent South)",
    :"Medal Queen's Castle",
    :"Tartarus (Ilias Continent East)",
    :"Haunted Mansion",
    :"Tartarus (Sabasa Continent North)",
    :"MS Fish",
    :"Magical Academy",
    :"Tartarus (Gold Continent West)",
    :"Ghost Ship",
    :"Snow Shrine",
    :"Hellgondo Shrine",
    :"Sealed Sinner's Prison",
    :"Tartarus (Hellgondo Continent West)",
    :"Missing Number",
    :"Missing Number",
    :"Missing Number",
    :"Missing Number",
    :"Missing Number",
    :"Cave of Trials",
    :"North Ilina Mountains Cave",
    :"South Ilina Mountains Cave",
    :"Pornof Mines",
    :"Harpy Tower",
    :"Slug Tower",
    :"Midas Abandoned Mines",
    :"Rostrum Mountain Caverns",
    :"Cave of Treasures",
    :"Southern Undersea Temple",
    :"Ancient Temple Ruins",
    :"Seeker's Cave",
    :"Pyramid",
    :"Tower of Magic",
    :"Saloon Abandoned Mines",
    :"Great Marle Corridor",
    :"Grand Pirate's Cave",
    :"Administrator's Tower",
    :"Fossil Mines",
    :"Yamatai Cave",
    :"Orochi's Cave",
    :"Labyrinth Caverns",
    :"Minotauros' Labyrinth",
    :"Undine's Spring",
    :"Gold Fort",
    :"Lava Mines",
    :"Gold Volcano",
    :"Vampire Castle",
    :"Northern Undersea Temple",
    :"Forgotten Tower",
    :"The World Tree",
    :"The Secluded Lands",
    :"Puppeteer's Tower",
    :"Snow Cave",
    :"Hall of Creation",
    :"Hellgondo Cave",
    :"Monster Lord's Castle",
    :"Iliasville Mountains",
    :"Talus Hill",
    :"Lost Woods",
    :"Lemuse Beach",
    :"Natalia Coast",
    :"Mount Saint Amos",
    :"Forest of Spirits",
    :"Carlos Hill",
    :"Oasis of Blessings",
    :"Safar Ruins",
    :"Saloon Hill",
    :"Red Mountain",
    :"Lyra Falls",
    :"Eastern Gold Toxic Swamp",
    :"Gold Coast",
    :"March Hills",
    :"Scenic Hill",
    :"Nightmare Wastes",
    :"Fairy's Island",
    :"Mount Horai",
    :"Ilias Temple Ruins",
    :"Succubus Village (Alt)",
    :"Hellgondo Cave (Alt)",
    :"Hellgondo Continent (Alt)",
    :"Monster Lord's Castle (Alt)",
    :"Collab Scenario",
    :"Tartarus (Esta)",
    :"Around Esta (Heaven)",
    :"Around Sabasa (Heaven)",
    :"Around Grand Noah (Heaven)",
    :"Around Grangold (Heaven)",
    :"Hellgondo Continent (Heaven)",
    :"Around San Ilia (Heaven)",
    :"Ilias Continent (Heaven)",
    :"Esta (Heaven)",
    :"Crystal Spring (Heaven)",
    :"Administrator's Tower (Heaven)",
    :"Rubiana (Heaven)",
    :"Safar Ruins (Heaven)",
    :"Sabasa (Heaven)",
    :"Finoa (Heaven)",
    :"Grand Noah (Heaven)",
    :"Grangold (Heaven)",
    :"Goddard (Heaven)",
    :"Gold Volcano (Heaven)",
    :"Sealed Sinner's Prison (Heaven)",
    :"Monster Lord's Castle Ruins (Heaven)",
    :"Remina Research Institute (Heaven)",
    :"Monster Lord's Castle (Angel Occupation)",
    :"Port Natalia (Heaven)",
    :"Luddite Village (Heaven)",
    :"San Ilia (Heaven)",
    :"Forest of Spirits (Heaven)",
    :"Fairy's Island (Heaven)",
    :"Black Mansion",
    :"Allice of Wisdom Institute (Heaven)",
    :"The Moon (Heaven)",
    :"Mount Saint Amos (Heaven)",
    :"Iliasburg (Heaven)",
    :"Sealed Castle Mithra (Heaven)",
    :"Ilias Temple (Heaven)",
    :"Hellgondo Continent (Hell)",
    :"Around Succubus Village (Hell)",
    :"Around Grangold (Hell)",
    :"Around Grand Noah (Hell)",
    :"Around Yamatai (Hell)",
    :"Around San Ilia (Hell)",
    :"Around Sabasa (Hell)",
    :"Ilias Continent (Hell)",
    :"Hell Station (Hell)",
    :"Succubus Village (Hell)",
    :"Gold Port (Hell)",
    :"Grangold (Hell)",
    :"Gold Fort (Hell)",
    :"Around Iliasville",
    :"Around Iliasburg",
    :"Around Nameless Slums",
    :"Around Pornof",
    :"Around Happiness Village",
    :"Around Iliasport",
    :"Eastern Natalia Region",
    :"Western Natalia Region",
    :"Southern Sabasa Desert",
    :"Northern Sabasa Desert",
    :"Around Magistea Village",
    :"Around Port Marle",
    :"Inland Sea",
    :"Around Grand Noah",
    :"Around Yamatai",
    :"Around Esta",
    :"Around Grangold",
    :"Around Succubus Village",
    :"Around Devastated Plains",
    :"Open Sea",
    :"Around Fairy's Island",
    :"Hellgondo Continent",
    :"Around Snow Heaven",
    :"Field (Heaven)",
    :"Field (Makai)",
    :"Various Places",
    :"Around Iliasville (Alt)",
    :"Iliasville (Alt)",
    :"Rostrum Village (Alt)",
    :"Cooking",
    :"Casino Prize (Pornof)",
    :"Casino Prize (Grandeur)",
    :"Casino Prize (Pocket Castle)",
    :"Colosseum Prize",
    :"Battlefuck Prize",
    :"Remina (Alt)",
    :"Around Goddard",
  ]

  # * New Method: SToggle Between Ranks and Percents
  def self.toggle_rank(use_percent = false)
    if !use_percent
      new_valuation = ["S","A","B","C","D","E"]
      new_status_valuation = [
        [130,120,110,100, 90], # MAXHP
        [130,120,110,100, 90], # MAXMP
        [ 30, 20, 10,  0,-20], # MAXSP
        [130,120,110,100, 90], # 攻撃力
        [130,120,110,100, 90], # 防御力
        [130,120,110,100, 90], # 魔法力
        [130,120,110,100, 90], # 魔法防御
        [130,120,110,100, 90], # 敏捷性
        [130,120,110,100, 90], # 運
      ]
    else
      new_valuation = ["500%","495%","490%","485%","480%","475%","470%","465%","460%","455%","450%","445%","440%","435%","430%","425%","420%","415%","410%","405%","400%","395%","390%","385%","380%","375%","370%","365%","360%","355%","350%","345%","340%","335%","330%","325%","320%","315%","310%","305%","300%","295%","290%","285%","280%","275%","270%","265%","260%","255%","250%","245%","240%","235%","230%","225%","220%","215%","210%","205%","200%","195%","190%","185%","180%","175%","170%","165%","160%","155%","150%","145%","140%","135%","130%","125%","120%","115%","110%","105%","100%"," 95%"," 90%"," 85%"," 80%"," 75%"," 70%"," 65%"," 60%"," 55%"," 50%"," 45%"," 40%"," 35%"," 30%"," 25%"," 20%"," 15%"," 10%","  5%"]
      new_status_valuation = [
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # MAXHP
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # MAXMP
        [400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5,  0, -5,-10,-15,-20,-25,-30,-35,-40,-45,-50,-55,-60,-65,-70,-75,-80,-85,-90,-95], # MAXSP
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # 攻撃力
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # 防御力
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # 魔法力
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # 魔法防御
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # 敏捷性
        [500,495,490,485,480,475,470,465,460,455,450,445,440,435,430,425,420,415,410,405,400,395,390,385,380,375,370,365,360,355,350,345,340,335,330,325,320,315,310,305,300,295,290,285,280,275,270,265,260,255,250,245,240,235,230,225,220,215,210,205,200,195,190,185,180,175,170,165,160,155,150,145,140,135,130,125,120,115,110,105,100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10,  5], # 運
      ]
    end

    NWConst::JobChange.send(:remove_const , :VALUATION) if NWConst::Casino.const_defined?(:VALUATION)
    NWConst::JobChange.const_set(:VALUATION, new_valuation)

    NWConst::JobChange.send(:remove_const , :STATUS_VALUATION) if NWConst::Casino.const_defined?(:VALUATION)
    NWConst::JobChange.const_set(:STATUS_VALUATION, new_status_valuation)
  end
end

#==============================================================================
#From Script: ベース/DataObject
#Overwrite lib_category AND lib_name
#This Allows Searching The Above Arrays And Replacing Data
#==============================================================================
#==============================================================================
# ■ RPG::Actor
#==============================================================================
class RPG::Actor < RPG::BaseItem
  #--------------------------------------------------------------------------
  # ● イラストレーター名	
  #--------------------------------------------------------------------------
  def illustrator_name
    if @data_ex.key?(:illustrator_name)
        illustrator_name = @data_ex[:illustrator_name].to_s
        NWConst::Library::ILLUSTRATOR_NAME.each{|jpn_name, tran_name| 
          if jpn_name.force_encoding('UTF-8').encode == illustrator_name
            illustrator_name = tran_name.force_encoding('UTF-8').encode
            break
          end
        }
        return illustrator_name
    else
      return self.name
    end
    return @data_ex.key?(:illustrator_name) ? @data_ex[:illustrator_name] : ""
  end
end

#==============================================================================
# ■ RPG::Enemy
#==============================================================================
class RPG::Enemy < RPG::BaseItem
  #--------------------------------------------------------------------------
  # ● 図鑑種族名
  #--------------------------------------------------------------------------
  def lib_category
    if @data_ex.key?(:lib_category)
        category_name = @data_ex[:lib_category].to_s
        NWConst::Library::RACE_SPECIAL_NAME.each{|jpn_name, tran_name| 
          if jpn_name.force_encoding('UTF-8').encode == category_name
            category_name = tran_name
            break
          end
        }
        return category_name
    else
      return :EMPTY
    end
    #return @data_ex.key?(:lib_category) ? @data_ex[:lib_category] : :EMPTY
  end  
  #--------------------------------------------------------------------------
  # ● 図鑑名称
  #--------------------------------------------------------------------------
  def lib_name
    if @data_ex.key?(:lib_name)
        enemy_name = @data_ex[:lib_name].to_s
        NWConst::Library::ENEMY_SPECIAL_NAME.each{|jpn_name, tran_name| 
          if jpn_name.force_encoding('UTF-8').encode == enemy_name
            enemy_name = tran_name
            break
          end
        }
        return enemy_name
    else
      return self.name
    end
  end
end

#==============================================================================
# ■ RPG::Skill
#==============================================================================
class RPG::Skill < RPG::UsableItem
  #--------------------------------------------------------------------------
  # ● 図鑑名称
  #--------------------------------------------------------------------------
  def lib_name
    if @data_ex.key?(:lib_name)
        skill_name = @data_ex[:lib_name].to_s
        NWConst::Library::SKILL_SPECIAL_NAME.each{|jpn_name, tran_name| 
          if jpn_name.force_encoding('UTF-8').encode == skill_name
            skill_name = tran_name
            break
          end
        }
        return skill_name
    else
      return self.name
    end
  end
end

#==============================================================================
# ■ RPG::BaseItem
#==============================================================================
class RPG::BaseItem
  #--------------------------------------------------------------------------
  # ● Special Sale                    特殊売却
  #--------------------------------------------------------------------------
  def selld_draw
    if @data_ex.key?(:selld_draw)
        sale_text = @data_ex[:selld_draw]
        NWConst::Library::SPECIAL_SALE.each{|index, jap_text, tran_text| 
          if jap_text.force_encoding('UTF-8').encode == sale_text
            sale_text = tran_text
            break
          end
        }
        return sale_text
    else
      return nil
    end
  end
end

class Window_BattleLibrary_MainCommand < Window_Library_MainCommand
  def update_index
    @index_window.set_text("#{INDEX_STRING[:lib_enemy]}(Battle Scene)\r\nPercent Complete:" + sprintf("%3d", collect_per_enemy) + "%")
  end
end

class Window_Library_MainCommand < Window_Command
  def update_index
    case @category
    when 1
      @index_window.set_text("#{INDEX_STRING[:lib_actor]}\r\nPercent Complete:" + sprintf("%3d", collect_per_actor) + "%")
    when 2
      @index_window.set_text("#{INDEX_STRING[:lib_enemy]}\r\nPercent Complete:" + sprintf("%3d", collect_per_enemy) + "%")
    when 3
      @index_window.set_text("#{INDEX_STRING[:lib_weapon]}\r\nPercent Complete:" + sprintf("%3d", collect_per_weapon) + "%")
    when 4
      @index_window.set_text("#{INDEX_STRING[:lib_armor]}\r\nPercent Complete:" + sprintf("%3d", collect_per_armor) + "%")
    when 5
      @index_window.set_text("#{INDEX_STRING[:lib_accessory]}\r\nPercent Complete:" + sprintf("%3d", collect_per_accessory) + "%")
    when 6
      @index_window.set_text("#{INDEX_STRING[:lib_item]}\r\nPercent Complete:" + sprintf("%3d", collect_per_item) + "%")
    when 7
      @index_window.set_text("#{INDEX_STRING[:lib_record]}")
    when 8
      @index_window.set_text("#{INDEX_STRING[:lib_medal]}\r\nAchieved:" + sprintf("%3d", collect_per_medal) + "%")
    else
      @index_window.set_text("#{INDEX_STRING[:lib_top]}\r\nPercent Complete:" + sprintf("%3d", collect_per_all) + "%")
    end
  end
end
module NWConst::Synthesize
  CONFIRM_TEXT = "%s will be synthesized. Proceed?"
  SELECT_BEFORE_ACTOR_TEXT = "Synthesizing %s while equipped.\\nSelect the character equipped it's equipped on."
end
class Window_SynthesizeCommand < Window_HorzCommand
  def make_command_list
    add_command("Synthesize", :synthesize)
    add_command("Leave", :cancel)
  end
end
class Window_SynthesizeStatus < Window_Base
  def draw_before_items(x, y, dummy)
    rect = Rect.new(x, y, contents.width, line_height)
    change_color(system_color)
    draw_text(rect, "Materials Needed")
    rect.y += line_height + 2
    change_color(normal_color)
    @befores.each do |before|
      item = recipe_item(before)
      party_num = $game_party.item_number_include_uniq(item)
      need_num = before[:num]
      draw_item_name(item, rect.x, rect.y, need_num <= party_num)
      draw_text(rect, "#{need_num}/#{party_num}", 2)
      rect.y += line_height
    end
  end
  def draw_params(x, y, index)
    rect = Rect.new(x, y, contents.width, line_height)
    param_names = @item.param_names[index * NWConst::Shop::STATUS_MAX, NWConst::Shop::STATUS_MAX]
    change_color(system_color)
    draw_text(rect, "Basic Stats")
    rect.y += line_height + 2
    change_color(normal_color)
    param_names.each  do |name|
      draw_text(rect, name)
      rect.y += line_height
    end
  end
  def draw_enchants(x, y, index)
    rect = Rect.new(x, y, contents.width, line_height)
    enchant_names = @item.enchant_names[index * NWConst::Shop::STATUS_MAX, NWConst::Shop::STATUS_MAX]
    change_color(system_color)
    draw_text(rect, "Special Effects")
    rect.y += line_height + 2
    change_color(normal_color)
    enchant_names.each do |name|
      draw_text_plus(rect, name)
      rect.y += line_height
    end
  end
end
class Window_Library_RightMain < Window_Selectable
  def draw_record1(rect)
    lr = Rect.new(rect.x, rect.y, Integer(rect.width * 0.65), rect.height)
    rr = Rect.new(rect.x + Integer(rect.width * 0.65), rect.y, Integer(rect.width * 0.35), rect.height)    
    change_color(system_color)
    draw_text(lr, "Playtime:");                        lr.y += rect.height
    draw_text(lr, "Saves:");                        lr.y += rect.height
    draw_text(lr, "Cleared:");                            lr.y += rect.height
    draw_text(lr, "Difficulty:");                      lr.y += rect.height
    draw_text(lr, "Highest Cleared:");                lr.y += rect.height
    draw_text(lr, "Battles:");                          lr.y += rect.height
    draw_text(lr, "Defeats:");                      lr.y += rect.height        
    draw_text(lr, "Companions:");                          lr.y += rect.height
    draw_text(lr, "BF Wins:");        lr.y += rect.height
    draw_text(lr, ex_dungeon_record[0]);                lr.y += rect.height
    draw_text(lr, ex_dungeon_record2[0]);               lr.y += rect.height
    change_color(normal_color)
    draw_text(rr, $game_system.playtime_s);              rr.y += rect.height    
    draw_text(rr, "#{$game_system.save_count}");       rr.y += rect.height    
    draw_text(rr, track);                                rr.y += rect.height    
    draw_text(rr, current_difficulty);                   rr.y += rect.height
    draw_text(rr, clear_difficulty);                     rr.y += rect.height    
    draw_text(rr, "#{$game_system.battle_count}");     rr.y += rect.height    
    draw_text(rr, "#{$game_system.party_lose_count}"); rr.y += rect.height    
    draw_text(rr, "#{party_friendly}");                rr.y += rect.height
    draw_text(rr, battlefucker_defeat);                  rr.y += rect.height
    draw_text(rr, ex_dungeon_record[1]);                 rr.y += rect.height
    draw_text(rr, ex_dungeon_record2[1]); rr.y += rect.height
  end
  def ex_dungeon_record
    var = $game_variables[NWConst::Var::EX_DUNGEON_REACH]
    if var > 0
      return ["Lab. of Chaos Highest Floor:", "#{var}"]
    else
      return ["", ""]
    end
  end
  def ex_dungeon_record2
    var = $game_variables[NWConst::Var::EX_DUNGEON_REACH2]
    if var > 0
      return ["Carnage LoC Highest Floor:", "#{var}"]
    else
      return ["", ""]
    end
  end
  def draw_record2(rect)
		@page_max = 2
		draw_common_page(@page_max)
		lr = Rect.new(rect.x, rect.y, Integer(rect.width * 0.65), rect.height)
		rr = Rect.new(rect.x + Integer(rect.width * 0.65), rect.y, Integer(rect.width * 0.35), rect.height)    
		change_color(system_color)
		case @page
		when 0
			draw_text(lr, "Total Battles:");                              lr.y += rect.height
			draw_text(lr, "Enemies Defeated:");                          lr.y += rect.height
			draw_text(lr, "Defeats (Luka)");                    lr.y += rect.height
			draw_text(lr, "Climaxes (Luka):");                  lr.y += rect.height
			draw_text(lr, "Luka Raped by Companion:");            lr.y += rect.height
			draw_text(lr, "Defeats (All):");                    lr.y += rect.height
			draw_text(lr, "Climaxes (All):");                  lr.y += rect.height
			draw_text(lr, "Escapes from Battle:");                            lr.y += rect.height
			draw_text(lr, "Wiped Out:");                          lr.y += rect.height    
			draw_text(lr, "Max. Damage Dealt:");												lr.y += rect.height
			draw_text(lr, "Max. Damage Received:");												lr.y += rect.height
			draw_text(lr, "BF Wins:");                  lr.y += rect.height
			draw_text(lr, "BF Defeats:");                  lr.y += rect.height
			draw_text(lr, "Gold spent:");                            lr.y += rect.height
			draw_text(lr, "Synthesizes:");                            lr.y += rect.height
			change_color(normal_color)
			draw_text(rr, "#{party_battle}");                      rr.y += rect.height
			draw_text(rr, "#{party_defeat}");                      rr.y += rect.height
			draw_text(rr, "#{actor_down(NWConst::Actor::LUCA)}");  rr.y += rect.height
			draw_text(rr, "#{actor_orgasm(NWConst::Actor::LUCA)}");rr.y += rect.height
			draw_text(rr, "#{friendly_orgasm}");                   rr.y += rect.height
			draw_text(rr, "#{party_down}");                        rr.y += rect.height
			draw_text(rr, "#{party_orgasm}");                      rr.y += rect.height
			draw_text(rr, "#{party_escape}");                      rr.y += rect.height
			draw_text(rr, "#{party_lose}");                        rr.y += rect.height    
			damage_record = party_damage_record_actor >= NWConst::Damage::GIVE_UNIT ? party_damage_record_actor.give_unit_floor(12) : party_damage_record_actor
			draw_text(rr, "#{damage_record}");                      rr.y += rect.height
			damage_record = party_damage_record_enemy >= NWConst::Damage::GIVE_UNIT ? party_damage_record_enemy.give_unit_floor(12) : party_damage_record_enemy
			draw_text(rr, "#{damage_record}");                        rr.y += rect.height
			draw_text(rr, "#{battlefuck_win}");                    rr.y += rect.height
			draw_text(rr, "#{battlefuck_lose}");                   rr.y += rect.height
			draw_text(rr, "#{purchase_gold}#{Vocab.currency_unit}"); rr.y += rect.height
			draw_text(rr, "#{party_synthesize}");                  rr.y += rect.height
		when 1
			draw_text(lr, "Total Job Changes:");                          lr.y += rect.height
			draw_text(lr, "Total Race Changes:");                          lr.y += rect.height
			change_color(normal_color)  
			draw_text(rr, "#{party_class_change}");                rr.y += rect.height
			draw_text(rr, "#{party_tribe_change}");                rr.y += rect.height
		end	
  end
  def track
    num = $game_variables[NWConst::Var::TRACK]
    return 0 < num ? "#{num}" : "None"
  end
  def convert_difficulty(key)
    key = [[-3, key].max, 4].min
    {
      -3 => "None",
      -2 => "VERY EASY",
      -1 => "EASY",
      0  => "NORMAL",
      1  => "HARD",
      2  => "VERY HARD",
      3  => "HELL",
      4  => "PARADOX",
    }[key]
  end
  def battlefucker_defeat
    "#{$game_variables[NWConst::Var::BATTLEFUCKER_DEFEAT]}"
  end
  def draw_contents_medal
    @page = 0
    @page_max = 1
    id = @ext % 80000
    rect = standard_rect
    reset_font_settings
    change_color(normal_color)
    draw_icon(medal_icon_id(id), rect.x, rect.y)
    rect.x     += 24
    rect.width -= 24
    draw_text(rect,medal_title(id))
    rect = standard_rect
    rect.y += rect.height + LINE_HEIGHT
    change_color(system_color)
    draw_text(rect,"Achieved on:")
    rect.y += rect.height
    change_color(normal_color)
    draw_text(rect,medal_gain_time(id))
    rect.y += rect.height + LINE_HEIGHT
    change_color(system_color)
    draw_text(rect,"Details:")
    rect.y += rect.height
    change_color(normal_color)
    draw_text_auto_line(rect, medal_description(id))
  end
  def draw_actor_stat(y, actor)
    lr = half_left_rect(y)
    rr = half_right_rect(y)
    reset_font_settings
    unless actor.id == NWConst::Actor::LUCA
      draw_common_friend(lr,rr,actor)
      lr.y += lr.height
      rr.y += rr.height
    end
    txt = "Battles:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{$game_library.actor_battle(actor.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height    
    txt = "Enemies Defeated:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{actor_defeat(actor.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    txt = "Enemies Raped:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{actor_carry(actor.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    txt = "Times Defeated:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{actor_down(actor.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    txt = "Times Came:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{actor_orgasm(actor.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    txt = "Items Stolen:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{$game_library.actor_steal(actor.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    return rr.y + LINE_HEIGHT
  end
  def draw_actor_illustrator(actor)
    reset_font_settings
    contents.font.size = 20
    rect = Rect.new(0, contents.height - 20, contents.width, 20)
    draw_text(rect, "Artist: #{actor.illustrator_name}", 2)
  end
  def draw_weapon_basic(y, weapon)
    rect = half_left_rect(y)
    change_color(system_color)
    txt = "Type:"
    draw_text(rect, txt)
    reset_font_settings
    w = text_size(txt).width
    rect.x += w
    rect.width -= w
    txt = $data_system.weapon_types[weapon.wtype_id]
    draw_text(rect, txt, 2)
    rect = half_right_rect(rect.y)
    txt = "Cost:"
    w = text_size(txt).width
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    self.draw_currency_value(weapon.price, Vocab.currency_unit, rect.x + w, rect.y, rect.width - w)
    rect.y += rect.height + LINE_HEIGHT
    rect.y = draw_equips_common(rect.y, weapon)
  end
  def draw_armor_basic(y, armor)
    rect = standard_rect
    rect.y = draw_items_common(armor)
    rect = half_left_rect(rect.y)
    txt = "Type:"
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    w = text_size(txt).width
    rect.x += w
    rect.width -= w
    txt = $data_system.armor_types[armor.atype_id]
    draw_text(rect, txt, 2)
    rect = half_right_rect(rect.y)
    txt = "Cost:"
    w = text_size(txt).width
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    self.draw_currency_value(armor.price, Vocab.currency_unit, rect.x + w, rect.y, rect.width - w)
    rect.y += rect.height + LINE_HEIGHT
    rect = half_left_rect(rect.y)
    txt = "Slot:"
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    txt = Vocab.etype(armor.etype_id)
    draw_text(rect, txt, 2)
    rect.y += rect.height + LINE_HEIGHT
    rect.y = draw_equips_common(rect.y, armor)
  end
  def draw_accessory_basic(y, accessory)
    rect = standard_rect
    rect.y = draw_items_common(accessory)
    rect = half_left_rect(rect.y)
    txt = "Type:"
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    txt = $data_system.armor_types[accessory.atype_id]
    draw_text(rect, txt, 2)
    rect = half_right_rect(rect.y)
    txt = "Cost:"
    w = text_size(txt).width
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    self.draw_currency_value(accessory.price, Vocab.currency_unit, rect.x + w, rect.y, rect.width - w)
    rect.y += rect.height + LINE_HEIGHT
    rect = half_left_rect(rect.y)
    txt = "Slot:"
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    txt = Vocab.etype(accessory.etype_id)
    draw_text(rect, txt, 2)
    rect.y += rect.height + LINE_HEIGHT
    rect.y = draw_equips_common(rect.y, accessory)
  end
  def draw_item_basic(y, item)
    rect = standard_rect
    rect.y = draw_items_common(item)
    rect = half_left_rect(rect.y)
    txt = "Cost:"
    w = text_size(txt).width
    change_color(system_color)
    draw_text(rect, txt)
    reset_font_settings
    self.draw_currency_value(item.price, Vocab.currency_unit, rect.x + w, rect.y, rect.width - w)
    rect.y += rect.height + LINE_HEIGHT
  end
  def draw_common_friend(lr, rr, item)
    if item.is_a?(RPG::Actor)
      text = "Affection:"
      friend = $game_variables[NWConst::Var::ACTOR_REL_BASE + item.id]
    else
      text = "Affinity:"
      friend = $game_variables[NWConst::Var::ENEMY_REL_BASE + item.id]
    end
    reset_font_settings
    change_color(system_color)
    draw_text(lr, text)
    change_color(normal_color)
    draw_text(rr, friend)
  end
  ENEMY_SKILL_PAGE_MAX = 14
  def draw_enemy_skill(y, enemy, page = 0)
    lr = standard_rect(y)
    lr.width *= 0.7
    rr = standard_rect(y)
    rr.x += rr.width * 0.7
    rr.width *= 0.3

    reset_font_settings
    change_color(system_color)
    draw_text(lr, "Skill")
    draw_text(rr, "Times Taken")
    lr.y += lr.height + LINE_HEIGHT
    rr.y += rr.height + LINE_HEIGHT
    start_index = ENEMY_SKILL_PAGE_MAX * page

    enemy_show_skills(enemy)[start_index, ENEMY_SKILL_PAGE_MAX].each do |skill|
      name = skill.lib_name
      num  = $game_library.enemy_skill_used_num(enemy.id, skill.id)
      change_color(normal_color, 0 < num)
      draw_text(lr, name)
      draw_text(rr, num)
      lr.y += lr.height
      rr.y += rr.height
    end
    rr.y
  end

  def enemy_show_skills(enemy)
    enemy.actions.map(&:skill).compact.reject do |skill|
      skill.lib_name.empty? || skill.lib_exclude?
    end
  end

  def enemy_skill_page(enemy)
    ([enemy_show_skills(enemy).count - 1, 0].max / ENEMY_SKILL_PAGE_MAX) + 1
  end
  def steal_item_list_index
    {
      1 => "Stolen Items",
      2 => "Stolen Food",
      3 => "Stolen Materials",
      4 => "Stolen Panties"
    }
  end  
   #--------------------------------------------------------------------------
  # ● 敵キャラ統計描画
  #--------------------------------------------------------------------------
  def draw_enemy_stat(y, enemy)
    rect = standard_rect(y)
    lr = half_left_rect(rect.y)
    rr = half_right_rect(rect.y)
    join_flag = false
    join_exist = false
    if enemy.follower?
      join_flag = true
      join_exist = $game_party.follow?(enemy.follower_actor_id)
    elsif enemy.join_switch
      join_flag = true
      join_actor_id = enemy.join_switch - NWConst::Sw::ADD_ACTOR_BASE
      join_exist = $game_party.follow?(join_actor_id)
    end
    if join_flag
      txt = "Recruited: #{ join_exist ? "Yes" : "No" }"
      change_color(normal_color)
      draw_text(lr, txt)
      lr.y += lr.height + LINE_HEIGHT
      rr.y += rr.height + LINE_HEIGHT
    end
    txt = "Race:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = enemy.lib_category.to_s
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    draw_common_friend(lr, rr, enemy)
    lr.y += lr.height
    rr.y += rr.height    
    txt = "Times Defeated:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{enemy_down(enemy.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    txt = "Times Came:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{enemy_orgasm(enemy.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height
    rr.y += rr.height
    txt = "Times Raped:"
    change_color(system_color)
    draw_text(lr, txt)
    txt = "#{enemy_victory(enemy.id).to_i}"
    change_color(normal_color)
    draw_text(rr, txt)
    lr.y += lr.height + LINE_HEIGHT
    rr.y += rr.height + LINE_HEIGHT
    y = draw_encounter_enemy_place(lr.y, enemy)
    return y
  end
end

class Foo::PTEdit::Window_ActorStatus < Window_Base
  def draw_actor_status(actor, x)
    draw_actor_face(actor, x, 0)

    # 残りWIDTH領域132
    rect1 = Rect.new(x + 96, 0, 84, line_height)
    rect2 = Rect.new(rect1.x + rect1.width, rect1.y, 48, rect1.height)

    change_color(normal_color)
    draw_text(rect1, actor.name) # 名前だけ少し大きく
    temp_font_size = contents.font.size
    contents.font.size = 22
    draw_actor_level(actor, rect2.x, rect2.y, :base)

    rect1.y += line_height + 4
    rect2.y += line_height + 4
    change_color(tp_gauge_color2)
    draw_text(rect1, actor.class.name)
    draw_actor_level(actor, rect2.x, rect2.y, :class)

    rect1.y += line_height
    rect2.y += line_height
    change_color(mp_gauge_color2)
    draw_text(rect1, actor.tribe.name)
    draw_actor_level(actor, rect2.x, rect2.y, :tribe)

    rect1.y += line_height
    rect2.y += line_height
    change_color(normal_color)
    draw_text(rect1, "Affection")
    draw_text(rect2, actor.luca? ? "-----" : "#{actor.love}")

    contents.font.size = temp_font_size
  end
end

module NWConst::JobChange 
  CLASS_TYPE = [
    ["Basic Jobs", "Intermediate Jobs", "Advanced Jobs", "Sealed Jobs", "Forbidden Jobs"],
    ["Basic Races", "Intermediate Races", "Advanced Races", "Sealed Races"]
  ]

  UNKNOWN_NEED_ITEM      = "Job change item "
  UNKNOWN_NEED_CLASS     = "The jobs below must all be mastered first."
  UNKNOWN_SELECT_CLASS   = "Any of the jobs below must be mastered first."
  UNKNOWN_DIFFERENT_KIND = "Cannot select this race."
  CLASS_CHANGE_RESULT  = ["%s's job is now %s!", "%s's race is now %s!"]
end

module NWConst::Status
  ROOT_COMMAND          = ["Basic Info","Equipment","Jobs and Races","Trait"]
  BASIC_COMMAND         = ["Stats", "Adv. Stats", "Damage Mult.", "State Chance", "Job", "Race", "Affection"]
  CLASS_HISTORY_COMMAND = ["Jobs", "Races"]
  BASIC_PARAM_VOCAB     = ["Max HP", "Max MP", "Attack", "Defense", "Magic", "Willpower", "Agility", "Dexterity",
                           "Hit Rate", "Critical Rate", "Evasion Rate", "Magic Evasion Rate", "Counter Rate", "Magic Counter Rate"]
  SPECIAL_PARAM_VOCAB   = ["HP Recovery Rate", "MP Recovery Rate", "SP Recovery Rate", "SP Charge Rate", "MP Consumption", "SP Consumption",
                           "Recovery Effectiveness", "Item Effectiveness", "Steal Success Rate", "Magic Reflect",
                           "XP Rate", "Job XP Rate", "Gold Bonus", "Item Drop Rate"]
end

module ShowKey_Help
  class << self
    #--------------------------------------------------------------------------
    # ● 各画面
    #--------------------------------------------------------------------------
    def lr_scroll
      "#{Vocab.key_l}/#{Vocab.key_r}:Page Scroll"
    end

    #--------------------------------------------------------------------------
    # ● アイテムを表示する画面
    #--------------------------------------------------------------------------
    def favorite_item(state)
      "#{Vocab.key_y}:#{state ? "Remove" : "Save"} favorite"
    end

    #--------------------------------------------------------------------------
    # ● アイテムを表示する画面
    #--------------------------------------------------------------------------
    def favorite_mode(mode)
      "#{Vocab.key_z}:#{mode ? "Display all but" : "Display only"} favorites"
    end

    #--------------------------------------------------------------------------
    # ● 装備を表示する画面
    #--------------------------------------------------------------------------
    def equip_info
      "#{Vocab.key_x}:Equip Info"
    end

    #--------------------------------------------------------------------------
    # ● キャラごとの画面
    #--------------------------------------------------------------------------
    def lr_actor
      "#{Vocab.key_l}/#{Vocab.key_r}:Switch Character"
    end

    #--------------------------------------------------------------------------
    # ● スキル画面
    #--------------------------------------------------------------------------
    def stype_disable
      "#{Vocab.key_a}:Hide in Battle + Disable in Auto Mode"
    end

    #--------------------------------------------------------------------------
    # ● スキル画面
    #--------------------------------------------------------------------------
    def stype_move
      "#{Vocab.key_x}+↑↓:Sort".force_encoding('UTF-8').encode
    end

    #--------------------------------------------------------------------------
    # ● 装備画面
    #--------------------------------------------------------------------------
    def equip_shift
      "#{Vocab.key_y}:Remove Equip"
    end

    def equip_stone
      "#{Vocab.key_a}:Switch Gem"
    end
    #--------------------------------------------------------------------------
    # ● アビリティ画面
    #--------------------------------------------------------------------------
    def ability_shift_all
      "#{Vocab.key_a}:Remove all Abilities"
    end

    #--------------------------------------------------------------------------
    # ● アビリティ画面
    #--------------------------------------------------------------------------
    def ability_shift
      "#{Vocab.key_a}:Remove Ability"
    end

    #--------------------------------------------------------------------------
    # ● ショップ画面の装備品情報変更テキスト
    #--------------------------------------------------------------------------
    def shop_equip_change
      Help.shop_equip_change
    end

    #--------------------------------------------------------------------------
    # ● ショップ画面の装備品性能比較テキスト
    #--------------------------------------------------------------------------
    def shop_param_compare
      Help.shop_param_compare
    end

    #--------------------------------------------------------------------------
    # ● 秘石画面
    #--------------------------------------------------------------------------
    def stone_shift_all
      "#{Vocab.key_a}:Remove All Gems"
    end

    #--------------------------------------------------------------------------
    # ●  秘石画面
    #--------------------------------------------------------------------------
    def stone_shift
      "#{Vocab.key_a}:Remove Gem"
    end

    def stone_change_scene
      "←/→：Gem Equip Screen".force_encoding('UTF-8').encode
    end
  end
end

module NWConst::Warp
  AREA_SELECT_TEXT  = "Please select a region."
  PLACE_SELECT_TEXT = "Please select a location."
  CONFIRM_TEXT      = "Warping to %s. Is that okay?"
end

class << DataManager
  def convert_jpn_placename(place_name)
    place_names = [["？？？", "? ? ?"],
    ["イリアスヴィル", "Iliasville"],
    ["イリアス神殿", "Ilias Temple"],
    ["イリアスベルク", "Iliasburg"],
    ["名も無きスラム", "Nameless Slums"],
    ["ポルノフ", "Pornof"],
    ["ハピネス村", "Happiness Village"],
    ["ハーピーの集落", "Harpy Village"],
    ["ミダス村", "Midas Village"],
    ["エンリカ", "Enrika"],
    ["ロストルム村", "Rostrum Village"],
    ["イリアスポート", "Iliasport"],
    ["ナタリアポート", "Port Natalia"],
    ["ラダイト村", "Luddite Village"],
    ["サン・イリア", "San Ilia"],
    ["地下図書館", "Underground Library"],
    ["モンテカルロ", "Monte Carlo"],
    ["ルビアナ", "Rubiana"],
    ["サバサ", "Sabasa"],
    ["グランドール", "Grandeur"],
    ["マギステア村", "Magistea Village"],
    ["リリィの館", "Lily's Mansion"],
    ["サルーン", "Saloon"],
    ["マールポート", "Port Marle"],
    ["海軍本部", "Navy Headquarters"],
    ["フィノア", "Finoa"],
    ["グランドノア", "Grand Noah"],
    ["ヤマタイ村", "Yamatai Village"],
    ["エスタ", "Esta"],
    ["プランセクト村", "Plansect Village"],
    ["リマ村廃墟", "Lima Village Ruins"],
    ["ゴッダール", "Goddard"],
    ["グランゴルド", "Grangold"],
    ["ゴルドポート", "Gold Port"],
    ["サキュバスの村", "Succubus Village"],
    ["貴婦人の村", "Lady's Village"],
    ["滅びの地", "Devastated Plains"],
    ["エルフの里", "Elf Village"],
    ["きつねの里", "Kitsune Village"],
    ["たぬきの里", "Tanuki Village"],
    ["スノウヘブン", "Snow Heaven"],
    ["ポケット魔王城", "Pocket Castle"],
    ["ミトラ封印城", "Sealed Castle Mithra"],
    ["タルタロス（イリアス大陸南）", "Tartarus (Ilias Continent South)"],
    ["メダル女王の城", "Medal Queen's Castle"],
    ["タルタロス（イリアス大陸東）", "Tartarus (Ilias Continent East)"],
    ["北のお化け屋敷", "Haunted Mansion"],
    ["タルタロス（サバサ大陸北）", "Tartarus (Sabasa Continent North)"],
    ["おさかな号", "MS Fish"],
    ["魔導学園", "Magic Academy"],
    ["タルタロス（ゴルド大陸西）", "Tartarus (Gold Continent West)"],
    ["幽霊船", "Ghost Ship"],
    ["雪のほこら", "Snow Shrine"],
    ["ヘルゴンドのほこら", "Hellgondo Shrine"],
    ["罪人の封牢", "Sealed Sinner's Prison"],
    ["タルタロス（ヘルゴンド大陸西）", "Tartarus (Hellgondo Continent West)"],
    ["ドレインラボ", "Drain Lab"],
    ["バイオラボ", "Biolab"],
    ["レミナ研究所", "Remina Laboratory"],
    ["タルタロス（ゴルド大陸中央）", "Tartarus (Gold Continent Center)"],
    ["カオスフィールド", "Chaos Field"],
    ["試しの洞窟", "Cave of Trials"],
    ["イリナ山地北の洞窟", "North Ilina Mountains Cave"],
    ["イリナ山地南の洞窟", "South Ilina Mountains Cave"],
    ["ポルノフ鉱山", "Pornof Mines"],
    ["ハーピーの塔", "Harpy Tower"],
    ["ナメクジタワー", "Slug Tower"],
    ["ミダス廃鉱", "Midas Abandoned Mines"],
    ["ロストルム山地洞窟", "Rostrum Mountain Caverns"],
    ["秘宝の洞窟", "Cave of Treasures"],
    ["南の海底神殿", "Southern Undersea Temple"],
    ["古代神殿跡", "Ancient Temple Ruins"],
    ["求道者の洞窟", "Seeker's Cave"],
    ["ピラミッド", "Pyramid"],
    ["魔導の塔", "Tower of Magic"],
    ["サルーン廃鉱", "Saloon Abandoned Mine"],
    ["マール大回廊", "Great Marle Corridor"],
    ["大海賊の洞窟", "Grand Pirate's Cave"],
    ["管理者の塔", "Administrator's Tower"],
    ["化石鉱山", "Fossil Mines"],
    ["ヤマタイへの洞窟", "Yamatai Cave"],
    ["オロチの洞", "Orochi's Cave"],
    ["迷宮への洞窟", "Labyrinth Caverns"],
    ["ミノタウロスの迷宮", "Minotauros' Labyrinth"],
    ["ウンディーネの泉", "Undine's Spring"],
    ["ゴルド砦", "Gold Fort"],
    ["溶岩鉱窟", "Lava Mines"],
    ["ゴルド火山洞窟", "Gold Volcano"],
    ["吸血鬼の魔城", "Vampire Castle"],
    ["北の海底神殿", "Northern Undersea Temple"],
    ["忘れられし塔", "Forgotten Tower"],
    ["世界樹", "The World Tree"],
    ["孤独の地", "The Secluded Lands"],
    ["人形遣いの塔", "Puppeteer's Tower"],
    ["雪越えの洞窟", "Snow Cave"],
    ["創世の館", "Hall of Creation"],
    ["ヘルゴンドの洞窟", "Hellgondo Cave"],
    ["魔王城", "Monster Lord's Castle"],
    ["イリアスヴィルの裏山", "Iliasville Mountains"],
    ["タラスの丘", "Talus Hill"],
    ["迷いの森", "Lost Woods"],
    ["レムズ海岸", "Lemuse Beach"],
    ["ナタリア海岸", "Natalia Coast"],
    ["聖山アモス", "Mount Saint Amos"],
    ["精霊の森", "Forest of Spirits"],
    ["カルロスの丘", "Carlos Hill"],
    ["恵みのオアシス", "Oasis of Blessings"],
    ["サファル砂漠遺跡", "Safar Ruins"],
    ["サルーンの丘", "Saloon Hill"],
    ["レド山", "Red Mountain"],
    ["ライラの大滝", "Lyra Falls"],
    ["ゴルド東の大毒沼", "East Gold Toxic Swamp"],
    ["ゴルド海岸", "Gold Beach"],
    ["進軍の丘", "March Hills"],
    ["風光明媚の丘", "Scenic Hill"],
    ["悪夢の荒野", "Nightmare Wastes"],
    ["妖精の島", "Fairy's Island"],
    ["蓬莱山", "Mount Horai"],
    ["イリアス神殿跡", "Ilias Temple Ruins"],
    ["サキュバスの村（異世界）", "Succubus Village (Alt)"],
    ["ヘルゴンドの洞窟（異世界）", "Hellgondo Cave (Alt)"],
    ["ヘルゴンド大陸（異世界）", "Hellgondo Continent (Alt)"],
    ["魔王城（異世界）", "Monster Lord's Castle (Alt)"],
    ["コラボシナリオ", "Collab Scenario"],
    ["タルタロス（エスタ）", "Tartarus (Esta)"],
    ["エスタ周辺（天界）", "Around Esta (Heaven)"],
    ["サバサ周辺（天界）", "Around Sabasa (Heaven)"],
    ["グランドノア周辺（天界）", "Around Grand Noah (Heaven)"],
    ["グランゴルド周辺（天界）", "Around Grangold (Heaven)"],
    ["ヘルゴンド大陸（天界）", "Hellgondo Continent (Heaven)"],
    ["サン・イリア周辺（天界）", "Around San Ilia (Heaven)"],
    ["イリアス大陸（天界）", "Ilias Continent (Heaven)"],
    ["エスタ（天界）", "Esta (Heaven)"],
    ["水精の泉（天界）", "Crystal Spring (Heaven)"],
    ["管理者の塔（天界）", "Administrator's Tower (Heaven)"],
    ["ルビアナ（天界）", "Rubiana (Heaven)"],
    ["サファル砂漠遺跡（天界）", "Safar Ruins (Heaven)"],
    ["サバサ（天界）", "Sabasa (Heaven)"],
    ["フィノア（天界）", "Finoa (Heaven)"],
    ["グランドノア（天界）", "Grand Noah (Heaven)"],
    ["グランゴルド（天界）", "Grangold (Heaven)"],
    ["ゴッダール（天界）", "Goddard (Heaven)"],
    ["ゴルド火山洞窟（天界）", "Gold Volcano (Heaven)"],
    ["罪人の封牢（天界）", "Sealed Sinner's Prison (Heaven)"],
    ["魔王城跡（天界）", "Monster Lord's Castle Ruins (Heaven)"],
    ["レミナ研究所（天界）", "Remina Research Institute (Heaven)"],
    ["魔王城（天使占領）", "Monster Lord's Castle (Angel Occupation)"],
    ["ナタリアポート（天界）", "Port Natalia (Heaven)"],
    ["ラダイト村（天界）", "Luddite Village (Heaven)"],
    ["サン・イリア（天界）", "San Ilia (Heaven)"],
    ["精霊の森（天界）", "Forest of Spirits (Heaven)"],
    ["妖精の島（天界）", "Fairy's Island (Heaven)"],
    ["黒の屋敷", "Black Mansion"],
    ["智の同盟研究所（天界）", "Alliance of Wisdom Institute (Heaven)"],
    ["月（天界）", "The Moon (Heaven)"],
    ["聖山アモス（天界）", "Mount Saint Amos (Heaven)"],
    ["イリアスベルク（天界）", "Iliasburg (Heaven)"],
    ["ミトラ封印城（天界）", "Sealed Castle Mithra (Heaven)"],
    ["イリアス神殿（天界）", "Ilias Temple (Heaven)"],
    ["ヘルゴンド大陸（魔界）", "Hellgondo Continent (Hell)"],
    ["サキュバスの村周辺（魔界）", "Around Succubus Village (Hell)"],
    ["グランゴルド周辺（魔界）", "Around Grangold (Hell)"],
    ["グランドノア周辺（魔界）", "Around Grand Noah (Hell)"],
    ["ヤマタイ周辺（魔界）", "Around Yamatai (Hell)"],
    ["サン・イリア周辺（魔界）", "Around San Ilia (Hell)"],
    ["サバサ周辺（魔界）", "Around Sabasa (Hell)"],
    ["イリアス大陸（魔界）", "Ilias Continent (Hell)"],
    ["魔界駅（魔界）", "Hell Station (Hell)"],
    ["サキュバスの村（魔界）", "Succubus Village (Hell)"],
    ["ゴルドポート（魔界）", "Gold Port (Hell)"],
    ["グランゴルド（魔界）", "Grangold (Hell)"],
    ["ゴルド砦（魔界）", "Gold Fort (Hell)"],
    ["イリアスヴィル周辺", "Around Iliasville"],
    ["イリアスベルク周辺", "Around Iliasburg"],
    ["名も無きスラム周辺", "Around Nameless Slums"],
    ["ポルノフ周辺", "Around Pornof"],
    ["ハピネス村周辺", "Around Happiness Village"],
    ["イリアスポート周辺", "Around Iliasport"],
    ["ナタリア地域東部", "Eastern Natalia Region"],
    ["ナタリア地域西部", "Western Natalia Region"],
    ["サバサ砂漠南部", "Southern Sabasa Desert"],
    ["サバサ砂漠北部", "Northern Sabasa Desert"],
    ["マギステア村周辺", "Around Magistea Village"],
    ["マールポート周辺", "Around Port Marle"],
    ["内海", "Inland Sea"],
    ["グランドノア周辺", "Around Grand Noah"],
    ["ヤマタイ周辺", "Around Yamatai"],
    ["エスタ周辺", "Around Esta"],
    ["グランゴルド周辺", "Around Grangold"],
    ["サキュバスの村周辺", "Around Succubus Village"],
    ["滅びの地周辺", "Around Devastated Plains"],
    ["外海", "Open Sea"],
    ["妖精の島周辺", "Around Fairy's Island"],
    ["ヘルゴンド大陸", "Hellgondo Continent"],
    ["スノウヘブン周辺", "Around Snow Heaven"],
    ["フィールド（天界）", "Field (Heaven)"],
    ["フィールド（魔界）", "Field (Hell)"],
    ["各地", "Various Places"],
    ["イリアスヴィル周辺（異世界）", "Around Iliasville (Alt)"],
    ["イリアスヴィル（異世界）", "Iliasville (Alt)"],
    ["ロストルム村（異世界）", "Rostrum Village (Alt)"],
    ["料理", "Cooking"],
    ["カジノ景品（ポルノフ）", "Casino Prize (Pornof)"],
    ["カジノ景品（グランドール）", "Casino Prize (Grandeur)"],
    ["カジノ景品（ポケット魔王城）", "Casino Prize (Pocket Castle)"],
    ["コロシアム景品", "Colosseum Prize"],
    ["バトルファック景品", "Battlefuck Prize"],
    ["レミナ（異世界）", "Remina (Alt)"],
    ["ゴッダール周辺", "Around Goddard"],
    ["コラボシナリオ", "Collab Scenario"],
    ["管理者の塔（異世界）", "Administrator's Tower (Alt)"],
    ["神鳥のほこら", "Holy Wings Shrine"],
    ["欠番", "Missing Number"]]
    place_names.each do |name|
      return name[1] if name[0].force_encoding('UTF-8').encode == place_name
    end
    return place_name
  end
end

module NWConst::PTEdit
  CONFIRM_TEXT = "Teleport to %s?"
end

class Foo::PTEdit::Window_PartyMember < Window_Command
  def no_actor_name
    "【Empty】".force_encoding('UTF-8').encode
  end
end

module NWConst::Present
  RESULT_STR_UP = "%s's affection increased by %d points! (Affection:%d)"
  RESULT_STR_DOWN =  "%s's affection decreased by %d points... (Affection:%d)"
  RESULT_STR_INVARIANT = "%s's affection didn't change... (Affection:%d)"
end

module BattleManager
  class << self
    def change_novel_scene
      unless memory_battle?
        SceneManager.clear
        SceneManager.push(Scene_Map)
      end
      $game_novel.setup($game_troop.lose_event_id)
      SceneManager.goto(Scene_Novel)

      skip_flag = $game_system.conf[:ls_skip] == 1
      skip_flag &&= $game_library.lose_event_view?($game_novel.event_id)
      check_flag = $game_system.conf[:ls_skip] == 2
      choice = -1
      if no_lose_skip?
        skip_flag  = false
        check_flag = false
      end
      if check_flag
        $game_message.add("Skip defeat scene?")
        ["Yes", "No"].each { |s| $game_message.choices.push(s) }
        $game_message.choice_cancel_type = 2
        $game_message.choice_proc = proc { |n| choice = n }
        wait_for_message
      end
      if no_lose_skip? and memory_battle?
        $game_novel.interpreter.memory_interruption
      elsif skip_flag || (choice == 0)
        $game_novel.interpreter.goto_ilias
      end
    end
    def process_follow_choice(follower_name = nil)
      e = $game_troop.follower_enemy
      follower_name ||= e.original_name
      $game_message.add("#{follower_name} manages to rise back up.")
      $game_message.add("She looks like she wants to join your party!\f")
      $game_message.add("Add her as a companion?")
      choice = 0
      ["Yes", "No"].each { |s| $game_message.choices.push(s) }
      $game_message.choice_cancel_type = 2
      $game_message.choice_proc = proc { |n| choice = n }
      wait_for_message

      choice == 0
    end
    def process_follow_ok(follower_name = nil)
      e = $game_troop.follower_enemy
      follower_name ||= e.original_name
      e.follow_yes_word.execute
      wait_for_message
      $game_message.add("#{follower_name} has joined the party!")
      if $game_party.multi_party?
        $game_message.add("#{wait_member_name} heads back to the pocket castle!")
        $game_party.add_stand_actor(e.follower_actor_id)
        $game_temp.getup_enemy = e.follower_actor_id
        wait_for_message
        return
      end
      wait_for_message
      process_follow_ok_member_full(e.follower_actor_id, follower_name) if $game_party.party_member_full?
      # 仲間になったエネミーを保存
      $game_actors[e.follower_actor_id].setup(e.follower_actor_id)
      $game_party.add_actor(e.follower_actor_id)
      $game_temp.getup_enemy = e.follower_actor_id
    end
    def process_follow_ok_member_full(follower_id, follower_name = nil)
      $game_message.add("The party is full.")
      $game_message.add("Please choose a member to remove from the party.")
      wait_for_message
      stand_actor = $game_party.choice_stand_actor_on_member_full(follower_id, follower_name)
      $game_party.move_stand_actor(stand_actor.id) if stand_actor
      wait_member_name = stand_actor ? stand_actor.name : follower_name
      $game_message.add("#{wait_member_name} heads back to the pocket castle!")
      wait_for_message
    end
    def process_follow_bye(follower_name = nil)
      e = $game_troop.follower_enemy
      follower_name ||= e.original_name
      $game_troop.follower_enemy = nil
      $game_message.add("#{follower_name} sulks away...")
      wait_for_message
    end
  end
end
class Game_Interpreter
  def change_friend(value)
    return unless $data_enemies[$game_variables[51]]
    return if value == 0

    enemy = $data_enemies[$game_variables[51]]
    se = RPG::SE.new(value > 0 ? "Raise3" : "Down3", 100, 100)
    text = format("%s's Affinity went %s%d!", enemy.name, 0 < value ? "up by " : "down by ", value.abs,)
    enemy.friend += value
    se.play
    $game_message.add(text)
    wait_for_message
  end
  def add_actor_ex(actor_id)
    $game_party.persona_change(actor_id)
    return if $game_party.exist_party_actor_id?(actor_id)

    $game_actors[actor_id].recover_all if $game_actors.exist?(actor_id)
    if $game_party.party_member_full?
      $game_message.add("The party is full.")
      $game_message.add("Choose someone to remove from the party.")
      stand_actor = $game_party.choice_stand_actor_on_member_full(actor_id, nil)
      move_stand_actor(stand_actor.id) if stand_actor
    end
    $game_party.add_actor(actor_id)
  end
  def move_actor_ex(actor_id)
    if $game_party.party_member_full?
      $game_message.add("The party is full.")
      $game_message.add("Choose someone to remove from the party.")
      stand_actor = $game_party.choice_stand_actor_on_member_full(actor_id, nil)
      move_stand_actor(stand_actor.id) if stand_actor
      return unless stand_actor
    end
    move_actor(actor_id)
  end
end

class Game_ActionResult
  def element_text
    @elements.delete(1)
    return "" if @elements.empty?

    elements =
      if @nightmare
        [NWConst::Elem::NIGHTMARE]
      elsif @pleasure
        [NWConst::Elem::PLEASURE]
      else
        @elements
      end
    texts = elements.sort.map { |id| $data_system.elements[id] || "" }.reject(&:empty?)
    return "" if texts.empty?

    if texts.size > 3
      "#{texts[0..2].join('/')}".force_encoding('UTF-8').encode
    else
      "#{texts.join('/')}".force_encoding('UTF-8').encode
    end
  end
end

module CAO::Poker
  BATTLE_HELP = "\\C[2]<key_c> key:\\C[0] Flip selected card　\\C[2]<key_b> key:\\C[0] Replace face-down cards"
  VOCAB_HANDS = [ "High card", "One pair", "Two pair", "Three of a kind",
                  "Straight", "Flush", "Full House", "Four of a kind",
                  "Straight Flush", "Five of a kind",
                  "Royal Flush"]
  VOCAB_COIN = "Balance"
  VOCAB_COIN_U = " coins"
  VOCAB_BET  = "Please set your wager"
  VOCAB_WIN  = "Congratulations.\n You won \\C[14]%s\\C[0] coins."
  VOCAB_LOSE = "That's too bad.\nYou lost and got \\C[14]%s\\C[0] coins."
  VOCAB_HELP = "\\C[2]<key_a>\\C[0] : Game description"
  TEXT_MANUAL = <<-'[EOS]'
\C[16]<Setting the amount of your bid>\C[0]
 ・\C[2]↑/↓ key\C[0] Increase/decrease your bid
 ・\C[2]<key_c> key\C[0] Confirm your bid and start the game
 ・\C[2]<key_b> key\C[0] Cancel

\C[16]<Card selection>\C[0]
 ・\C[2]←/→ key\C[0] Change the highlighted card
 ・\C[2]<key_c> key\C[0] Flip the selected card
 ・\C[2]<key_b> key\C[0] Replace face-down cards and confirm your hand
 
  [EOS]
end

class Scene_Slot < Scene_CasinoBase
  def process_result
    CasinoManager.slot_result.each do |bonus_lines|
      gain_coin = 0
      desc_text = nil
      sound = nil
      bonuses = []
      bonus_lines.each do |line_index|
        $game_slot.check_bonus(NWConst::Slot::LINES[line_index])
        CasinoManager.process_medal(:slot, $game_slot.result_medal, 0)
        bonuses.push($game_slot.result_bonus)
        gain_coin += CasinoManager.minimum_coin * $game_slot.result_scale
        desc_text = $game_slot.result_desc
        sound = $game_slot.result_sound
      end
      @spriteset.set_line_number(bonus_lines)
      @bonus_window.cursor_keys = bonuses
      $game_party.gain_coin(gain_coin)
      @desc_window.set_text(desc_text + "\n#{gain_coin} coins won!")
      CasinoManager.process_sound(:slot, sound, method(:abs_wait))
      @bonus_window.cursor_keys = []
    end
    
    @spriteset.set_line_number([])
    $game_slot.clear_result
    @desc_window.set_text(Help.slot_description[:stand])
    change_phase(:stand)
  end
end

module NWConst::Slot
  BONUS = {
    [-1, -1, -1] => {
      :desc  => "Nothing matched.",
      :scale => 0,
      :sound => NWConst::Casino::SE_LOSE,
      :skill_id => 3270,
      :medal => nil,
    },  
    [0, -1, -1] => {
      :desc  => "1 Cherry!",
      :scale => 1,
      :sound => NWConst::Casino::SE_WIN1,
      :skill_id => 3270,
      :medal => nil,
    },  
    [0, 0, -1] => {
      :desc  => "2 Cherry!",
      :scale => 2,
      :sound => NWConst::Casino::SE_WIN1,
      :skill_id => 3270,
      :medal => nil,
    },  
    [0, 0, 0] => {
      :desc  => "3 Cherry!",
      :scale => 5,
      :sound => NWConst::Casino::SE_WIN1,
      :skill_id => 3271,
      :medal => nil,
    },
    [1, 1, 1] => {
      :desc  => "Plum!",
      :scale => 20,
      :sound => NWConst::Casino::SE_WIN2,
      :skill_id => 3272,
      :medal => nil,
    },
    [2, 2, 2] => {
      :desc  => "Bell!",
      :scale => 50,
      :sound => NWConst::Casino::SE_WIN2,
      :skill_id => 3273,
      :medal => nil,
    },
    [3, 3, 3] => {
      :desc  => "Watermelon!",
      :scale => 100,
      :sound => NWConst::Casino::SE_WIN3,
      :skill_id => 3274,
      :medal => nil,
    },
    [4, 4, 4] => {
      :desc  => "BAR!",
      :scale => 200,
      :sound => NWConst::Casino::SE_WIN3,
      :skill_id => 3275,
      :medal => nil,
    },
    [5, 5, 5] => {
      :desc  => "777!",
      :scale => 500,
      :sound => NWConst::Casino::SE_WIN4,
      :skill_id => 3276,
      :medal => NWConst::Casino::MEDAL_SLOT,
    },
  }
end
class Foo::Status::Window_MainStatus < Window_Selectable
  def data
    [
      NWConst::Status::BASIC_COMMAND,
#~       (1..5).collect{|i| "装備品#{i}"},
      $data_system.terms.etypes.collect{|etype| etype + " Info"},
      NWConst::Status::CLASS_HISTORY_COMMAND,
      [],
    ]
  end
end