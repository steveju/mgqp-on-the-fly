class Scene_Title < Scene_Base
  def create_background
    @sprite1 = Sprite.new

    begin
      @sprite1.bitmap = Cache.title1("Paradox.png")
    rescue Errno::ENOENT
      @sprite1.bitmap = Cache.title1($data_system.title1_name)
    end

    @sprite2 = Sprite.new
    @sprite2.bitmap = Cache.title2($data_system.title2_name)
    center_sprite(@sprite1)
    center_sprite(@sprite2)
  end
end